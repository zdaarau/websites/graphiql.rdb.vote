const root = ReactDOM.createRoot(document.getElementById('graphiql'));
const fetcher = async graphQLParams => {
  const data = await fetch(
    'https://api.rdb.vote/rpc/graphql?query=' + encodeURIComponent(graphQLParams.query),
    {
      method: 'get',
      headers: { 'accept': 'application/json' }
    }
  );
  return data.json().catch(() => data.text());
};
const explorerPlugin = GraphiQLPluginExplorer.explorerPlugin();
root.render(
  React.createElement(GraphiQL, {
    fetcher,
    readOnly: false,
    defaultEditorToolsVisibility: true,
    plugins: [explorerPlugin]
  }),
);
