# TODOs

-   add favicon

-   add table and column descriptions via [SQL comment directives](https://supabase.github.io/pg_graphql/configuration/#description)?

    problem: [comment directives](https://supabase.github.io/pg_graphql/configuration/#comment-directives) aren't an official PostgreSQL standard, but specific
    to `pg_graphql`, and thus *not* ignored by other tools like PostgREST, i.e. pollute [PostgREST's OpenAPI
    description](https://postgrest.org/en/v12/references/api/openapi.html). if PostgREST adds [support for YAML headers in PostgreSQL
    `COMMENT`s](https://github.com/PostgREST/postgrest/issues/3487), we could simply hide the comment directives in there.
