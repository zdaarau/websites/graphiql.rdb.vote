# [graphiql.rdb.vote](https://graphiql.rdb.vote/)

[![Health](https://status.rdb.vote/api/v1/endpoints/static-sites_graphql-ide-(graphiql)/health/badge.svg)](https://status.rdb.vote/endpoints/static-sites_graphql-ide-(graphiql))
[![Uptime (24h)](https://status.rdb.vote/api/v1/endpoints/static-sites_graphql-ide-(graphiql)/uptimes/24h/badge.svg)](https://status.rdb.vote/endpoints/static-sites_graphql-ide-(graphiql))
[![Response time (24h)](https://status.rdb.vote/api/v1/endpoints/static-sites_graphql-ide-(graphiql)/response-times/24h/badge.svg)](https://status.rdb.vote/endpoints/static-sites_graphql-ide-(graphiql))
[![Netlify Status](https://api.netlify.com/api/v1/badges/00a360c8-246c-4faa-bb97-16de8b7cd308/deploy-status)](https://app.netlify.com/sites/graphiql-rdb-vote/deploys)

RDB GraphQL IDE provided by statically served [GraphiQL](https://github.com/graphql/graphiql/tree/main/packages/graphiql#readme).

## Inital setup

``` sh
mkdir -p public/assets
npm install graphiql react react-dom graphql @graphiql/plugin-explorer
cp --recursive node_modules/* public/assets/
# minify --recursive --output public/ public/assets
minify --output public/custom.min.css custom.css
minify --output public/init.min.js init.js
minify --output public/ index.html
```

Note that the minification of the `public/assets` folder is commented out since it results in an `Uncaught SyntaxError: illegal character U+0008` when
`graphiql.min.js` is executed.

GraphiQL is configured in [`init.js`](init.js) via [React props and children](https://github.com/graphql/graphiql/tree/main/packages/graphiql#customize).
Unfortunately, the [auto-generated documentation](https://graphiql-test.netlify.app/typedoc/modules/graphiql.html#graphiqlprops-1) is diffuse and hard to
comprehend. A simple overview of supported props and children is found [in a very outdated version of the project
README](https://github.com/TheSharpieOne/graphiql/blob/c2917c74db10ea55bd64521a0ec330648b9d8916/README.md#usage), though.

## Update

``` sh
npm-check-updates --upgrade
npm update
rm --recursive public/assets/*/
cp --recursive node_modules/* public/assets/
# minify --recursive --output public/ public/assets
minify --output public/custom.min.css custom.css
minify --output public/init.min.js init.js
minify --output public/ index.html
```

## Notes

-   We define a custom `fetcher` function in [`init.js`](init.js) that uses PostgREST's `GET /rpc/graphql` endpoint (the default fetcher function issues `POST`
    requests), so it works when PostgREST is connected to a PostgreSQL read replica.

-   By default, GraphiQL issues an [introspection](https://graphql.org/learn/introspection/) query to build the GraphQL schema. Instead, we could also provide a
    prebuilt schema. To issue GraphiQL's [default introspection query](https://github.com/graphql/graphql-js/blob/main/src/utilities/getIntrospectionQuery.ts)
    via PostgREST's `GET /rpc/graphql` endpoint and get the schema data to be set as the `schema` prop's value in [`init.js`](init.js), run:

    ``` sh
    node --input-type=module \
         --eval "import { getIntrospectionQuery } from 'graphql/utilities/getIntrospectionQuery.js'; console.log(getIntrospectionQuery());" \
      | sd '\s+' ' ' \
      | sd '^\s+|\s+$' '' \
      | jq --slurp --raw-input --raw-output @uri \
      | echo "https://api.rdb.vote:443/rpc/graphql?query=$(cat -)" \
      | xargs -n 1 curl --request 'GET' --header 'accept: application/json' \
      | jq --compact-output .data
    ```

-   The following files served as inspiration

    -   for [`index.html`](index.html):

        -   <https://github.com/graphql/graphiql/blob/main/examples/graphiql-cdn/index.html>
        -   <https://github.com/supabase/pg_graphql/blob/master/dockerfiles/graphiql/index.html>

    -   for [`init.js`](init.js) (the custom fetcher function in particular):

        -   <https://github.com/graphql/graphiql/blob/main/examples/graphiql-create-react-app/src/App.tsx>
        -   <https://github.com/graphql/graphiql/blob/main/examples/graphiql-parcel/src/index.tsx>

## License

Code and configuration in this repository is licensed under [`AGPL-3.0-or-later`](https://spdx.org/licenses/AGPL-3.0-or-later.html). See
[`LICENSE.md`](LICENSE.md).
