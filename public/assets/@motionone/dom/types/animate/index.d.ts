import type { AnimationOptionsWithOverrides, MotionKeyframesDefinition } from "./types";
import { AnimationControls } from "@motionone/types";
import { ElementOrSelector } from "../types";
export declare function animate(elements: ElementOrSelector, keyframes: MotionKeyframesDefinition, options?: AnimationOptionsWithOverrides): AnimationControls;
//# sourceMappingURL=index.d.ts.map