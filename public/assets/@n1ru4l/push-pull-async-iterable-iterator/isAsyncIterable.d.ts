export declare function isAsyncIterable(input: unknown): input is AsyncIterator<unknown> | AsyncIterableIterator<unknown>;
