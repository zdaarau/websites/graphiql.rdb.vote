import { GraphiQLPlugin } from '@graphiql/react';
import { GraphiQLExplorerProps } from 'graphiql-explorer';
import './graphiql-explorer.d.ts';
import './index.css';
export declare type GraphiQLExplorerPluginProps = Omit<GraphiQLExplorerProps, 'onEdit' | 'query'>;
export declare function explorerPlugin(props?: GraphiQLExplorerPluginProps): GraphiQLPlugin;
