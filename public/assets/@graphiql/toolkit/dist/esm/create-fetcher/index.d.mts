export { CreateFetcherOptions, ExecutionResultPayload, Fetcher, FetcherOpts, FetcherParams, FetcherResult, FetcherResultPayload, FetcherReturnType, MaybePromise, Observable, SyncExecutionResult, SyncFetcherResult, Unsubscribable } from './types.mjs';
export { createGraphiQLFetcher } from './createFetcher.mjs';
import 'graphql';
import 'graphql-ws';
