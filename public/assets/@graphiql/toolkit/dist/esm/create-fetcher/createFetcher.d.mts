import { CreateFetcherOptions, Fetcher } from './types.mjs';
import 'graphql';
import 'graphql-ws';

/**
 * build a GraphiQL fetcher that is:
 * - backwards compatible
 * - optionally supports graphql-ws or `
 */
declare function createGraphiQLFetcher(options: CreateFetcherOptions): Fetcher;

export { createGraphiQLFetcher };
