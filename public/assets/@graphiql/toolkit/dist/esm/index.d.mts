export { fetcherReturnToPromise, isAsyncIterable, isObservable, isPromise } from './async-helpers/index.mjs';
export { CreateFetcherOptions, ExecutionResultPayload, Fetcher, FetcherOpts, FetcherParams, FetcherResult, FetcherResultPayload, FetcherReturnType, MaybePromise, Observable, SyncExecutionResult, SyncFetcherResult, Unsubscribable } from './create-fetcher/types.mjs';
export { createGraphiQLFetcher } from './create-fetcher/createFetcher.mjs';
export { formatError, formatResult } from './format/index.mjs';
export { GetDefaultFieldNamesFn, fillLeafs } from './graphql-helpers/auto-complete.mjs';
export { mergeAst } from './graphql-helpers/merge-ast.mjs';
export { getSelectedOperationName } from './graphql-helpers/operation-name.mjs';
export { Storage, StorageAPI } from './storage/base.mjs';
export { HistoryStore } from './storage/history.mjs';
export { QueryStore, QueryStoreItem } from './storage/query.mjs';
export { CreateLocalStorageOptions, createLocalStorage } from './storage/custom.mjs';
import 'graphql';
import 'graphql-ws';
