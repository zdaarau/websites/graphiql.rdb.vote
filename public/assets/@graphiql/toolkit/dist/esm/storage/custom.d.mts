import { Storage } from './base.mjs';

/**
 * This function enables a custom namespace for localStorage
 */

declare type CreateLocalStorageOptions = {
    /**
     * specify a different storage namespace prefix from the default of 'graphiql'
     */
    namespace?: string;
};
/**
 * generate a custom local storage adapter for GraphiQL `storage` prop.
 */
declare function createLocalStorage({ namespace, }: CreateLocalStorageOptions): Storage;

export { type CreateLocalStorageOptions, createLocalStorage };
