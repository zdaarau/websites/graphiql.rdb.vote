import { StorageAPI } from './base.mjs';

declare type QueryStoreItem = {
    query?: string;
    variables?: string;
    headers?: string;
    operationName?: string;
    label?: string;
    favorite?: boolean;
};
declare class QueryStore {
    private key;
    private storage;
    private maxSize;
    items: Array<QueryStoreItem>;
    constructor(key: string, storage: StorageAPI, maxSize?: number | null);
    get length(): number;
    contains(item: QueryStoreItem): boolean;
    edit(item: QueryStoreItem, index?: number): void;
    delete(item: QueryStoreItem): void;
    fetchRecent(): QueryStoreItem | undefined;
    fetchAll(): QueryStoreItem[];
    push(item: QueryStoreItem): void;
    save(): void;
}

export { QueryStore, type QueryStoreItem };
