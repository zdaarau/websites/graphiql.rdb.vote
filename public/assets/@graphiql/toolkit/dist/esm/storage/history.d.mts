import { StorageAPI } from './base.mjs';
import { QueryStoreItem, QueryStore } from './query.mjs';

declare class HistoryStore {
    private storage;
    private maxHistoryLength;
    queries: Array<QueryStoreItem>;
    history: QueryStore;
    favorite: QueryStore;
    constructor(storage: StorageAPI, maxHistoryLength: number);
    private shouldSaveQuery;
    updateHistory: ({ query, variables, headers, operationName, }: QueryStoreItem) => void;
    toggleFavorite({ query, variables, headers, operationName, label, favorite, }: QueryStoreItem): void;
    editLabel({ query, variables, headers, operationName, label, favorite, }: QueryStoreItem, index?: number): void;
    deleteHistory: ({ query, variables, headers, operationName, favorite }: QueryStoreItem, clearFavorites?: boolean) => void;
}

export { HistoryStore };
