export { Storage, StorageAPI } from './base.mjs';
export { HistoryStore } from './history.mjs';
export { QueryStore, QueryStoreItem } from './query.mjs';
export { CreateLocalStorageOptions, createLocalStorage } from './custom.mjs';
