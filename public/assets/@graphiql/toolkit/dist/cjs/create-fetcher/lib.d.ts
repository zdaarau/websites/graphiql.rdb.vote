import { DocumentNode } from 'graphql';
import { ClientOptions, Client } from 'graphql-ws';
import { CreateFetcherOptions, Fetcher, FetcherParams, FetcherOpts } from './types.js';

/**
 * Returns true if the name matches a subscription in the AST
 *
 * @param document {DocumentNode}
 * @param name the operation name to lookup
 * @returns {boolean}
 */
declare const isSubscriptionWithName: (document: DocumentNode, name?: string) => boolean;
/**
 * create a simple HTTP/S fetcher using a fetch implementation where
 * multipart is not needed
 *
 * @param options {CreateFetcherOptions}
 * @param httpFetch {typeof fetch}
 * @returns {Fetcher}
 */
declare const createSimpleFetcher: (options: CreateFetcherOptions, httpFetch: typeof fetch) => Fetcher;
declare function createWebsocketsFetcherFromUrl(url: string, connectionParams?: ClientOptions['connectionParams']): Promise<Fetcher | void>;
/**
 * Create ws/s fetcher using provided wsClient implementation
 */
declare const createWebsocketsFetcherFromClient: (wsClient: Client) => Fetcher;
/**
 * Allow legacy websockets protocol client, but no definitions for it,
 * as the library is deprecated and has security issues
 */
declare const createLegacyWebsocketsFetcher: (legacyWsClient: {
    request: (params: FetcherParams) => unknown;
}) => Fetcher;
/**
 * Create a fetcher with the `IncrementalDelivery` HTTP/S spec for
 * `@stream` and `@defer` support using `fetch-multipart-graphql`
 */
declare const createMultipartFetcher: (options: CreateFetcherOptions, httpFetch: typeof fetch) => Fetcher;
/**
 * If `wsClient` or `legacyClient` are provided, then `subscriptionUrl` is overridden.
 */
declare function getWsFetcher(options: CreateFetcherOptions, fetcherOpts?: FetcherOpts): Promise<Fetcher | void>;

export { createLegacyWebsocketsFetcher, createMultipartFetcher, createSimpleFetcher, createWebsocketsFetcherFromClient, createWebsocketsFetcherFromUrl, getWsFetcher, isSubscriptionWithName };
