"use strict";
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: !0 });
}, __copyProps = (to, from, except, desc) => {
  if (from && typeof from == "object" || typeof from == "function")
    for (let key of __getOwnPropNames(from))
      !__hasOwnProp.call(to, key) && key !== except && __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
  return to;
};
var __toCommonJS = (mod) => __copyProps(__defProp({}, "__esModule", { value: !0 }), mod);
var createFetcher_exports = {};
__export(createFetcher_exports, {
  createGraphiQLFetcher: () => createGraphiQLFetcher
});
module.exports = __toCommonJS(createFetcher_exports);
var import_lib = require("./lib");
function createGraphiQLFetcher(options) {
  const httpFetch = options.fetch || typeof window != "undefined" && window.fetch;
  if (!httpFetch)
    throw new Error("No valid fetcher implementation available");
  options.enableIncrementalDelivery = options.enableIncrementalDelivery !== !1;
  const simpleFetcher = (0, import_lib.createSimpleFetcher)(options, httpFetch), httpFetcher = options.enableIncrementalDelivery ? (0, import_lib.createMultipartFetcher)(options, httpFetch) : simpleFetcher;
  return async (graphQLParams, fetcherOpts) => {
    if (graphQLParams.operationName === "IntrospectionQuery")
      return (options.schemaFetcher || simpleFetcher)(
        graphQLParams,
        fetcherOpts
      );
    if (fetcherOpts != null && fetcherOpts.documentAST ? (0, import_lib.isSubscriptionWithName)(
      fetcherOpts.documentAST,
      graphQLParams.operationName || void 0
    ) : !1) {
      const wsFetcher = await (0, import_lib.getWsFetcher)(options, fetcherOpts);
      if (!wsFetcher)
        throw new Error(
          `Your GraphiQL createFetcher is not properly configured for websocket subscriptions yet. ${options.subscriptionUrl ? `Provided URL ${options.subscriptionUrl} failed` : "Please provide subscriptionUrl, wsClient or legacyClient option first."}`
        );
      return wsFetcher(graphQLParams);
    }
    return httpFetcher(graphQLParams, fetcherOpts);
  };
}
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  createGraphiQLFetcher
});
