"use strict";
var __defProp = Object.defineProperty, __defProps = Object.defineProperties, __getOwnPropDesc = Object.getOwnPropertyDescriptor, __getOwnPropDescs = Object.getOwnPropertyDescriptors, __getOwnPropNames = Object.getOwnPropertyNames, __getOwnPropSymbols = Object.getOwnPropertySymbols;
var __hasOwnProp = Object.prototype.hasOwnProperty, __propIsEnum = Object.prototype.propertyIsEnumerable;
var __knownSymbol = (name, symbol) => (symbol = Symbol[name]) ? symbol : Symbol.for("Symbol." + name);
var __defNormalProp = (obj, key, value) => key in obj ? __defProp(obj, key, { enumerable: !0, configurable: !0, writable: !0, value }) : obj[key] = value, __spreadValues = (a, b) => {
  for (var prop in b || (b = {}))
    __hasOwnProp.call(b, prop) && __defNormalProp(a, prop, b[prop]);
  if (__getOwnPropSymbols)
    for (var prop of __getOwnPropSymbols(b))
      __propIsEnum.call(b, prop) && __defNormalProp(a, prop, b[prop]);
  return a;
}, __spreadProps = (a, b) => __defProps(a, __getOwnPropDescs(b));
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: !0 });
}, __copyProps = (to, from, except, desc) => {
  if (from && typeof from == "object" || typeof from == "function")
    for (let key of __getOwnPropNames(from))
      !__hasOwnProp.call(to, key) && key !== except && __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
  return to;
};
var __toCommonJS = (mod) => __copyProps(__defProp({}, "__esModule", { value: !0 }), mod);
var __await = function(promise, isYieldStar) {
  this[0] = promise, this[1] = isYieldStar;
}, __asyncGenerator = (__this, __arguments, generator) => {
  var resume = (k, v, yes, no) => {
    try {
      var x = generator[k](v), isAwait = (v = x.value) instanceof __await, done = x.done;
      Promise.resolve(isAwait ? v[0] : v).then((y) => isAwait ? resume(k === "return" ? k : "next", v[1] ? { done: y.done, value: y.value } : y, yes, no) : yes({ value: y, done })).catch((e) => resume("throw", e, yes, no));
    } catch (e) {
      no(e);
    }
  }, method = (k) => it[k] = (x) => new Promise((yes, no) => resume(k, x, yes, no)), it = {};
  return generator = generator.apply(__this, __arguments), it[__knownSymbol("asyncIterator")] = () => it, method("next"), method("throw"), method("return"), it;
};
var __forAwait = (obj, it, method) => (it = obj[__knownSymbol("asyncIterator")]) ? it.call(obj) : (obj = obj[__knownSymbol("iterator")](), it = {}, method = (key, fn) => (fn = obj[key]) && (it[key] = (arg) => new Promise((yes, no, done) => (arg = fn.call(obj, arg), done = arg.done, Promise.resolve(arg.value).then((value) => yes({ value, done }), no)))), method("next"), method("return"), it);
var lib_exports = {};
__export(lib_exports, {
  createLegacyWebsocketsFetcher: () => createLegacyWebsocketsFetcher,
  createMultipartFetcher: () => createMultipartFetcher,
  createSimpleFetcher: () => createSimpleFetcher,
  createWebsocketsFetcherFromClient: () => createWebsocketsFetcherFromClient,
  createWebsocketsFetcherFromUrl: () => createWebsocketsFetcherFromUrl,
  getWsFetcher: () => getWsFetcher,
  isSubscriptionWithName: () => isSubscriptionWithName
});
module.exports = __toCommonJS(lib_exports);
var import_graphql = require("graphql"), import_meros = require("meros"), import_push_pull_async_iterable_iterator = require("@n1ru4l/push-pull-async-iterable-iterator");
const errorHasCode = (err) => typeof err == "object" && err !== null && "code" in err, isSubscriptionWithName = (document, name) => {
  let isSubscription = !1;
  return (0, import_graphql.visit)(document, {
    OperationDefinition(node) {
      var _a;
      name === ((_a = node.name) == null ? void 0 : _a.value) && node.operation === "subscription" && (isSubscription = !0);
    }
  }), isSubscription;
}, createSimpleFetcher = (options, httpFetch) => async (graphQLParams, fetcherOpts) => (await httpFetch(options.url, {
  method: "POST",
  body: JSON.stringify(graphQLParams),
  headers: __spreadValues(__spreadValues({
    "content-type": "application/json"
  }, options.headers), fetcherOpts == null ? void 0 : fetcherOpts.headers)
})).json();
async function createWebsocketsFetcherFromUrl(url, connectionParams) {
  let wsClient;
  try {
    const { createClient } = require("graphql-ws");
    return wsClient = createClient({ url, connectionParams }), createWebsocketsFetcherFromClient(wsClient);
  } catch (err) {
    if (errorHasCode(err) && err.code === "MODULE_NOT_FOUND")
      throw new Error(
        "You need to install the 'graphql-ws' package to use websockets when passing a 'subscriptionUrl'"
      );
    console.error(`Error creating websocket client for ${url}`, err);
  }
}
const createWebsocketsFetcherFromClient = (wsClient) => (graphQLParams) => (0, import_push_pull_async_iterable_iterator.makeAsyncIterableIteratorFromSink)(
  (sink) => wsClient.subscribe(graphQLParams, __spreadProps(__spreadValues({}, sink), {
    error(err) {
      err instanceof CloseEvent ? sink.error(
        new Error(
          `Socket closed with event ${err.code} ${err.reason || ""}`.trim()
        )
      ) : sink.error(err);
    }
  }))
), createLegacyWebsocketsFetcher = (legacyWsClient) => (graphQLParams) => {
  const observable = legacyWsClient.request(graphQLParams);
  return (0, import_push_pull_async_iterable_iterator.makeAsyncIterableIteratorFromSink)(
    // @ts-ignore
    (sink) => observable.subscribe(sink).unsubscribe
  );
}, createMultipartFetcher = (options, httpFetch) => function(graphQLParams, fetcherOpts) {
  return __asyncGenerator(this, null, function* () {
    const response = yield new __await(httpFetch(options.url, {
      method: "POST",
      body: JSON.stringify(graphQLParams),
      headers: __spreadValues(__spreadValues({
        "content-type": "application/json",
        accept: "application/json, multipart/mixed"
      }, options.headers), fetcherOpts == null ? void 0 : fetcherOpts.headers)
    }).then(
      (r) => (0, import_meros.meros)(r, {
        multiple: !0
      })
    ));
    if (!(0, import_push_pull_async_iterable_iterator.isAsyncIterable)(response))
      return yield response.json();
    try {
      for (var iter = __forAwait(response), more, temp, error; more = !(temp = yield new __await(iter.next())).done; more = !1) {
        const chunk = temp.value;
        if (chunk.some((part) => !part.json)) {
          const message = chunk.map(
            (part) => `Headers::
${part.headers}

Body::
${part.body}`
          );
          throw new Error(
            `Expected multipart chunks to be of json type. got:
${message}`
          );
        }
        yield chunk.map((part) => part.body);
      }
    } catch (temp) {
      error = [temp];
    } finally {
      try {
        more && (temp = iter.return) && (yield new __await(temp.call(iter)));
      } finally {
        if (error)
          throw error[0];
      }
    }
  });
};
async function getWsFetcher(options, fetcherOpts) {
  if (options.wsClient)
    return createWebsocketsFetcherFromClient(options.wsClient);
  if (options.subscriptionUrl)
    return createWebsocketsFetcherFromUrl(options.subscriptionUrl, __spreadValues(__spreadValues({}, options.wsConnectionParams), fetcherOpts == null ? void 0 : fetcherOpts.headers));
  const legacyWebsocketsClient = options.legacyClient || options.legacyWsClient;
  if (legacyWebsocketsClient)
    return createLegacyWebsocketsFetcher(legacyWebsocketsClient);
}
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  createLegacyWebsocketsFetcher,
  createMultipartFetcher,
  createSimpleFetcher,
  createWebsocketsFetcherFromClient,
  createWebsocketsFetcherFromUrl,
  getWsFetcher,
  isSubscriptionWithName
});
