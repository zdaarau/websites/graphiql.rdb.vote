"use strict";
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: !0 });
}, __copyProps = (to, from, except, desc) => {
  if (from && typeof from == "object" || typeof from == "function")
    for (let key of __getOwnPropNames(from))
      !__hasOwnProp.call(to, key) && key !== except && __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
  return to;
}, __reExport = (target, mod, secondTarget) => (__copyProps(target, mod, "default"), secondTarget && __copyProps(secondTarget, mod, "default"));
var __toCommonJS = (mod) => __copyProps(__defProp({}, "__esModule", { value: !0 }), mod);
var create_fetcher_exports = {};
__export(create_fetcher_exports, {
  createGraphiQLFetcher: () => import_createFetcher.createGraphiQLFetcher
});
module.exports = __toCommonJS(create_fetcher_exports);
__reExport(create_fetcher_exports, require("./types"), module.exports);
var import_createFetcher = require("./createFetcher");
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  createGraphiQLFetcher,
  ...require("./types")
});
