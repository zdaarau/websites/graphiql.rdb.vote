import { DocumentNode, IntrospectionQuery } from 'graphql';
import { ExecutionResult, Client, ClientOptions } from 'graphql-ws';

declare type Observable<T> = {
    subscribe(opts: {
        next: (value: T) => void;
        error: (error: any) => void;
        complete: () => void;
    }): Unsubscribable;
    subscribe(next: (value: T) => void, error: null | undefined, complete: () => void): Unsubscribable;
    subscribe(next?: (value: T) => void, error?: (error: any) => void, complete?: () => void): Unsubscribable;
};
declare type Unsubscribable = {
    unsubscribe: () => void;
};
declare type FetcherParams = {
    query: string;
    operationName?: string | null;
    variables?: any;
};
declare type FetcherOpts = {
    headers?: {
        [key: string]: any;
    };
    documentAST?: DocumentNode;
};
declare type ExecutionResultPayload = {
    data: IntrospectionQuery;
    errors?: Array<any>;
} | {
    data?: any;
    errors?: Array<any>;
} | {
    data?: any;
    errors?: Array<any>;
    hasNext: boolean;
} | {
    data?: any;
    errors?: any[];
    path: (string | number)[];
    hasNext: boolean;
};
declare type FetcherResultPayload = ExecutionResultPayload;
declare type MaybePromise<T> = T | Promise<T>;
declare type FetcherResult = ExecutionResult | {
    data: IntrospectionQuery;
};
declare type SyncExecutionResult = ExecutionResult | Observable<ExecutionResult> | AsyncIterable<ExecutionResult>;
declare type SyncFetcherResult = SyncExecutionResult;
declare type FetcherReturnType = MaybePromise<SyncExecutionResult>;
declare type Fetcher = (graphQLParams: FetcherParams, opts?: FetcherOpts) => FetcherReturnType;
/**
 * Options for creating a simple, spec-compliant GraphiQL fetcher
 */
interface CreateFetcherOptions {
    /**
     * url for HTTP(S) requests. required!
     */
    url: string;
    /**
     * url for websocket subscription requests
     */
    subscriptionUrl?: string;
    /**
     * `wsClient` implementation that matches `ws-graphql` signature,
     * whether via `createClient()` itself or another client.
     */
    wsClient?: Client;
    /**
     * `legacyWsClient` implementation that matches `subscriptions-transport-ws` signature,
     * whether via `new SubscriptionsClient()` itself or another client with a similar signature.
     */
    legacyWsClient?: any;
    /**
     * alias for `legacyWsClient`
     */
    legacyClient?: any;
    /**
     * Headers you can provide statically.
     *
     * If you enable the headers editor and the user provides
     * A header you set statically here, it will be overridden by their value.
     */
    headers?: Record<string, string>;
    /**
     * Websockets connection params used when you provide subscriptionUrl. graphql-ws `ClientOptions.connectionParams`
     */
    wsConnectionParams?: ClientOptions['connectionParams'];
    /**
     * You can disable the usage of the `fetch-multipart-graphql` library
     * entirely, defaulting to a simple fetch POST implementation.
     */
    enableIncrementalDelivery?: boolean;
    /**
     * The fetch implementation, in case the user needs to override this for SSR
     * or other purposes. this does not override the `fetch-multipart-graphql`
     * default fetch behavior yet.
     */
    fetch?: typeof fetch;
    /**
     * An optional custom fetcher specifically for your schema. For most cases
     * the `url` and `headers` property should have you covered.
     */
    schemaFetcher?: Fetcher;
}

export type { CreateFetcherOptions, ExecutionResultPayload, Fetcher, FetcherOpts, FetcherParams, FetcherResult, FetcherResultPayload, FetcherReturnType, MaybePromise, Observable, SyncExecutionResult, SyncFetcherResult, Unsubscribable };
