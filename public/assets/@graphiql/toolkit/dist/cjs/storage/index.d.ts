export { Storage, StorageAPI } from './base.js';
export { HistoryStore } from './history.js';
export { QueryStore, QueryStoreItem } from './query.js';
export { CreateLocalStorageOptions, createLocalStorage } from './custom.js';
