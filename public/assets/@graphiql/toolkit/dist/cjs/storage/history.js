"use strict";
var __defProp = Object.defineProperty, __defProps = Object.defineProperties, __getOwnPropDesc = Object.getOwnPropertyDescriptor, __getOwnPropDescs = Object.getOwnPropertyDescriptors, __getOwnPropNames = Object.getOwnPropertyNames, __getOwnPropSymbols = Object.getOwnPropertySymbols;
var __hasOwnProp = Object.prototype.hasOwnProperty, __propIsEnum = Object.prototype.propertyIsEnumerable;
var __defNormalProp = (obj, key, value) => key in obj ? __defProp(obj, key, { enumerable: !0, configurable: !0, writable: !0, value }) : obj[key] = value, __spreadValues = (a, b) => {
  for (var prop in b || (b = {}))
    __hasOwnProp.call(b, prop) && __defNormalProp(a, prop, b[prop]);
  if (__getOwnPropSymbols)
    for (var prop of __getOwnPropSymbols(b))
      __propIsEnum.call(b, prop) && __defNormalProp(a, prop, b[prop]);
  return a;
}, __spreadProps = (a, b) => __defProps(a, __getOwnPropDescs(b));
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: !0 });
}, __copyProps = (to, from, except, desc) => {
  if (from && typeof from == "object" || typeof from == "function")
    for (let key of __getOwnPropNames(from))
      !__hasOwnProp.call(to, key) && key !== except && __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
  return to;
};
var __toCommonJS = (mod) => __copyProps(__defProp({}, "__esModule", { value: !0 }), mod);
var history_exports = {};
__export(history_exports, {
  HistoryStore: () => HistoryStore
});
module.exports = __toCommonJS(history_exports);
var import_graphql = require("graphql"), import_query = require("./query");
const MAX_QUERY_SIZE = 1e5;
class HistoryStore {
  constructor(storage, maxHistoryLength) {
    this.storage = storage;
    this.maxHistoryLength = maxHistoryLength;
    this.updateHistory = ({
      query,
      variables,
      headers,
      operationName
    }) => {
      if (!this.shouldSaveQuery(
        query,
        variables,
        headers,
        this.history.fetchRecent()
      ))
        return;
      this.history.push({
        query,
        variables,
        headers,
        operationName
      });
      const historyQueries = this.history.items, favoriteQueries = this.favorite.items;
      this.queries = historyQueries.concat(favoriteQueries);
    };
    this.deleteHistory = ({ query, variables, headers, operationName, favorite }, clearFavorites = !1) => {
      function deleteFromStore(store) {
        const found = store.items.find(
          (x) => x.query === query && x.variables === variables && x.headers === headers && x.operationName === operationName
        );
        found && store.delete(found);
      }
      (favorite || clearFavorites) && deleteFromStore(this.favorite), (!favorite || clearFavorites) && deleteFromStore(this.history), this.queries = [...this.history.items, ...this.favorite.items];
    };
    this.history = new import_query.QueryStore(
      "queries",
      this.storage,
      this.maxHistoryLength
    ), this.favorite = new import_query.QueryStore("favorites", this.storage, null), this.queries = [...this.history.fetchAll(), ...this.favorite.fetchAll()];
  }
  shouldSaveQuery(query, variables, headers, lastQuerySaved) {
    if (!query)
      return !1;
    try {
      (0, import_graphql.parse)(query);
    } catch (e) {
      return !1;
    }
    return query.length > MAX_QUERY_SIZE ? !1 : lastQuerySaved ? !(JSON.stringify(query) === JSON.stringify(lastQuerySaved.query) && (JSON.stringify(variables) === JSON.stringify(lastQuerySaved.variables) && (JSON.stringify(headers) === JSON.stringify(lastQuerySaved.headers) || headers && !lastQuerySaved.headers) || variables && !lastQuerySaved.variables)) : !0;
  }
  toggleFavorite({
    query,
    variables,
    headers,
    operationName,
    label,
    favorite
  }) {
    const item = {
      query,
      variables,
      headers,
      operationName,
      label
    };
    favorite ? (item.favorite = !1, this.favorite.delete(item), this.history.push(item)) : (item.favorite = !0, this.favorite.push(item), this.history.delete(item)), this.queries = [...this.history.items, ...this.favorite.items];
  }
  editLabel({
    query,
    variables,
    headers,
    operationName,
    label,
    favorite
  }, index) {
    const item = {
      query,
      variables,
      headers,
      operationName,
      label
    };
    favorite ? this.favorite.edit(__spreadProps(__spreadValues({}, item), { favorite }), index) : this.history.edit(item, index), this.queries = [...this.history.items, ...this.favorite.items];
  }
}
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  HistoryStore
});
