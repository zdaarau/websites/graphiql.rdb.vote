"use strict";
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: !0 });
}, __copyProps = (to, from, except, desc) => {
  if (from && typeof from == "object" || typeof from == "function")
    for (let key of __getOwnPropNames(from))
      !__hasOwnProp.call(to, key) && key !== except && __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
  return to;
};
var __toCommonJS = (mod) => __copyProps(__defProp({}, "__esModule", { value: !0 }), mod);
var query_exports = {};
__export(query_exports, {
  QueryStore: () => QueryStore
});
module.exports = __toCommonJS(query_exports);
class QueryStore {
  constructor(key, storage, maxSize = null) {
    this.key = key;
    this.storage = storage;
    this.maxSize = maxSize;
    this.items = this.fetchAll();
  }
  get length() {
    return this.items.length;
  }
  contains(item) {
    return this.items.some(
      (x) => x.query === item.query && x.variables === item.variables && x.headers === item.headers && x.operationName === item.operationName
    );
  }
  edit(item, index) {
    if (typeof index == "number" && this.items[index]) {
      const found = this.items[index];
      if (found.query === item.query && found.variables === item.variables && found.headers === item.headers && found.operationName === item.operationName) {
        this.items.splice(index, 1, item), this.save();
        return;
      }
    }
    const itemIndex = this.items.findIndex(
      (x) => x.query === item.query && x.variables === item.variables && x.headers === item.headers && x.operationName === item.operationName
    );
    itemIndex !== -1 && (this.items.splice(itemIndex, 1, item), this.save());
  }
  delete(item) {
    const itemIndex = this.items.findIndex(
      (x) => x.query === item.query && x.variables === item.variables && x.headers === item.headers && x.operationName === item.operationName
    );
    itemIndex !== -1 && (this.items.splice(itemIndex, 1), this.save());
  }
  fetchRecent() {
    return this.items.at(-1);
  }
  fetchAll() {
    const raw = this.storage.get(this.key);
    return raw ? JSON.parse(raw)[this.key] : [];
  }
  push(item) {
    const items = [...this.items, item];
    this.maxSize && items.length > this.maxSize && items.shift();
    for (let attempts = 0; attempts < 5; attempts++) {
      const response = this.storage.set(
        this.key,
        JSON.stringify({ [this.key]: items })
      );
      if (!(response != null && response.error))
        this.items = items;
      else if (response.isQuotaError && this.maxSize)
        items.shift();
      else
        return;
    }
  }
  save() {
    this.storage.set(this.key, JSON.stringify({ [this.key]: this.items }));
  }
}
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  QueryStore
});
