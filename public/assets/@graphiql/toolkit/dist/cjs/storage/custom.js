"use strict";
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: !0 });
}, __copyProps = (to, from, except, desc) => {
  if (from && typeof from == "object" || typeof from == "function")
    for (let key of __getOwnPropNames(from))
      !__hasOwnProp.call(to, key) && key !== except && __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
  return to;
};
var __toCommonJS = (mod) => __copyProps(__defProp({}, "__esModule", { value: !0 }), mod);
var custom_exports = {};
__export(custom_exports, {
  createLocalStorage: () => createLocalStorage
});
module.exports = __toCommonJS(custom_exports);
function createLocalStorage({
  namespace
}) {
  const storageKeyPrefix = `${namespace}:`, getStorageKey = (key) => `${storageKeyPrefix}${key}`;
  return {
    setItem: (key, value) => localStorage.setItem(getStorageKey(key), value),
    getItem: (key) => localStorage.getItem(getStorageKey(key)),
    removeItem: (key) => localStorage.removeItem(getStorageKey(key)),
    get length() {
      let keys = 0;
      for (const key in localStorage)
        key.indexOf(storageKeyPrefix) === 0 && (keys += 1);
      return keys;
    },
    clear() {
      for (const key in localStorage)
        key.indexOf(storageKeyPrefix) === 0 && localStorage.removeItem(key);
    }
  };
}
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  createLocalStorage
});
