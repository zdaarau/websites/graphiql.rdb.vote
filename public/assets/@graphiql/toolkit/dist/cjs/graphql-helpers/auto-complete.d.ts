import { GraphQLType, GraphQLSchema } from 'graphql';

declare type Insertion = {
    index: number;
    string: string;
};
declare type GetDefaultFieldNamesFn = (type: GraphQLType) => string[];
/**
 * Given a document string which may not be valid due to terminal fields not
 * representing leaf values (Spec Section: "Leaf Field Selections"), and a
 * function which provides reasonable default field names for a given type,
 * this function will attempt to produce a schema which is valid after filling
 * in selection sets for the invalid fields.
 *
 * Note that there is no guarantee that the result will be a valid query, this
 * utility represents a "best effort" which may be useful within IDE tools.
 */
declare function fillLeafs(schema?: GraphQLSchema | null, docString?: string, getDefaultFieldNames?: GetDefaultFieldNamesFn): {
    insertions: Insertion[];
    result: string | undefined;
};

export { type GetDefaultFieldNamesFn, fillLeafs };
