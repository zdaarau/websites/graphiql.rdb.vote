"use strict";
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: !0 });
}, __copyProps = (to, from, except, desc) => {
  if (from && typeof from == "object" || typeof from == "function")
    for (let key of __getOwnPropNames(from))
      !__hasOwnProp.call(to, key) && key !== except && __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
  return to;
};
var __toCommonJS = (mod) => __copyProps(__defProp({}, "__esModule", { value: !0 }), mod);
var operation_name_exports = {};
__export(operation_name_exports, {
  getSelectedOperationName: () => getSelectedOperationName
});
module.exports = __toCommonJS(operation_name_exports);
function getSelectedOperationName(prevOperations, prevSelectedOperationName, operations) {
  if (!operations || operations.length < 1)
    return;
  const names = operations.map((op) => {
    var _a;
    return (_a = op.name) == null ? void 0 : _a.value;
  });
  if (prevSelectedOperationName && names.includes(prevSelectedOperationName))
    return prevSelectedOperationName;
  if (prevSelectedOperationName && prevOperations) {
    const prevIndex = prevOperations.map((op) => {
      var _a;
      return (_a = op.name) == null ? void 0 : _a.value;
    }).indexOf(prevSelectedOperationName);
    if (prevIndex !== -1 && prevIndex < names.length)
      return names[prevIndex];
  }
  return names[0];
}
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  getSelectedOperationName
});
