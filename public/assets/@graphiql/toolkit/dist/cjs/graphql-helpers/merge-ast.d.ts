import { DocumentNode, GraphQLSchema } from 'graphql';

/**
 * Given a document AST, inline all named fragment definitions.
 */
declare function mergeAst(documentAST: DocumentNode, schema?: GraphQLSchema | null): DocumentNode;

export { mergeAst };
