import { OperationDefinitionNode } from 'graphql';

/**
 * Provided optional previous operations and selected name, and a next list of
 * operations, determine what the next selected operation should be.
 */
declare function getSelectedOperationName(prevOperations?: OperationDefinitionNode[], prevSelectedOperationName?: string, operations?: OperationDefinitionNode[]): string | undefined;

export { getSelectedOperationName };
