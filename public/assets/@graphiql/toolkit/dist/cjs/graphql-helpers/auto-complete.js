"use strict";
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: !0 });
}, __copyProps = (to, from, except, desc) => {
  if (from && typeof from == "object" || typeof from == "function")
    for (let key of __getOwnPropNames(from))
      !__hasOwnProp.call(to, key) && key !== except && __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
  return to;
};
var __toCommonJS = (mod) => __copyProps(__defProp({}, "__esModule", { value: !0 }), mod);
var auto_complete_exports = {};
__export(auto_complete_exports, {
  fillLeafs: () => fillLeafs
});
module.exports = __toCommonJS(auto_complete_exports);
var import_graphql = require("graphql");
function fillLeafs(schema, docString, getDefaultFieldNames) {
  const insertions = [];
  if (!schema || !docString)
    return { insertions, result: docString };
  let ast;
  try {
    ast = (0, import_graphql.parse)(docString);
  } catch (e) {
    return { insertions, result: docString };
  }
  const fieldNameFn = getDefaultFieldNames || defaultGetDefaultFieldNames, typeInfo = new import_graphql.TypeInfo(schema);
  return (0, import_graphql.visit)(ast, {
    leave(node) {
      typeInfo.leave(node);
    },
    enter(node) {
      if (typeInfo.enter(node), node.kind === "Field" && !node.selectionSet) {
        const fieldType = typeInfo.getType(), selectionSet = buildSelectionSet(
          isFieldType(fieldType),
          fieldNameFn
        );
        if (selectionSet && node.loc) {
          const indent = getIndentation(docString, node.loc.start);
          insertions.push({
            index: node.loc.end,
            string: " " + (0, import_graphql.print)(selectionSet).replaceAll(`
`, `
` + indent)
          });
        }
      }
    }
  }), {
    insertions,
    result: withInsertions(docString, insertions)
  };
}
function defaultGetDefaultFieldNames(type) {
  if (!("getFields" in type))
    return [];
  const fields = type.getFields();
  if (fields.id)
    return ["id"];
  if (fields.edges)
    return ["edges"];
  if (fields.node)
    return ["node"];
  const leafFieldNames = [];
  for (const fieldName of Object.keys(fields))
    (0, import_graphql.isLeafType)(fields[fieldName].type) && leafFieldNames.push(fieldName);
  return leafFieldNames;
}
function buildSelectionSet(type, getDefaultFieldNames) {
  const namedType = (0, import_graphql.getNamedType)(type);
  if (!type || (0, import_graphql.isLeafType)(type))
    return;
  const fieldNames = getDefaultFieldNames(namedType);
  if (!(!Array.isArray(fieldNames) || fieldNames.length === 0 || !("getFields" in namedType)))
    return {
      kind: import_graphql.Kind.SELECTION_SET,
      selections: fieldNames.map((fieldName) => {
        const fieldDef = namedType.getFields()[fieldName], fieldType = fieldDef ? fieldDef.type : null;
        return {
          kind: import_graphql.Kind.FIELD,
          name: {
            kind: import_graphql.Kind.NAME,
            value: fieldName
          },
          // we can use as here, because we already know that fieldType
          // comes from an origin parameter
          selectionSet: buildSelectionSet(fieldType, getDefaultFieldNames)
        };
      })
    };
}
function withInsertions(initial, insertions) {
  if (insertions.length === 0)
    return initial;
  let edited = "", prevIndex = 0;
  for (const { index, string } of insertions)
    edited += initial.slice(prevIndex, index) + string, prevIndex = index;
  return edited += initial.slice(prevIndex), edited;
}
function getIndentation(str, index) {
  let indentStart = index, indentEnd = index;
  for (; indentStart; ) {
    const c = str.charCodeAt(indentStart - 1);
    if (c === 10 || c === 13 || c === 8232 || c === 8233)
      break;
    indentStart--, c !== 9 && c !== 11 && c !== 12 && c !== 32 && c !== 160 && (indentEnd = indentStart);
  }
  return str.slice(indentStart, indentEnd);
}
function isFieldType(fieldType) {
  if (fieldType)
    return fieldType;
}
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  fillLeafs
});
