"use strict";
var __defProp = Object.defineProperty, __defProps = Object.defineProperties, __getOwnPropDesc = Object.getOwnPropertyDescriptor, __getOwnPropDescs = Object.getOwnPropertyDescriptors, __getOwnPropNames = Object.getOwnPropertyNames, __getOwnPropSymbols = Object.getOwnPropertySymbols;
var __hasOwnProp = Object.prototype.hasOwnProperty, __propIsEnum = Object.prototype.propertyIsEnumerable;
var __defNormalProp = (obj, key, value) => key in obj ? __defProp(obj, key, { enumerable: !0, configurable: !0, writable: !0, value }) : obj[key] = value, __spreadValues = (a, b) => {
  for (var prop in b || (b = {}))
    __hasOwnProp.call(b, prop) && __defNormalProp(a, prop, b[prop]);
  if (__getOwnPropSymbols)
    for (var prop of __getOwnPropSymbols(b))
      __propIsEnum.call(b, prop) && __defNormalProp(a, prop, b[prop]);
  return a;
}, __spreadProps = (a, b) => __defProps(a, __getOwnPropDescs(b));
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: !0 });
}, __copyProps = (to, from, except, desc) => {
  if (from && typeof from == "object" || typeof from == "function")
    for (let key of __getOwnPropNames(from))
      !__hasOwnProp.call(to, key) && key !== except && __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
  return to;
};
var __toCommonJS = (mod) => __copyProps(__defProp({}, "__esModule", { value: !0 }), mod);
var merge_ast_exports = {};
__export(merge_ast_exports, {
  mergeAst: () => mergeAst
});
module.exports = __toCommonJS(merge_ast_exports);
var import_graphql = require("graphql");
function uniqueBy(array, iteratee) {
  var _a;
  const FilteredMap = /* @__PURE__ */ new Map(), result = [];
  for (const item of array)
    if (item.kind === "Field") {
      const uniqueValue = iteratee(item), existing = FilteredMap.get(uniqueValue);
      if ((_a = item.directives) != null && _a.length) {
        const itemClone = __spreadValues({}, item);
        result.push(itemClone);
      } else if (existing != null && existing.selectionSet && item.selectionSet)
        existing.selectionSet.selections = [
          ...existing.selectionSet.selections,
          ...item.selectionSet.selections
        ];
      else if (!existing) {
        const itemClone = __spreadValues({}, item);
        FilteredMap.set(uniqueValue, itemClone), result.push(itemClone);
      }
    } else
      result.push(item);
  return result;
}
function inlineRelevantFragmentSpreads(fragmentDefinitions, selections, selectionSetType) {
  var _a;
  const selectionSetTypeName = selectionSetType ? (0, import_graphql.getNamedType)(selectionSetType).name : null, outputSelections = [], seenSpreads = [];
  for (let selection of selections) {
    if (selection.kind === "FragmentSpread") {
      const fragmentName = selection.name.value;
      if (!selection.directives || selection.directives.length === 0) {
        if (seenSpreads.includes(fragmentName))
          continue;
        seenSpreads.push(fragmentName);
      }
      const fragmentDefinition = fragmentDefinitions[selection.name.value];
      if (fragmentDefinition) {
        const { typeCondition, directives, selectionSet } = fragmentDefinition;
        selection = {
          kind: import_graphql.Kind.INLINE_FRAGMENT,
          typeCondition,
          directives,
          selectionSet
        };
      }
    }
    if (selection.kind === import_graphql.Kind.INLINE_FRAGMENT && // Cannot inline if there are directives
    (!selection.directives || ((_a = selection.directives) == null ? void 0 : _a.length) === 0)) {
      const fragmentTypeName = selection.typeCondition ? selection.typeCondition.name.value : null;
      if (!fragmentTypeName || fragmentTypeName === selectionSetTypeName) {
        outputSelections.push(
          ...inlineRelevantFragmentSpreads(
            fragmentDefinitions,
            selection.selectionSet.selections,
            selectionSetType
          )
        );
        continue;
      }
    }
    outputSelections.push(selection);
  }
  return outputSelections;
}
function mergeAst(documentAST, schema) {
  const typeInfo = schema ? new import_graphql.TypeInfo(schema) : null, fragmentDefinitions = /* @__PURE__ */ Object.create(null);
  for (const definition of documentAST.definitions)
    definition.kind === import_graphql.Kind.FRAGMENT_DEFINITION && (fragmentDefinitions[definition.name.value] = definition);
  const flattenVisitors = {
    SelectionSet(node) {
      const selectionSetType = typeInfo ? typeInfo.getParentType() : null;
      let { selections } = node;
      return selections = inlineRelevantFragmentSpreads(
        fragmentDefinitions,
        selections,
        selectionSetType
      ), __spreadProps(__spreadValues({}, node), {
        selections
      });
    },
    FragmentDefinition() {
      return null;
    }
  }, flattenedAST = (0, import_graphql.visit)(
    documentAST,
    typeInfo ? (0, import_graphql.visitWithTypeInfo)(typeInfo, flattenVisitors) : flattenVisitors
  );
  return (0, import_graphql.visit)(flattenedAST, {
    SelectionSet(node) {
      let { selections } = node;
      return selections = uniqueBy(
        selections,
        (selection) => selection.alias ? selection.alias.value : selection.name.value
      ), __spreadProps(__spreadValues({}, node), {
        selections
      });
    },
    FragmentDefinition() {
      return null;
    }
  });
}
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  mergeAst
});
