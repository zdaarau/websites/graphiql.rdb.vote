declare function formatError(error: unknown): string;
declare function formatResult(result: any): string;

export { formatError, formatResult };
