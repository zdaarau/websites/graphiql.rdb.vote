"use strict";
var __defProp = Object.defineProperty, __defProps = Object.defineProperties, __getOwnPropDesc = Object.getOwnPropertyDescriptor, __getOwnPropDescs = Object.getOwnPropertyDescriptors, __getOwnPropNames = Object.getOwnPropertyNames, __getOwnPropSymbols = Object.getOwnPropertySymbols;
var __hasOwnProp = Object.prototype.hasOwnProperty, __propIsEnum = Object.prototype.propertyIsEnumerable;
var __defNormalProp = (obj, key, value) => key in obj ? __defProp(obj, key, { enumerable: !0, configurable: !0, writable: !0, value }) : obj[key] = value, __spreadValues = (a, b) => {
  for (var prop in b || (b = {}))
    __hasOwnProp.call(b, prop) && __defNormalProp(a, prop, b[prop]);
  if (__getOwnPropSymbols)
    for (var prop of __getOwnPropSymbols(b))
      __propIsEnum.call(b, prop) && __defNormalProp(a, prop, b[prop]);
  return a;
}, __spreadProps = (a, b) => __defProps(a, __getOwnPropDescs(b));
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: !0 });
}, __copyProps = (to, from, except, desc) => {
  if (from && typeof from == "object" || typeof from == "function")
    for (let key of __getOwnPropNames(from))
      !__hasOwnProp.call(to, key) && key !== except && __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
  return to;
};
var __toCommonJS = (mod) => __copyProps(__defProp({}, "__esModule", { value: !0 }), mod);
var format_exports = {};
__export(format_exports, {
  formatError: () => formatError,
  formatResult: () => formatResult
});
module.exports = __toCommonJS(format_exports);
function stringify(obj) {
  return JSON.stringify(obj, null, 2);
}
function formatSingleError(error) {
  return __spreadProps(__spreadValues({}, error), {
    // Raise these details even if they're non-enumerable
    message: error.message,
    stack: error.stack
  });
}
function handleSingleError(error) {
  return error instanceof Error ? formatSingleError(error) : error;
}
function formatError(error) {
  return Array.isArray(error) ? stringify({
    errors: error.map((e) => handleSingleError(e))
  }) : stringify({ errors: [handleSingleError(error)] });
}
function formatResult(result) {
  return stringify(result);
}
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  formatError,
  formatResult
});
