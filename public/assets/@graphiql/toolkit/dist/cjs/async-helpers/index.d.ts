import { Observable, FetcherReturnType, FetcherResult } from '../create-fetcher/types.js';
import 'graphql';
import 'graphql-ws';

declare function isPromise<T>(value: Promise<T> | any): value is Promise<T>;
declare function isObservable<T>(value: any): value is Observable<T>;
declare function isAsyncIterable(input: unknown): input is AsyncIterable<unknown>;
declare function fetcherReturnToPromise(fetcherResult: FetcherReturnType): Promise<FetcherResult>;

export { fetcherReturnToPromise, isAsyncIterable, isObservable, isPromise };
