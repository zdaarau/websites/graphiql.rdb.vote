"use strict";
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: !0 });
}, __copyProps = (to, from, except, desc) => {
  if (from && typeof from == "object" || typeof from == "function")
    for (let key of __getOwnPropNames(from))
      !__hasOwnProp.call(to, key) && key !== except && __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
  return to;
};
var __toCommonJS = (mod) => __copyProps(__defProp({}, "__esModule", { value: !0 }), mod);
var async_helpers_exports = {};
__export(async_helpers_exports, {
  fetcherReturnToPromise: () => fetcherReturnToPromise,
  isAsyncIterable: () => isAsyncIterable,
  isObservable: () => isObservable,
  isPromise: () => isPromise
});
module.exports = __toCommonJS(async_helpers_exports);
function isPromise(value) {
  return typeof value == "object" && value !== null && typeof value.then == "function";
}
function observableToPromise(observable) {
  return new Promise((resolve, reject) => {
    const subscription = observable.subscribe({
      next(v) {
        resolve(v), subscription.unsubscribe();
      },
      error: reject,
      complete() {
        reject(new Error("no value resolved"));
      }
    });
  });
}
function isObservable(value) {
  return typeof value == "object" && value !== null && "subscribe" in value && typeof value.subscribe == "function";
}
function isAsyncIterable(input) {
  return typeof input == "object" && input !== null && // Some browsers still don't have Symbol.asyncIterator implemented (iOS Safari)
  // That means every custom AsyncIterable must be built using a AsyncGeneratorFunction (async function * () {})
  (input[Symbol.toStringTag] === "AsyncGenerator" || Symbol.asyncIterator in input);
}
async function asyncIterableToPromise(input) {
  var _a;
  const iteratorReturn = (_a = ("return" in input ? input : input[Symbol.asyncIterator]()).return) == null ? void 0 : _a.bind(input), result = await ("next" in input ? input : input[Symbol.asyncIterator]()).next.bind(input)();
  return iteratorReturn == null || iteratorReturn(), result.value;
}
async function fetcherReturnToPromise(fetcherResult) {
  const result = await fetcherResult;
  return isAsyncIterable(result) ? asyncIterableToPromise(result) : isObservable(result) ? observableToPromise(result) : result;
}
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  fetcherReturnToPromise,
  isAsyncIterable,
  isObservable,
  isPromise
});
