/// <reference types="react" />
import { GraphQLNamedType, GraphQLType } from 'graphql';
export declare function renderType(type: GraphQLType, renderNamedType: (namedType: GraphQLNamedType) => JSX.Element): JSX.Element;
