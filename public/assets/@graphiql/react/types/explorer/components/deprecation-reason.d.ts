import './deprecation-reason.css';
declare type DeprecationReasonProps = {
    /**
     * The deprecation reason as markdown string.
     */
    children?: string | null;
    preview?: boolean;
};
export declare function DeprecationReason(props: DeprecationReasonProps): import("react/jsx-runtime").JSX.Element | null;
export {};
