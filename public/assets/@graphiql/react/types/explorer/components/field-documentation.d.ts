import { ExplorerFieldDef } from '../context';
declare type FieldDocumentationProps = {
    /**
     * The field or argument that should be rendered.
     */
    field: ExplorerFieldDef;
};
export declare function FieldDocumentation(props: FieldDocumentationProps): import("react/jsx-runtime").JSX.Element;
export {};
