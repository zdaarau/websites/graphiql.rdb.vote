import { ExplorerFieldDef } from '../context';
import './field-link.css';
declare type FieldLinkProps = {
    /**
     * The field or argument that should be linked to.
     */
    field: ExplorerFieldDef;
};
export declare function FieldLink(props: FieldLinkProps): import("react/jsx-runtime").JSX.Element;
export {};
