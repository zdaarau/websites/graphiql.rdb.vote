import { ExplorerFieldDef } from '../context';
import './default-value.css';
declare type DefaultValueProps = {
    /**
     * The field or argument for which to render the default value.
     */
    field: ExplorerFieldDef;
};
export declare function DefaultValue({ field }: DefaultValueProps): import("react/jsx-runtime").JSX.Element | null;
export {};
