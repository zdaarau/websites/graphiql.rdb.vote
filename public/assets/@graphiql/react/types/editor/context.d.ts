import { DocumentNode, FragmentDefinitionNode, OperationDefinitionNode, ValidationRule } from 'graphql';
import { VariableToType } from 'graphql-language-service';
import { ReactNode } from 'react';
import { TabDefinition, TabsState, TabState } from './tabs';
import { CodeMirrorEditor } from './types';
export declare type CodeMirrorEditorWithOperationFacts = CodeMirrorEditor & {
    documentAST: DocumentNode | null;
    operationName: string | null;
    operations: OperationDefinitionNode[] | null;
    variableToType: VariableToType | null;
};
export declare type EditorContextType = TabsState & {
    /**
     * Add a new tab.
     */
    addTab(): void;
    /**
     * Switch to a different tab.
     * @param index The index of the tab that should be switched to.
     */
    changeTab(index: number): void;
    /**
     * Move a tab to a new spot.
     * @param newOrder The new order for the tabs.
     */
    moveTab(newOrder: TabState[]): void;
    /**
     * Close a tab. If the currently active tab is closed, the tab before it will
     * become active. If there is no tab before the closed one, the tab after it
     * will become active.
     * @param index The index of the tab that should be closed.
     */
    closeTab(index: number): void;
    /**
     * Update the state for the tab that is currently active. This will be
     * reflected in the `tabs` object and the state will be persisted in storage
     * (if available).
     * @param partialTab A partial tab state object that will override the
     * current values. The properties `id`, `hash` and `title` cannot be changed.
     */
    updateActiveTabValues(partialTab: Partial<Omit<TabState, 'id' | 'hash' | 'title'>>): void;
    /**
     * The CodeMirror editor instance for the headers editor.
     */
    headerEditor: CodeMirrorEditor | null;
    /**
     * The CodeMirror editor instance for the query editor. This editor also
     * stores the operation facts that are derived from the current editor
     * contents.
     */
    queryEditor: CodeMirrorEditorWithOperationFacts | null;
    /**
     * The CodeMirror editor instance for the response editor.
     */
    responseEditor: CodeMirrorEditor | null;
    /**
     * The CodeMirror editor instance for the variables editor.
     */
    variableEditor: CodeMirrorEditor | null;
    /**
     * Set the CodeMirror editor instance for the headers editor.
     */
    setHeaderEditor(newEditor: CodeMirrorEditor): void;
    /**
     * Set the CodeMirror editor instance for the query editor.
     */
    setQueryEditor(newEditor: CodeMirrorEditorWithOperationFacts): void;
    /**
     * Set the CodeMirror editor instance for the response editor.
     */
    setResponseEditor(newEditor: CodeMirrorEditor): void;
    /**
     * Set the CodeMirror editor instance for the variables editor.
     */
    setVariableEditor(newEditor: CodeMirrorEditor): void;
    /**
     * Changes the operation name and invokes the `onEditOperationName` callback.
     */
    setOperationName(operationName: string): void;
    /**
     * The contents of the headers editor when initially rendering the provider
     * component.
     */
    initialHeaders: string;
    /**
     * The contents of the query editor when initially rendering the provider
     * component.
     */
    initialQuery: string;
    /**
     * The contents of the response editor when initially rendering the provider
     * component.
     */
    initialResponse: string;
    /**
     * The contents of the variables editor when initially rendering the provider
     * component.
     */
    initialVariables: string;
    /**
     * A map of fragment definitions using the fragment name as key which are
     * made available to include in the query.
     */
    externalFragments: Map<string, FragmentDefinitionNode>;
    /**
     * A list of custom validation rules that are run in addition to the rules
     * provided by the GraphQL spec.
     */
    validationRules: ValidationRule[];
    /**
     * If the contents of the headers editor are persisted in storage.
     */
    shouldPersistHeaders: boolean;
    /**
     * Changes if headers should be persisted.
     */
    setShouldPersistHeaders(persist: boolean): void;
};
export declare const EditorContext: import("react").Context<EditorContextType | null>;
export declare type EditorContextProviderProps = {
    children: ReactNode;
    /**
     * The initial contents of the query editor when loading GraphiQL and there
     * is no other source for the editor state. Other sources can be:
     * - The `query` prop
     * - The value persisted in storage
     * These default contents will only be used for the first tab. When opening
     * more tabs the query editor will start out empty.
     */
    defaultQuery?: string;
    /**
     * With this prop you can pass so-called "external" fragments that will be
     * included in the query document (depending on usage). You can either pass
     * the fragments using SDL (passing a string) or you can pass a list of
     * `FragmentDefinitionNode` objects.
     */
    externalFragments?: string | FragmentDefinitionNode[];
    /**
     * This prop can be used to set the contents of the headers editor. Every
     * time this prop changes, the contents of the headers editor are replaced.
     * Note that the editor contents can be changed in between these updates by
     * typing in the editor.
     */
    headers?: string;
    /**
     * This prop can be used to define the default set of tabs, with their
     * queries, variables, and headers. It will be used as default only if
     * there is no tab state persisted in storage.
     *
     * @example
     * ```tsx
     * <GraphiQL
     *   defaultTabs={[
     *     { query: 'query myExampleQuery {}' },
     *     { query: '{ id }' }
     *   ]}
     * />
     *```
     */
    defaultTabs?: TabDefinition[];
    /**
     * Invoked when the operation name changes. Possible triggers are:
     * - Editing the contents of the query editor
     * - Selecting a operation for execution in a document that contains multiple
     *   operation definitions
     * @param operationName The operation name after it has been changed.
     */
    onEditOperationName?(operationName: string): void;
    /**
     * Invoked when the state of the tabs changes. Possible triggers are:
     * - Updating any editor contents inside the currently active tab
     * - Adding a tab
     * - Switching to a different tab
     * - Closing a tab
     * @param tabState The tabs state after it has been updated.
     */
    onTabChange?(tabState: TabsState): void;
    /**
     * This prop can be used to set the contents of the query editor. Every time
     * this prop changes, the contents of the query editor are replaced. Note
     * that the editor contents can be changed in between these updates by typing
     * in the editor.
     */
    query?: string;
    /**
     * This prop can be used to set the contents of the response editor. Every
     * time this prop changes, the contents of the response editor are replaced.
     * Note that the editor contents can change in between these updates by
     * executing queries that will show a response.
     */
    response?: string;
    /**
     * This prop toggles if the contents of the headers editor are persisted in
     * storage.
     * @default false
     */
    shouldPersistHeaders?: boolean;
    /**
     * This prop accepts custom validation rules for GraphQL documents that are
     * run against the contents of the query editor (in addition to the rules
     * that are specified in the GraphQL spec).
     */
    validationRules?: ValidationRule[];
    /**
     * This prop can be used to set the contents of the variables editor. Every
     * time this prop changes, the contents of the variables editor are replaced.
     * Note that the editor contents can be changed in between these updates by
     * typing in the editor.
     */
    variables?: string;
    /**
     * Headers to be set when opening a new tab
     */
    defaultHeaders?: string;
};
export declare function EditorContextProvider(props: EditorContextProviderProps): import("react/jsx-runtime").JSX.Element;
export declare const useEditorContext: {
    (options: {
        nonNull: true;
        caller?: Function | undefined;
    }): EditorContextType;
    (options: {
        nonNull?: boolean | undefined;
        caller?: Function | undefined;
    }): EditorContextType | null;
    (): EditorContextType | null;
};
export declare const DEFAULT_QUERY = "# Welcome to GraphiQL\n#\n# GraphiQL is an in-browser tool for writing, validating, and\n# testing GraphQL queries.\n#\n# Type queries into this side of the screen, and you will see intelligent\n# typeaheads aware of the current GraphQL type schema and live syntax and\n# validation errors highlighted within the text.\n#\n# GraphQL queries typically start with a \"{\" character. Lines that start\n# with a # are ignored.\n#\n# An example GraphQL query might look like:\n#\n#     {\n#       field(arg: \"value\") {\n#         subField\n#       }\n#     }\n#\n# Keyboard shortcuts:\n#\n#   Prettify query:  Shift-Ctrl-P (or press the prettify button)\n#\n#  Merge fragments:  Shift-Ctrl-M (or press the merge button)\n#\n#        Run Query:  Ctrl-Enter (or press the play button)\n#\n#    Auto Complete:  Ctrl-Space (or just start typing)\n#\n\n";
