import { StorageAPI } from '@graphiql/toolkit';
import { CodeMirrorEditorWithOperationFacts } from './context';
import { CodeMirrorEditor } from './types';
export declare type TabDefinition = {
    /**
     * The contents of the query editor of this tab.
     */
    query: string | null;
    /**
     * The contents of the variable editor of this tab.
     */
    variables?: string | null;
    /**
     * The contents of the headers editor of this tab.
     */
    headers?: string | null;
};
/**
 * This object describes the state of a single tab.
 */
export declare type TabState = TabDefinition & {
    /**
     * A GUID value generated when the tab was created.
     */
    id: string;
    /**
     * A hash that is unique for a combination of the contents of the query
     * editor, the variable editor and the header editor (i.e. all the editor
     * where the contents are persisted in storage).
     */
    hash: string;
    /**
     * The title of the tab shown in the tab element.
     */
    title: string;
    /**
     * The operation name derived from the contents of the query editor of this
     * tab.
     */
    operationName: string | null;
    /**
     * The contents of the response editor of this tab.
     */
    response: string | null;
};
/**
 * This object describes the state of all tabs.
 */
export declare type TabsState = {
    /**
     * A list of state objects for each tab.
     */
    tabs: TabState[];
    /**
     * The index of the currently active tab with regards to the `tabs` list of
     * this object.
     */
    activeTabIndex: number;
};
export declare function getDefaultTabState({ defaultQuery, defaultHeaders, headers, defaultTabs, query, variables, storage, shouldPersistHeaders, }: {
    defaultQuery: string;
    defaultHeaders?: string;
    headers: string | null;
    defaultTabs?: TabDefinition[];
    query: string | null;
    variables: string | null;
    storage: StorageAPI | null;
    shouldPersistHeaders?: boolean;
}): TabsState;
export declare function useSynchronizeActiveTabValues({ queryEditor, variableEditor, headerEditor, responseEditor, }: {
    queryEditor: CodeMirrorEditorWithOperationFacts | null;
    variableEditor: CodeMirrorEditor | null;
    headerEditor: CodeMirrorEditor | null;
    responseEditor: CodeMirrorEditor | null;
}): (state: TabsState) => TabsState;
export declare function serializeTabState(tabState: TabsState, shouldPersistHeaders?: boolean): string;
export declare function useStoreTabs({ storage, shouldPersistHeaders, }: {
    storage: StorageAPI | null;
    shouldPersistHeaders?: boolean;
}): (currentState: TabsState) => void;
export declare function useSetEditorValues({ queryEditor, variableEditor, headerEditor, responseEditor, defaultHeaders, }: {
    queryEditor: CodeMirrorEditorWithOperationFacts | null;
    variableEditor: CodeMirrorEditor | null;
    headerEditor: CodeMirrorEditor | null;
    responseEditor: CodeMirrorEditor | null;
    defaultHeaders?: string;
}): ({ query, variables, headers, response, }: {
    query: string | null;
    variables?: string | null | undefined;
    headers?: string | null | undefined;
    response: string | null;
}) => void;
export declare function createTab({ query, variables, headers, }?: Partial<TabDefinition>): TabState;
export declare function setPropertiesInActiveTab(state: TabsState, partialTab: Partial<Omit<TabState, 'id' | 'hash' | 'title'>>): TabsState;
export declare function fuzzyExtractOperationName(str: string): string | null;
export declare function clearHeadersFromTabs(storage: StorageAPI | null): void;
export declare const STORAGE_KEY = "tabState";
