import { UseResponseEditorArgs } from '../response-editor';
import '../style/codemirror.css';
import '../style/fold.css';
import '../style/info.css';
import '../style/editor.css';
export declare function ResponseEditor(props: UseResponseEditorArgs): import("react/jsx-runtime").JSX.Element;
