import { jsx, jsxs, Fragment } from "react/jsx-runtime";
import * as React from "react";
import { createContext, useContext, useRef, useState, useEffect, forwardRef, useMemo, useCallback, useLayoutEffect } from "react";
import { clsx } from "clsx";
import { print, astFromValue, isSchema, buildClientSchema, validateSchema, getIntrospectionQuery, isNamedType, isObjectType, isInputObjectType, isScalarType, isEnumType, isInterfaceType, isUnionType, isNonNullType, isListType, isAbstractType, isType, parse, visit } from "graphql";
import { StorageAPI, HistoryStore, formatResult, isObservable, formatError, isAsyncIterable, fetcherReturnToPromise, isPromise, mergeAst, fillLeafs, getSelectedOperationName } from "@graphiql/toolkit";
import { getFragmentDependenciesForAST, GraphQLDocumentMode, getOperationFacts } from "graphql-language-service";
import setValue from "set-value";
import getValue from "get-value";
import copyToClipboard from "copy-to-clipboard";
import * as D from "@radix-ui/react-dialog";
import { Root } from "@radix-ui/react-visually-hidden";
import { Trigger, Root as Root$1, Portal, Content as Content$1, Item as Item$1 } from "@radix-ui/react-dropdown-menu";
import MarkdownIt from "markdown-it";
import { Reorder } from "framer-motion";
import * as T from "@radix-ui/react-tooltip";
import { Combobox } from "@headlessui/react";
import ReactDOM from "react-dom";
function createNullableContext(name) {
  const context = createContext(null);
  context.displayName = name;
  return context;
}
function createContextHook(context) {
  function useGivenContext(options) {
    var _a;
    const value = useContext(context);
    if (value === null && (options == null ? void 0 : options.nonNull)) {
      throw new Error(
        `Tried to use \`${((_a = options.caller) == null ? void 0 : _a.name) || useGivenContext.caller.name}\` without the necessary context. Make sure to render the \`${context.displayName}Provider\` component higher up the tree.`
      );
    }
    return value;
  }
  Object.defineProperty(useGivenContext, "name", {
    value: `use${context.displayName}`
  });
  return useGivenContext;
}
const StorageContext = createNullableContext("StorageContext");
function StorageContextProvider(props) {
  const isInitialRender = useRef(true);
  const [storage, setStorage] = useState(() => new StorageAPI(props.storage));
  useEffect(() => {
    if (isInitialRender.current) {
      isInitialRender.current = false;
    } else {
      setStorage(new StorageAPI(props.storage));
    }
  }, [props.storage]);
  return /* @__PURE__ */ jsx(StorageContext.Provider, { value: storage, children: props.children });
}
const useStorageContext = createContextHook(StorageContext);
const SvgArgument = ({
  title,
  titleId,
  ...props
}) => /* @__PURE__ */ React.createElement("svg", { height: "1em", viewBox: "0 0 14 14", fill: "none", xmlns: "http://www.w3.org/2000/svg", "aria-labelledby": titleId, ...props }, title ? /* @__PURE__ */ React.createElement("title", { id: titleId }, title) : null, /* @__PURE__ */ React.createElement("path", { d: "M5.0484 1.40838C6.12624 0.33054 7.87376 0.330541 8.9516 1.40838L12.5916 5.0484C13.6695 6.12624 13.6695 7.87376 12.5916 8.9516L8.9516 12.5916C7.87376 13.6695 6.12624 13.6695 5.0484 12.5916L1.40838 8.9516C0.33054 7.87376 0.330541 6.12624 1.40838 5.0484L5.0484 1.40838Z", stroke: "currentColor", strokeWidth: 1.2 }), /* @__PURE__ */ React.createElement("rect", { x: 6, y: 6, width: 2, height: 2, rx: 1, fill: "currentColor" }));
const SvgChevronDown = ({
  title,
  titleId,
  ...props
}) => /* @__PURE__ */ React.createElement("svg", { height: "1em", viewBox: "0 0 14 9", fill: "none", xmlns: "http://www.w3.org/2000/svg", "aria-labelledby": titleId, ...props }, title ? /* @__PURE__ */ React.createElement("title", { id: titleId }, title) : null, /* @__PURE__ */ React.createElement("path", { d: "M1 1L7 7L13 1", stroke: "currentColor", strokeWidth: 1.5 }));
const SvgChevronLeft = ({
  title,
  titleId,
  ...props
}) => /* @__PURE__ */ React.createElement("svg", { height: "1em", viewBox: "0 0 7 10", fill: "none", xmlns: "http://www.w3.org/2000/svg", "aria-labelledby": titleId, ...props }, title ? /* @__PURE__ */ React.createElement("title", { id: titleId }, title) : null, /* @__PURE__ */ React.createElement("path", { d: "M6 1.04819L2 5.04819L6 9.04819", stroke: "currentColor", strokeWidth: 1.75 }));
const SvgChevronUp = ({
  title,
  titleId,
  ...props
}) => /* @__PURE__ */ React.createElement("svg", { height: "1em", viewBox: "0 0 14 9", fill: "none", xmlns: "http://www.w3.org/2000/svg", "aria-labelledby": titleId, ...props }, title ? /* @__PURE__ */ React.createElement("title", { id: titleId }, title) : null, /* @__PURE__ */ React.createElement("path", { d: "M13 8L7 2L1 8", stroke: "currentColor", strokeWidth: 1.5 }));
const SvgClose = ({
  title,
  titleId,
  ...props
}) => /* @__PURE__ */ React.createElement("svg", { height: "1em", viewBox: "0 0 14 14", stroke: "currentColor", strokeWidth: 3, xmlns: "http://www.w3.org/2000/svg", "aria-labelledby": titleId, ...props }, title ? /* @__PURE__ */ React.createElement("title", { id: titleId }, title) : null, /* @__PURE__ */ React.createElement("path", { d: "M1 1L12.9998 12.9997" }), /* @__PURE__ */ React.createElement("path", { d: "M13 1L1.00079 13.0003" }));
const SvgCopy = ({
  title,
  titleId,
  ...props
}) => /* @__PURE__ */ React.createElement("svg", { height: "1em", viewBox: "-2 -2 22 22", fill: "none", xmlns: "http://www.w3.org/2000/svg", "aria-labelledby": titleId, ...props }, title ? /* @__PURE__ */ React.createElement("title", { id: titleId }, title) : null, /* @__PURE__ */ React.createElement("path", { d: "M11.25 14.2105V15.235C11.25 16.3479 10.3479 17.25 9.23501 17.25H2.76499C1.65214 17.25 0.75 16.3479 0.75 15.235L0.75 8.76499C0.75 7.65214 1.65214 6.75 2.76499 6.75L3.78947 6.75", stroke: "currentColor", strokeWidth: 1.5 }), /* @__PURE__ */ React.createElement("rect", { x: 6.75, y: 0.75, width: 10.5, height: 10.5, rx: 2.2069, stroke: "currentColor", strokeWidth: 1.5 }));
const SvgDeprecatedArgument = ({
  title,
  titleId,
  ...props
}) => /* @__PURE__ */ React.createElement("svg", { height: "1em", viewBox: "0 0 14 14", fill: "none", xmlns: "http://www.w3.org/2000/svg", "aria-labelledby": titleId, ...props }, title ? /* @__PURE__ */ React.createElement("title", { id: titleId }, title) : null, /* @__PURE__ */ React.createElement("path", { d: "M5.0484 1.40838C6.12624 0.33054 7.87376 0.330541 8.9516 1.40838L12.5916 5.0484C13.6695 6.12624 13.6695 7.87376 12.5916 8.9516L8.9516 12.5916C7.87376 13.6695 6.12624 13.6695 5.0484 12.5916L1.40838 8.9516C0.33054 7.87376 0.330541 6.12624 1.40838 5.0484L5.0484 1.40838Z", stroke: "currentColor", strokeWidth: 1.2 }), /* @__PURE__ */ React.createElement("path", { d: "M5 9L9 5", stroke: "currentColor", strokeWidth: 1.2 }), /* @__PURE__ */ React.createElement("path", { d: "M5 5L9 9", stroke: "currentColor", strokeWidth: 1.2 }));
const SvgDeprecatedEnumValue = ({
  title,
  titleId,
  ...props
}) => /* @__PURE__ */ React.createElement("svg", { height: "1em", viewBox: "0 0 12 12", fill: "none", xmlns: "http://www.w3.org/2000/svg", "aria-labelledby": titleId, ...props }, title ? /* @__PURE__ */ React.createElement("title", { id: titleId }, title) : null, /* @__PURE__ */ React.createElement("path", { d: "M4 8L8 4", stroke: "currentColor", strokeWidth: 1.2 }), /* @__PURE__ */ React.createElement("path", { d: "M4 4L8 8", stroke: "currentColor", strokeWidth: 1.2 }), /* @__PURE__ */ React.createElement("path", { fillRule: "evenodd", clipRule: "evenodd", d: "M8.5 1.2H9C9.99411 1.2 10.8 2.00589 10.8 3V9C10.8 9.99411 9.99411 10.8 9 10.8H8.5V12H9C10.6569 12 12 10.6569 12 9V3C12 1.34315 10.6569 0 9 0H8.5V1.2ZM3.5 1.2V0H3C1.34315 0 0 1.34315 0 3V9C0 10.6569 1.34315 12 3 12H3.5V10.8H3C2.00589 10.8 1.2 9.99411 1.2 9V3C1.2 2.00589 2.00589 1.2 3 1.2H3.5Z", fill: "currentColor" }));
const SvgDeprecatedField = ({
  title,
  titleId,
  ...props
}) => /* @__PURE__ */ React.createElement("svg", { height: "1em", viewBox: "0 0 12 12", fill: "none", xmlns: "http://www.w3.org/2000/svg", "aria-labelledby": titleId, ...props }, title ? /* @__PURE__ */ React.createElement("title", { id: titleId }, title) : null, /* @__PURE__ */ React.createElement("rect", { x: 0.6, y: 0.6, width: 10.8, height: 10.8, rx: 3.4, stroke: "currentColor", strokeWidth: 1.2 }), /* @__PURE__ */ React.createElement("path", { d: "M4 8L8 4", stroke: "currentColor", strokeWidth: 1.2 }), /* @__PURE__ */ React.createElement("path", { d: "M4 4L8 8", stroke: "currentColor", strokeWidth: 1.2 }));
const SvgDirective = ({
  title,
  titleId,
  ...props
}) => /* @__PURE__ */ React.createElement("svg", { height: "1em", viewBox: "0 0.5 12 12", xmlns: "http://www.w3.org/2000/svg", "aria-labelledby": titleId, ...props }, title ? /* @__PURE__ */ React.createElement("title", { id: titleId }, title) : null, /* @__PURE__ */ React.createElement("rect", { x: 7, y: 5.5, width: 2, height: 2, rx: 1, transform: "rotate(90 7 5.5)", fill: "currentColor" }), /* @__PURE__ */ React.createElement("path", { fillRule: "evenodd", clipRule: "evenodd", d: "M10.8 9L10.8 9.5C10.8 10.4941 9.99411 11.3 9 11.3L3 11.3C2.00589 11.3 1.2 10.4941 1.2 9.5L1.2 9L-3.71547e-07 9L-3.93402e-07 9.5C-4.65826e-07 11.1569 1.34314 12.5 3 12.5L9 12.5C10.6569 12.5 12 11.1569 12 9.5L12 9L10.8 9ZM10.8 4L12 4L12 3.5C12 1.84315 10.6569 0.5 9 0.5L3 0.5C1.34315 0.5 -5.87117e-08 1.84315 -1.31135e-07 3.5L-1.5299e-07 4L1.2 4L1.2 3.5C1.2 2.50589 2.00589 1.7 3 1.7L9 1.7C9.99411 1.7 10.8 2.50589 10.8 3.5L10.8 4Z", fill: "currentColor" }));
const SvgDocsFilled = ({
  title,
  titleId,
  ...props
}) => /* @__PURE__ */ React.createElement("svg", { height: "1em", viewBox: "0 0 20 24", fill: "none", xmlns: "http://www.w3.org/2000/svg", "aria-labelledby": titleId, ...props }, title ? /* @__PURE__ */ React.createElement("title", { id: titleId }, title) : null, /* @__PURE__ */ React.createElement("path", { d: "M0.75 3C0.75 1.75736 1.75736 0.75 3 0.75H17.25C17.8023 0.75 18.25 1.19772 18.25 1.75V5.25", stroke: "currentColor", strokeWidth: 1.5 }), /* @__PURE__ */ React.createElement("path", { d: "M0.75 3C0.75 4.24264 1.75736 5.25 3 5.25H18.25C18.8023 5.25 19.25 5.69771 19.25 6.25V22.25C19.25 22.8023 18.8023 23.25 18.25 23.25H3C1.75736 23.25 0.75 22.2426 0.75 21V3Z", stroke: "currentColor", strokeWidth: 1.5 }), /* @__PURE__ */ React.createElement("path", { fillRule: "evenodd", clipRule: "evenodd", d: "M3 5.25C1.75736 5.25 0.75 4.24264 0.75 3V21C0.75 22.2426 1.75736 23.25 3 23.25H18.25C18.8023 23.25 19.25 22.8023 19.25 22.25V6.25C19.25 5.69771 18.8023 5.25 18.25 5.25H3ZM13 11L6 11V12.5L13 12.5V11Z", fill: "currentColor" }));
const SvgDocs = ({
  title,
  titleId,
  ...props
}) => /* @__PURE__ */ React.createElement("svg", { height: "1em", viewBox: "0 0 20 24", fill: "none", xmlns: "http://www.w3.org/2000/svg", "aria-labelledby": titleId, ...props }, title ? /* @__PURE__ */ React.createElement("title", { id: titleId }, title) : null, /* @__PURE__ */ React.createElement("path", { d: "M0.75 3C0.75 4.24264 1.75736 5.25 3 5.25H17.25M0.75 3C0.75 1.75736 1.75736 0.75 3 0.75H16.25C16.8023 0.75 17.25 1.19772 17.25 1.75V5.25M0.75 3V21C0.75 22.2426 1.75736 23.25 3 23.25H18.25C18.8023 23.25 19.25 22.8023 19.25 22.25V6.25C19.25 5.69771 18.8023 5.25 18.25 5.25H17.25", stroke: "currentColor", strokeWidth: 1.5 }), /* @__PURE__ */ React.createElement("line", { x1: 13, y1: 11.75, x2: 6, y2: 11.75, stroke: "currentColor", strokeWidth: 1.5 }));
const SvgEnumValue = ({
  title,
  titleId,
  ...props
}) => /* @__PURE__ */ React.createElement("svg", { height: "1em", viewBox: "0 0 12 12", fill: "none", xmlns: "http://www.w3.org/2000/svg", "aria-labelledby": titleId, ...props }, title ? /* @__PURE__ */ React.createElement("title", { id: titleId }, title) : null, /* @__PURE__ */ React.createElement("rect", { x: 5, y: 5, width: 2, height: 2, rx: 1, fill: "currentColor" }), /* @__PURE__ */ React.createElement("path", { fillRule: "evenodd", clipRule: "evenodd", d: "M8.5 1.2H9C9.99411 1.2 10.8 2.00589 10.8 3V9C10.8 9.99411 9.99411 10.8 9 10.8H8.5V12H9C10.6569 12 12 10.6569 12 9V3C12 1.34315 10.6569 0 9 0H8.5V1.2ZM3.5 1.2V0H3C1.34315 0 0 1.34315 0 3V9C0 10.6569 1.34315 12 3 12H3.5V10.8H3C2.00589 10.8 1.2 9.99411 1.2 9V3C1.2 2.00589 2.00589 1.2 3 1.2H3.5Z", fill: "currentColor" }));
const SvgField = ({
  title,
  titleId,
  ...props
}) => /* @__PURE__ */ React.createElement("svg", { height: "1em", viewBox: "0 0 12 13", fill: "none", xmlns: "http://www.w3.org/2000/svg", "aria-labelledby": titleId, ...props }, title ? /* @__PURE__ */ React.createElement("title", { id: titleId }, title) : null, /* @__PURE__ */ React.createElement("rect", { x: 0.6, y: 1.1, width: 10.8, height: 10.8, rx: 2.4, stroke: "currentColor", strokeWidth: 1.2 }), /* @__PURE__ */ React.createElement("rect", { x: 5, y: 5.5, width: 2, height: 2, rx: 1, fill: "currentColor" }));
const SvgHistory = ({
  title,
  titleId,
  ...props
}) => /* @__PURE__ */ React.createElement("svg", { height: "1em", viewBox: "0 0 24 20", fill: "none", xmlns: "http://www.w3.org/2000/svg", "aria-labelledby": titleId, ...props }, title ? /* @__PURE__ */ React.createElement("title", { id: titleId }, title) : null, /* @__PURE__ */ React.createElement("path", { d: "M1.59375 9.52344L4.87259 12.9944L8.07872 9.41249", stroke: "currentColor", strokeWidth: 1.5, strokeLinecap: "square" }), /* @__PURE__ */ React.createElement("path", { d: "M13.75 5.25V10.75H18.75", stroke: "currentColor", strokeWidth: 1.5, strokeLinecap: "square" }), /* @__PURE__ */ React.createElement("path", { d: "M4.95427 11.9332C4.55457 10.0629 4.74441 8.11477 5.49765 6.35686C6.25089 4.59894 7.5305 3.11772 9.16034 2.11709C10.7902 1.11647 12.6901 0.645626 14.5986 0.769388C16.5071 0.893151 18.3303 1.60543 19.8172 2.80818C21.3042 4.01093 22.3818 5.64501 22.9017 7.48548C23.4216 9.32595 23.3582 11.2823 22.7203 13.0853C22.0824 14.8883 20.9013 16.4492 19.3396 17.5532C17.778 18.6572 15.9125 19.25 14 19.25", stroke: "currentColor", strokeWidth: 1.5 }));
const SvgImplements = ({
  title,
  titleId,
  ...props
}) => /* @__PURE__ */ React.createElement("svg", { height: "1em", viewBox: "0 0 12 12", fill: "none", xmlns: "http://www.w3.org/2000/svg", "aria-labelledby": titleId, ...props }, title ? /* @__PURE__ */ React.createElement("title", { id: titleId }, title) : null, /* @__PURE__ */ React.createElement("circle", { cx: 6, cy: 6, r: 5.4, stroke: "currentColor", strokeWidth: 1.2, strokeDasharray: "4.241025 4.241025", transform: "rotate(22.5)", "transform-origin": "center" }), /* @__PURE__ */ React.createElement("circle", { cx: 6, cy: 6, r: 1, fill: "currentColor" }));
const SvgKeyboardShortcut = ({
  title,
  titleId,
  ...props
}) => /* @__PURE__ */ React.createElement("svg", { height: "1em", viewBox: "0 0 19 18", fill: "none", xmlns: "http://www.w3.org/2000/svg", "aria-labelledby": titleId, ...props }, title ? /* @__PURE__ */ React.createElement("title", { id: titleId }, title) : null, /* @__PURE__ */ React.createElement("path", { d: "M1.5 14.5653C1.5 15.211 1.75652 15.8303 2.21314 16.2869C2.66975 16.7435 3.28905 17 3.9348 17C4.58054 17 5.19984 16.7435 5.65646 16.2869C6.11307 15.8303 6.36959 15.211 6.36959 14.5653V12.1305H3.9348C3.28905 12.1305 2.66975 12.387 2.21314 12.8437C1.75652 13.3003 1.5 13.9195 1.5 14.5653Z", stroke: "currentColor", strokeWidth: 1.125, strokeLinecap: "round", strokeLinejoin: "round" }), /* @__PURE__ */ React.createElement("path", { d: "M3.9348 1.00063C3.28905 1.00063 2.66975 1.25715 2.21314 1.71375C1.75652 2.17035 1.5 2.78964 1.5 3.43537C1.5 4.0811 1.75652 4.70038 2.21314 5.15698C2.66975 5.61358 3.28905 5.8701 3.9348 5.8701H6.36959V3.43537C6.36959 2.78964 6.11307 2.17035 5.65646 1.71375C5.19984 1.25715 4.58054 1.00063 3.9348 1.00063Z", stroke: "currentColor", strokeWidth: 1.125, strokeLinecap: "round", strokeLinejoin: "round" }), /* @__PURE__ */ React.createElement("path", { d: "M15.0652 12.1305H12.6304V14.5653C12.6304 15.0468 12.7732 15.5175 13.0407 15.9179C13.3083 16.3183 13.6885 16.6304 14.1334 16.8147C14.5783 16.9989 15.0679 17.0472 15.5402 16.9532C16.0125 16.8593 16.4464 16.6274 16.7869 16.2869C17.1274 15.9464 17.3593 15.5126 17.4532 15.0403C17.5472 14.568 17.4989 14.0784 17.3147 13.6335C17.1304 13.1886 16.8183 12.8084 16.4179 12.5409C16.0175 12.2733 15.5468 12.1305 15.0652 12.1305Z", stroke: "currentColor", strokeWidth: 1.125, strokeLinecap: "round", strokeLinejoin: "round" }), /* @__PURE__ */ React.createElement("path", { d: "M12.6318 5.86775H6.36955V12.1285H12.6318V5.86775Z", stroke: "currentColor", strokeWidth: 1.125, strokeLinecap: "round", strokeLinejoin: "round" }), /* @__PURE__ */ React.createElement("path", { d: "M17.5 3.43473C17.5 2.789 17.2435 2.16972 16.7869 1.71312C16.3303 1.25652 15.711 1 15.0652 1C14.4195 1 13.8002 1.25652 13.3435 1.71312C12.8869 2.16972 12.6304 2.789 12.6304 3.43473V5.86946H15.0652C15.711 5.86946 16.3303 5.61295 16.7869 5.15635C17.2435 4.69975 17.5 4.08046 17.5 3.43473Z", stroke: "currentColor", strokeWidth: 1.125, strokeLinecap: "round", strokeLinejoin: "round" }));
const SvgMagnifyingGlass = ({
  title,
  titleId,
  ...props
}) => /* @__PURE__ */ React.createElement("svg", { height: "1em", viewBox: "0 0 13 13", fill: "none", xmlns: "http://www.w3.org/2000/svg", "aria-labelledby": titleId, ...props }, title ? /* @__PURE__ */ React.createElement("title", { id: titleId }, title) : null, /* @__PURE__ */ React.createElement("circle", { cx: 5, cy: 5, r: 4.35, stroke: "currentColor", strokeWidth: 1.3 }), /* @__PURE__ */ React.createElement("line", { x1: 8.45962, y1: 8.54038, x2: 11.7525, y2: 11.8333, stroke: "currentColor", strokeWidth: 1.3 }));
const SvgMerge = ({
  title,
  titleId,
  ...props
}) => /* @__PURE__ */ React.createElement("svg", { height: "1em", viewBox: "-2 -2 22 22", fill: "none", xmlns: "http://www.w3.org/2000/svg", "aria-labelledby": titleId, ...props }, title ? /* @__PURE__ */ React.createElement("title", { id: titleId }, title) : null, /* @__PURE__ */ React.createElement("path", { d: "M17.2492 6V2.9569C17.2492 1.73806 16.2611 0.75 15.0423 0.75L2.9569 0.75C1.73806 0.75 0.75 1.73806 0.75 2.9569L0.75 6", stroke: "currentColor", strokeWidth: 1.5 }), /* @__PURE__ */ React.createElement("path", { d: "M0.749873 12V15.0431C0.749873 16.2619 1.73794 17.25 2.95677 17.25H15.0421C16.261 17.25 17.249 16.2619 17.249 15.0431V12", stroke: "currentColor", strokeWidth: 1.5 }), /* @__PURE__ */ React.createElement("path", { d: "M6 4.5L9 7.5L12 4.5", stroke: "currentColor", strokeWidth: 1.5 }), /* @__PURE__ */ React.createElement("path", { d: "M12 13.5L9 10.5L6 13.5", stroke: "currentColor", strokeWidth: 1.5 }));
const SvgPen = ({
  title,
  titleId,
  ...props
}) => /* @__PURE__ */ React.createElement("svg", { height: "1em", viewBox: "0 0 14 14", fill: "none", xmlns: "http://www.w3.org/2000/svg", "aria-labelledby": titleId, ...props }, title ? /* @__PURE__ */ React.createElement("title", { id: titleId }, title) : null, /* @__PURE__ */ React.createElement("path", { d: "M0.75 13.25L0.0554307 12.967C-0.0593528 13.2488 0.00743073 13.5719 0.224488 13.7851C0.441545 13.9983 0.765869 14.0592 1.04549 13.9393L0.75 13.25ZM12.8214 1.83253L12.2911 2.36286L12.2911 2.36286L12.8214 1.83253ZM12.8214 3.90194L13.3517 4.43227L12.8214 3.90194ZM10.0981 1.17859L9.56773 0.648259L10.0981 1.17859ZM12.1675 1.17859L12.6978 0.648258L12.6978 0.648257L12.1675 1.17859ZM2.58049 8.75697L3.27506 9.03994L2.58049 8.75697ZM2.70066 8.57599L3.23099 9.10632L2.70066 8.57599ZM5.2479 11.4195L4.95355 10.7297L5.2479 11.4195ZM5.42036 11.303L4.89003 10.7727L5.42036 11.303ZM4.95355 10.7297C4.08882 11.0987 3.41842 11.362 2.73535 11.6308C2.05146 11.9 1.35588 12.1743 0.454511 12.5607L1.04549 13.9393C1.92476 13.5624 2.60256 13.2951 3.28469 13.0266C3.96762 12.7578 4.65585 12.4876 5.54225 12.1093L4.95355 10.7297ZM1.44457 13.533L3.27506 9.03994L1.88592 8.474L0.0554307 12.967L1.44457 13.533ZM3.23099 9.10632L10.6284 1.70892L9.56773 0.648259L2.17033 8.04566L3.23099 9.10632ZM11.6371 1.70892L12.2911 2.36286L13.3517 1.3022L12.6978 0.648258L11.6371 1.70892ZM12.2911 3.37161L4.89003 10.7727L5.95069 11.8333L13.3517 4.43227L12.2911 3.37161ZM12.2911 2.36286C12.5696 2.64142 12.5696 3.09305 12.2911 3.37161L13.3517 4.43227C14.2161 3.56792 14.2161 2.16654 13.3517 1.3022L12.2911 2.36286ZM10.6284 1.70892C10.9069 1.43036 11.3586 1.43036 11.6371 1.70892L12.6978 0.648257C11.8335 -0.216088 10.4321 -0.216084 9.56773 0.648259L10.6284 1.70892ZM3.27506 9.03994C3.26494 9.06479 3.24996 9.08735 3.23099 9.10632L2.17033 8.04566C2.04793 8.16806 1.95123 8.31369 1.88592 8.474L3.27506 9.03994ZM5.54225 12.1093C5.69431 12.0444 5.83339 11.9506 5.95069 11.8333L4.89003 10.7727C4.90863 10.7541 4.92988 10.7398 4.95355 10.7297L5.54225 12.1093Z", fill: "currentColor" }), /* @__PURE__ */ React.createElement("path", { d: "M11.5 4.5L9.5 2.5", stroke: "currentColor", strokeWidth: 1.4026, strokeLinecap: "round", strokeLinejoin: "round" }), /* @__PURE__ */ React.createElement("path", { d: "M5.5 10.5L3.5 8.5", stroke: "currentColor", strokeWidth: 1.4026, strokeLinecap: "round", strokeLinejoin: "round" }));
const SvgPlay = ({
  title,
  titleId,
  ...props
}) => /* @__PURE__ */ React.createElement("svg", { height: "1em", viewBox: "0 0 16 18", fill: "none", xmlns: "http://www.w3.org/2000/svg", "aria-labelledby": titleId, ...props }, title ? /* @__PURE__ */ React.createElement("title", { id: titleId }, title) : null, /* @__PURE__ */ React.createElement("path", { d: "M1.32226e-07 1.6609C7.22332e-08 0.907329 0.801887 0.424528 1.46789 0.777117L15.3306 8.11621C16.0401 8.49182 16.0401 9.50818 15.3306 9.88379L1.46789 17.2229C0.801886 17.5755 1.36076e-06 17.0927 1.30077e-06 16.3391L1.32226e-07 1.6609Z", fill: "currentColor" }));
const SvgPlus = ({
  title,
  titleId,
  ...props
}) => /* @__PURE__ */ React.createElement("svg", { height: "1em", viewBox: "0 0 10 16", fill: "currentColor", xmlns: "http://www.w3.org/2000/svg", "aria-labelledby": titleId, ...props }, title ? /* @__PURE__ */ React.createElement("title", { id: titleId }, title) : null, /* @__PURE__ */ React.createElement("path", { fillRule: "evenodd", clipRule: "evenodd", d: "M4.25 9.25V13.5H5.75V9.25L10 9.25V7.75L5.75 7.75V3.5H4.25V7.75L0 7.75V9.25L4.25 9.25Z" }));
const SvgPrettify = ({
  title,
  titleId,
  ...props
}) => /* @__PURE__ */ React.createElement("svg", { width: 25, height: 25, viewBox: "0 0 25 25", fill: "none", xmlns: "http://www.w3.org/2000/svg", "aria-labelledby": titleId, ...props }, title ? /* @__PURE__ */ React.createElement("title", { id: titleId }, title) : null, /* @__PURE__ */ React.createElement("path", { d: "M10.2852 24.0745L13.7139 18.0742", stroke: "currentColor", strokeWidth: 1.5625 }), /* @__PURE__ */ React.createElement("path", { d: "M14.5742 24.0749L17.1457 19.7891", stroke: "currentColor", strokeWidth: 1.5625 }), /* @__PURE__ */ React.createElement("path", { d: "M19.4868 24.0735L20.7229 21.7523C21.3259 20.6143 21.5457 19.3122 21.3496 18.0394C21.1535 16.7666 20.5519 15.591 19.6342 14.6874L23.7984 6.87853C24.0123 6.47728 24.0581 6.00748 23.9256 5.57249C23.7932 5.1375 23.4933 4.77294 23.0921 4.55901C22.6908 4.34509 22.221 4.29932 21.7861 4.43178C21.3511 4.56424 20.9865 4.86408 20.7726 5.26533L16.6084 13.0742C15.3474 12.8142 14.0362 12.9683 12.8699 13.5135C11.7035 14.0586 10.7443 14.9658 10.135 16.1L6 24.0735", stroke: "currentColor", strokeWidth: 1.5625 }), /* @__PURE__ */ React.createElement("path", { d: "M4 15L5 13L7 12L5 11L4 9L3 11L1 12L3 13L4 15Z", stroke: "currentColor", strokeWidth: 1.5625, strokeLinejoin: "round" }), /* @__PURE__ */ React.createElement("path", { d: "M11.5 8L12.6662 5.6662L15 4.5L12.6662 3.3338L11.5 1L10.3338 3.3338L8 4.5L10.3338 5.6662L11.5 8Z", stroke: "currentColor", strokeWidth: 1.5625, strokeLinejoin: "round" }));
const SvgReload = ({
  title,
  titleId,
  ...props
}) => /* @__PURE__ */ React.createElement("svg", { height: "1em", viewBox: "0 0 16 16", fill: "none", xmlns: "http://www.w3.org/2000/svg", "aria-labelledby": titleId, ...props }, title ? /* @__PURE__ */ React.createElement("title", { id: titleId }, title) : null, /* @__PURE__ */ React.createElement("path", { d: "M4.75 9.25H1.25V12.75", stroke: "currentColor", strokeWidth: 1, strokeLinecap: "square" }), /* @__PURE__ */ React.createElement("path", { d: "M11.25 6.75H14.75V3.25", stroke: "currentColor", strokeWidth: 1, strokeLinecap: "square" }), /* @__PURE__ */ React.createElement("path", { d: "M14.1036 6.65539C13.8 5.27698 13.0387 4.04193 11.9437 3.15131C10.8487 2.26069 9.48447 1.76694 8.0731 1.75043C6.66173 1.73392 5.28633 2.19563 4.17079 3.0604C3.05526 3.92516 2.26529 5.14206 1.92947 6.513", stroke: "currentColor", strokeWidth: 1 }), /* @__PURE__ */ React.createElement("path", { d: "M1.89635 9.34461C2.20001 10.723 2.96131 11.9581 4.05631 12.8487C5.15131 13.7393 6.51553 14.2331 7.9269 14.2496C9.33827 14.2661 10.7137 13.8044 11.8292 12.9396C12.9447 12.0748 13.7347 10.8579 14.0705 9.487", stroke: "currentColor", strokeWidth: 1 }));
const SvgRootType = ({
  title,
  titleId,
  ...props
}) => /* @__PURE__ */ React.createElement("svg", { height: "1em", viewBox: "0 0 13 13", fill: "none", xmlns: "http://www.w3.org/2000/svg", "aria-labelledby": titleId, ...props }, title ? /* @__PURE__ */ React.createElement("title", { id: titleId }, title) : null, /* @__PURE__ */ React.createElement("rect", { x: 0.6, y: 0.6, width: 11.8, height: 11.8, rx: 5.9, stroke: "currentColor", strokeWidth: 1.2 }), /* @__PURE__ */ React.createElement("path", { d: "M4.25 7.5C4.25 6 5.75 5 6.5 6.5C7.25 8 8.75 7 8.75 5.5", stroke: "currentColor", strokeWidth: 1.2 }));
const SvgSettings = ({
  title,
  titleId,
  ...props
}) => /* @__PURE__ */ React.createElement("svg", { height: "1em", viewBox: "0 0 21 20", fill: "none", xmlns: "http://www.w3.org/2000/svg", "aria-labelledby": titleId, ...props }, title ? /* @__PURE__ */ React.createElement("title", { id: titleId }, title) : null, /* @__PURE__ */ React.createElement("path", { fillRule: "evenodd", clipRule: "evenodd", d: "M9.29186 1.92702C9.06924 1.82745 8.87014 1.68202 8.70757 1.50024L7.86631 0.574931C7.62496 0.309957 7.30773 0.12592 6.95791 0.0479385C6.60809 -0.0300431 6.24274 0.00182978 5.91171 0.139208C5.58068 0.276585 5.3001 0.512774 5.10828 0.815537C4.91645 1.1183 4.82272 1.47288 4.83989 1.83089L4.90388 3.08019C4.91612 3.32348 4.87721 3.56662 4.78968 3.79394C4.70215 4.02126 4.56794 4.2277 4.39571 4.39994C4.22347 4.57219 4.01704 4.7064 3.78974 4.79394C3.56243 4.88147 3.3193 4.92038 3.07603 4.90814L1.8308 4.84414C1.47162 4.82563 1.11553 4.91881 0.811445 5.11086C0.507359 5.30292 0.270203 5.58443 0.132561 5.91671C-0.00508149 6.249 -0.0364554 6.61576 0.0427496 6.9666C0.121955 7.31744 0.307852 7.63514 0.5749 7.87606L1.50016 8.71204C1.68193 8.87461 1.82735 9.07373 1.92692 9.29636C2.02648 9.51898 2.07794 9.76012 2.07794 10.004C2.07794 10.2479 2.02648 10.489 1.92692 10.7116C1.82735 10.9343 1.68193 11.1334 1.50016 11.296L0.5749 12.1319C0.309856 12.3729 0.125575 12.6898 0.0471809 13.0393C-0.0312128 13.3888 9.64098e-05 13.754 0.13684 14.0851C0.273583 14.4162 0.509106 14.6971 0.811296 14.8894C1.11349 15.0817 1.46764 15.1762 1.82546 15.1599L3.0707 15.0959C3.31397 15.0836 3.5571 15.1225 3.7844 15.2101C4.01171 15.2976 4.21814 15.4318 4.39037 15.6041C4.56261 15.7763 4.69682 15.9827 4.78435 16.2101C4.87188 16.4374 4.91078 16.6805 4.89855 16.9238L4.83455 18.1691C4.81605 18.5283 4.90921 18.8844 5.10126 19.1885C5.2933 19.4926 5.5748 19.7298 5.90707 19.8674C6.23934 20.0051 6.60608 20.0365 6.9569 19.9572C7.30772 19.878 7.6254 19.6921 7.86631 19.4251L8.7129 18.4998C8.87547 18.318 9.07458 18.1725 9.29719 18.073C9.51981 17.9734 9.76093 17.9219 10.0048 17.9219C10.2487 17.9219 10.4898 17.9734 10.7124 18.073C10.935 18.1725 11.1341 18.318 11.2967 18.4998L12.1326 19.4251C12.3735 19.6921 12.6912 19.878 13.042 19.9572C13.3929 20.0365 13.7596 20.0051 14.0919 19.8674C14.4241 19.7298 14.7056 19.4926 14.8977 19.1885C15.0897 18.8844 15.1829 18.5283 15.1644 18.1691L15.1004 16.9238C15.0882 16.6805 15.1271 16.4374 15.2146 16.2101C15.3021 15.9827 15.4363 15.7763 15.6086 15.6041C15.7808 15.4318 15.9872 15.2976 16.2145 15.2101C16.4418 15.1225 16.685 15.0836 16.9282 15.0959L18.1735 15.1599C18.5326 15.1784 18.8887 15.0852 19.1928 14.8931C19.4969 14.7011 19.7341 14.4196 19.8717 14.0873C20.0093 13.755 20.0407 13.3882 19.9615 13.0374C19.8823 12.6866 19.6964 12.3689 19.4294 12.1279L18.5041 11.292C18.3223 11.1294 18.1769 10.9303 18.0774 10.7076C17.9778 10.485 17.9263 10.2439 17.9263 10C17.9263 9.75612 17.9778 9.51499 18.0774 9.29236C18.1769 9.06973 18.3223 8.87062 18.5041 8.70804L19.4294 7.87206C19.6964 7.63114 19.8823 7.31344 19.9615 6.9626C20.0407 6.61176 20.0093 6.245 19.8717 5.91271C19.7341 5.58043 19.4969 5.29892 19.1928 5.10686C18.8887 4.91481 18.5326 4.82163 18.1735 4.84014L16.9282 4.90414C16.685 4.91638 16.4418 4.87747 16.2145 4.78994C15.9872 4.7024 15.7808 4.56818 15.6086 4.39594C15.4363 4.2237 15.3021 4.01726 15.2146 3.78994C15.1271 3.56262 15.0882 3.31948 15.1004 3.07619L15.1644 1.83089C15.1829 1.4717 15.0897 1.11559 14.8977 0.811487C14.7056 0.507385 14.4241 0.270217 14.0919 0.132568C13.7596 -0.00508182 13.3929 -0.0364573 13.042 0.0427519C12.6912 0.121961 12.3735 0.307869 12.1326 0.574931L11.2914 1.50024C11.1288 1.68202 10.9297 1.82745 10.7071 1.92702C10.4845 2.02659 10.2433 2.07805 9.99947 2.07805C9.7556 2.07805 9.51448 2.02659 9.29186 1.92702ZM14.3745 10C14.3745 12.4162 12.4159 14.375 9.99977 14.375C7.58365 14.375 5.625 12.4162 5.625 10C5.625 7.58375 7.58365 5.625 9.99977 5.625C12.4159 5.625 14.3745 7.58375 14.3745 10Z", fill: "currentColor" }));
const SvgStarFilled = ({
  title,
  titleId,
  ...props
}) => /* @__PURE__ */ React.createElement("svg", { height: "1em", viewBox: "0 0 14 14", fill: "none", xmlns: "http://www.w3.org/2000/svg", "aria-labelledby": titleId, ...props }, title ? /* @__PURE__ */ React.createElement("title", { id: titleId }, title) : null, /* @__PURE__ */ React.createElement("path", { d: "M6.5782 1.07092C6.71096 0.643026 7.28904 0.643027 7.4218 1.07092L8.59318 4.84622C8.65255 5.03758 8.82284 5.16714 9.01498 5.16714L12.8056 5.16714C13.2353 5.16714 13.4139 5.74287 13.0663 6.00732L9.99962 8.34058C9.84418 8.45885 9.77913 8.66848 9.83851 8.85984L11.0099 12.6351C11.1426 13.063 10.675 13.4189 10.3274 13.1544L7.26069 10.8211C7.10524 10.7029 6.89476 10.7029 6.73931 10.8211L3.6726 13.1544C3.32502 13.4189 2.85735 13.063 2.99012 12.6351L4.16149 8.85984C4.22087 8.66848 4.15582 8.45885 4.00038 8.34058L0.933671 6.00732C0.586087 5.74287 0.764722 5.16714 1.19436 5.16714L4.98502 5.16714C5.17716 5.16714 5.34745 5.03758 5.40682 4.84622L6.5782 1.07092Z", fill: "currentColor", stroke: "currentColor" }));
const SvgStar = ({
  title,
  titleId,
  ...props
}) => /* @__PURE__ */ React.createElement("svg", { height: "1em", viewBox: "0 0 14 14", fill: "none", xmlns: "http://www.w3.org/2000/svg", "aria-labelledby": titleId, ...props }, title ? /* @__PURE__ */ React.createElement("title", { id: titleId }, title) : null, /* @__PURE__ */ React.createElement("path", { d: "M6.5782 1.07092C6.71096 0.643026 7.28904 0.643027 7.4218 1.07092L8.59318 4.84622C8.65255 5.03758 8.82284 5.16714 9.01498 5.16714L12.8056 5.16714C13.2353 5.16714 13.4139 5.74287 13.0663 6.00732L9.99962 8.34058C9.84418 8.45885 9.77913 8.66848 9.83851 8.85984L11.0099 12.6351C11.1426 13.063 10.675 13.4189 10.3274 13.1544L7.26069 10.8211C7.10524 10.7029 6.89476 10.7029 6.73931 10.8211L3.6726 13.1544C3.32502 13.4189 2.85735 13.063 2.99012 12.6351L4.16149 8.85984C4.22087 8.66848 4.15582 8.45885 4.00038 8.34058L0.933671 6.00732C0.586087 5.74287 0.764722 5.16714 1.19436 5.16714L4.98502 5.16714C5.17716 5.16714 5.34745 5.03758 5.40682 4.84622L6.5782 1.07092Z", stroke: "currentColor", strokeWidth: 1.5 }));
const SvgStop = ({
  title,
  titleId,
  ...props
}) => /* @__PURE__ */ React.createElement("svg", { height: "1em", viewBox: "0 0 16 16", fill: "none", xmlns: "http://www.w3.org/2000/svg", "aria-labelledby": titleId, ...props }, title ? /* @__PURE__ */ React.createElement("title", { id: titleId }, title) : null, /* @__PURE__ */ React.createElement("rect", { width: 16, height: 16, rx: 2, fill: "currentColor" }));
const SvgTrash = ({
  title,
  titleId,
  ...props
}) => /* @__PURE__ */ React.createElement("svg", { width: "1em", height: "5em", xmlns: "http://www.w3.org/2000/svg", fillRule: "evenodd", "aria-hidden": "true", viewBox: "0 0 23 23", style: {
  height: "1.5em"
}, clipRule: "evenodd", "aria-labelledby": titleId, ...props }, title ? /* @__PURE__ */ React.createElement("title", { id: titleId }, title) : null, /* @__PURE__ */ React.createElement("path", { d: "M19 24h-14c-1.104 0-2-.896-2-2v-17h-1v-2h6v-1.5c0-.827.673-1.5 1.5-1.5h5c.825 0 1.5.671 1.5 1.5v1.5h6v2h-1v17c0 1.104-.896 2-2 2zm0-19h-14v16.5c0 .276.224.5.5.5h13c.276 0 .5-.224.5-.5v-16.5zm-7 7.586l3.293-3.293 1.414 1.414-3.293 3.293 3.293 3.293-1.414 1.414-3.293-3.293-3.293 3.293-1.414-1.414 3.293-3.293-3.293-3.293 1.414-1.414 3.293 3.293zm2-10.586h-4v1h4v-1z", fill: "currentColor", strokeWidth: 0.25, stroke: "currentColor" }));
const SvgType = ({
  title,
  titleId,
  ...props
}) => /* @__PURE__ */ React.createElement("svg", { height: "1em", viewBox: "0 0 13 13", fill: "none", xmlns: "http://www.w3.org/2000/svg", "aria-labelledby": titleId, ...props }, title ? /* @__PURE__ */ React.createElement("title", { id: titleId }, title) : null, /* @__PURE__ */ React.createElement("rect", { x: 0.6, y: 0.6, width: 11.8, height: 11.8, rx: 5.9, stroke: "currentColor", strokeWidth: 1.2 }), /* @__PURE__ */ React.createElement("rect", { x: 5.5, y: 5.5, width: 2, height: 2, rx: 1, fill: "currentColor" }));
const ArgumentIcon = generateIcon(SvgArgument);
const ChevronDownIcon = generateIcon(SvgChevronDown);
const ChevronLeftIcon = generateIcon(SvgChevronLeft);
const ChevronUpIcon = generateIcon(SvgChevronUp);
const CloseIcon = generateIcon(SvgClose);
const CopyIcon = generateIcon(SvgCopy);
const DeprecatedArgumentIcon = generateIcon(SvgDeprecatedArgument);
const DeprecatedEnumValueIcon = generateIcon(SvgDeprecatedEnumValue);
const DeprecatedFieldIcon = generateIcon(SvgDeprecatedField);
const DirectiveIcon = generateIcon(SvgDirective);
const DocsFilledIcon = generateIcon(SvgDocsFilled);
const DocsIcon = generateIcon(SvgDocs);
const EnumValueIcon = generateIcon(SvgEnumValue);
const FieldIcon = generateIcon(SvgField);
const HistoryIcon = generateIcon(SvgHistory);
const ImplementsIcon = generateIcon(SvgImplements);
const KeyboardShortcutIcon = generateIcon(SvgKeyboardShortcut);
const MagnifyingGlassIcon = generateIcon(SvgMagnifyingGlass);
const MergeIcon = generateIcon(SvgMerge);
const PenIcon = generateIcon(SvgPen);
const PlayIcon = generateIcon(SvgPlay);
const PlusIcon = generateIcon(SvgPlus);
const PrettifyIcon = generateIcon(SvgPrettify);
const ReloadIcon = generateIcon(SvgReload);
const RootTypeIcon = generateIcon(SvgRootType);
const SettingsIcon = generateIcon(SvgSettings);
const StarFilledIcon = generateIcon(SvgStarFilled);
const StarIcon = generateIcon(SvgStar);
const StopIcon = generateIcon(SvgStop);
const TrashIcon = generateIcon(SvgTrash);
const TypeIcon = generateIcon(SvgType);
function generateIcon(RawComponent) {
  const title = RawComponent.name.replace("Svg", "").replaceAll(/([A-Z])/g, " $1").trimStart().toLowerCase() + " icon";
  function IconComponent(props) {
    return /* @__PURE__ */ jsx(RawComponent, { title, ...props });
  }
  IconComponent.displayName = RawComponent.name;
  return IconComponent;
}
const UnStyledButton = forwardRef((props, ref) => /* @__PURE__ */ jsx(
  "button",
  {
    ...props,
    ref,
    className: clsx("graphiql-un-styled", props.className)
  }
));
UnStyledButton.displayName = "UnStyledButton";
const Button$1 = forwardRef((props, ref) => /* @__PURE__ */ jsx(
  "button",
  {
    ...props,
    ref,
    className: clsx(
      "graphiql-button",
      {
        success: "graphiql-button-success",
        error: "graphiql-button-error"
      }[props.state],
      props.className
    )
  }
));
Button$1.displayName = "Button";
const ButtonGroup = forwardRef((props, ref) => /* @__PURE__ */ jsx(
  "div",
  {
    ...props,
    ref,
    className: clsx("graphiql-button-group", props.className)
  }
));
ButtonGroup.displayName = "ButtonGroup";
const createComponentGroup = (root, children) => Object.entries(children).reduce((r, [key, value]) => {
  r[key] = value;
  return r;
}, root);
const DialogClose = forwardRef((props, ref) => /* @__PURE__ */ jsx(D.Close, { asChild: true, children: /* @__PURE__ */ jsxs(
  UnStyledButton,
  {
    ...props,
    ref,
    type: "button",
    className: clsx("graphiql-dialog-close", props.className),
    children: [
      /* @__PURE__ */ jsx(Root, { children: "Close dialog" }),
      /* @__PURE__ */ jsx(CloseIcon, {})
    ]
  }
) }));
DialogClose.displayName = "Dialog.Close";
function DialogRoot({
  children,
  ...props
}) {
  return /* @__PURE__ */ jsx(D.Root, { ...props, children: /* @__PURE__ */ jsxs(D.Portal, { children: [
    /* @__PURE__ */ jsx(D.Overlay, { className: "graphiql-dialog-overlay" }),
    /* @__PURE__ */ jsx(D.Content, { className: "graphiql-dialog", children })
  ] }) });
}
const Dialog = createComponentGroup(DialogRoot, {
  Close: DialogClose,
  Title: D.Title,
  Trigger: D.Trigger,
  Description: D.Description
});
const Button = forwardRef(
  (props, ref) => /* @__PURE__ */ jsx(Trigger, { asChild: true, children: /* @__PURE__ */ jsx(
    "button",
    {
      ...props,
      ref,
      className: clsx("graphiql-un-styled", props.className)
    }
  ) })
);
Button.displayName = "DropdownMenuButton";
function Content({
  children,
  align = "start",
  sideOffset = 5,
  className,
  ...props
}) {
  return /* @__PURE__ */ jsx(Portal, { children: /* @__PURE__ */ jsx(
    Content$1,
    {
      align,
      sideOffset,
      className: clsx("graphiql-dropdown-content", className),
      ...props,
      children
    }
  ) });
}
const Item = ({ className, children, ...props }) => /* @__PURE__ */ jsx(Item$1, { className: clsx("graphiql-dropdown-item", className), ...props, children });
const DropdownMenu = createComponentGroup(Root$1, {
  Button,
  Item,
  Content
});
const markdown = new MarkdownIt({
  breaks: true,
  linkify: true
});
const MarkdownContent = forwardRef(({ children, onlyShowFirstChild, type, ...props }, ref) => /* @__PURE__ */ jsx(
  "div",
  {
    ...props,
    ref,
    className: clsx(
      `graphiql-markdown-${type}`,
      onlyShowFirstChild && "graphiql-markdown-preview",
      props.className
    ),
    dangerouslySetInnerHTML: { __html: markdown.render(children) }
  }
));
MarkdownContent.displayName = "MarkdownContent";
const Spinner = forwardRef(
  (props, ref) => /* @__PURE__ */ jsx(
    "div",
    {
      ...props,
      ref,
      className: clsx("graphiql-spinner", props.className)
    }
  )
);
Spinner.displayName = "Spinner";
function TooltipRoot({
  children,
  align = "start",
  side = "bottom",
  sideOffset = 5,
  label
}) {
  return /* @__PURE__ */ jsxs(T.Root, { children: [
    /* @__PURE__ */ jsx(T.Trigger, { asChild: true, children }),
    /* @__PURE__ */ jsx(T.Portal, { children: /* @__PURE__ */ jsx(
      T.Content,
      {
        className: "graphiql-tooltip",
        align,
        side,
        sideOffset,
        children: label
      }
    ) })
  ] });
}
const Tooltip = createComponentGroup(TooltipRoot, {
  Provider: T.Provider
});
const TabRoot = forwardRef(
  ({ isActive, value, children, className, ...props }, ref) => /* @__PURE__ */ jsx(
    Reorder.Item,
    {
      ...props,
      ref,
      value,
      "aria-selected": isActive ? "true" : void 0,
      role: "tab",
      className: clsx(
        "graphiql-tab",
        isActive && "graphiql-tab-active",
        className
      ),
      children
    }
  )
);
TabRoot.displayName = "Tab";
const TabButton = forwardRef((props, ref) => /* @__PURE__ */ jsx(
  UnStyledButton,
  {
    ...props,
    ref,
    type: "button",
    className: clsx("graphiql-tab-button", props.className),
    children: props.children
  }
));
TabButton.displayName = "Tab.Button";
const TabClose = forwardRef(
  (props, ref) => /* @__PURE__ */ jsx(Tooltip, { label: "Close Tab", children: /* @__PURE__ */ jsx(
    UnStyledButton,
    {
      "aria-label": "Close Tab",
      ...props,
      ref,
      type: "button",
      className: clsx("graphiql-tab-close", props.className),
      children: /* @__PURE__ */ jsx(CloseIcon, {})
    }
  ) })
);
TabClose.displayName = "Tab.Close";
const Tab = createComponentGroup(TabRoot, {
  Button: TabButton,
  Close: TabClose
});
const Tabs = forwardRef(
  ({ values, onReorder, children, className, ...props }, ref) => /* @__PURE__ */ jsx(
    Reorder.Group,
    {
      ...props,
      ref,
      values,
      onReorder,
      axis: "x",
      role: "tablist",
      className: clsx("graphiql-tabs", className),
      children
    }
  )
);
Tabs.displayName = "Tabs";
const HistoryContext = createNullableContext("HistoryContext");
function HistoryContextProvider({
  maxHistoryLength = DEFAULT_HISTORY_LENGTH,
  children
}) {
  const storage = useStorageContext();
  const [historyStore] = useState(
    () => (
      // Fall back to a noop storage when the StorageContext is empty
      new HistoryStore(storage || new StorageAPI(null), maxHistoryLength)
    )
  );
  const [items, setItems] = useState(() => historyStore.queries || []);
  const value = useMemo(
    () => ({
      addToHistory(operation) {
        historyStore.updateHistory(operation);
        setItems(historyStore.queries);
      },
      editLabel(operation, index) {
        historyStore.editLabel(operation, index);
        setItems(historyStore.queries);
      },
      items,
      toggleFavorite(operation) {
        historyStore.toggleFavorite(operation);
        setItems(historyStore.queries);
      },
      setActive: (item) => item,
      deleteFromHistory(item, clearFavorites) {
        historyStore.deleteHistory(item, clearFavorites);
        setItems(historyStore.queries);
      }
    }),
    [items, historyStore]
  );
  return /* @__PURE__ */ jsx(HistoryContext.Provider, { value, children });
}
const useHistoryContext = createContextHook(HistoryContext);
const DEFAULT_HISTORY_LENGTH = 20;
function History() {
  const { items: all, deleteFromHistory } = useHistoryContext({
    nonNull: true
  });
  let items = all.slice().map((item, i) => ({ ...item, index: i })).reverse();
  const favorites = items.filter((item) => item.favorite);
  if (favorites.length) {
    items = items.filter((item) => !item.favorite);
  }
  const [clearStatus, setClearStatus] = useState(
    null
  );
  useEffect(() => {
    if (clearStatus) {
      setTimeout(() => {
        setClearStatus(null);
      }, 2e3);
    }
  }, [clearStatus]);
  const handleClearStatus = useCallback(() => {
    try {
      for (const item of items) {
        deleteFromHistory(item, true);
      }
      setClearStatus("success");
    } catch {
      setClearStatus("error");
    }
  }, [deleteFromHistory, items]);
  return /* @__PURE__ */ jsxs("section", { "aria-label": "History", className: "graphiql-history", children: [
    /* @__PURE__ */ jsxs("div", { className: "graphiql-history-header", children: [
      "History",
      (clearStatus || items.length > 0) && /* @__PURE__ */ jsx(
        Button$1,
        {
          type: "button",
          state: clearStatus || void 0,
          disabled: !items.length,
          onClick: handleClearStatus,
          children: {
            success: "Cleared",
            error: "Failed to Clear"
          }[clearStatus] || "Clear"
        }
      )
    ] }),
    Boolean(favorites.length) && /* @__PURE__ */ jsx("ul", { className: "graphiql-history-items", children: favorites.map((item) => /* @__PURE__ */ jsx(HistoryItem, { item }, item.index)) }),
    Boolean(favorites.length) && Boolean(items.length) && /* @__PURE__ */ jsx("div", { className: "graphiql-history-item-spacer" }),
    Boolean(items.length) && /* @__PURE__ */ jsx("ul", { className: "graphiql-history-items", children: items.map((item) => /* @__PURE__ */ jsx(HistoryItem, { item }, item.index)) })
  ] });
}
function HistoryItem(props) {
  const { editLabel, toggleFavorite, deleteFromHistory, setActive } = useHistoryContext({
    nonNull: true,
    caller: HistoryItem
  });
  const { headerEditor, queryEditor, variableEditor } = useEditorContext({
    nonNull: true,
    caller: HistoryItem
  });
  const inputRef = useRef(null);
  const buttonRef = useRef(null);
  const [isEditable, setIsEditable] = useState(false);
  useEffect(() => {
    var _a;
    if (isEditable) {
      (_a = inputRef.current) == null ? void 0 : _a.focus();
    }
  }, [isEditable]);
  const displayName = props.item.label || props.item.operationName || formatQuery(props.item.query);
  const handleSave = useCallback(() => {
    var _a;
    setIsEditable(false);
    const { index, ...item } = props.item;
    editLabel({ ...item, label: (_a = inputRef.current) == null ? void 0 : _a.value }, index);
  }, [editLabel, props.item]);
  const handleClose = useCallback(() => {
    setIsEditable(false);
  }, []);
  const handleEditLabel = useCallback(
    (e) => {
      e.stopPropagation();
      setIsEditable(true);
    },
    []
  );
  const handleHistoryItemClick = useCallback(() => {
    const { query, variables, headers } = props.item;
    queryEditor == null ? void 0 : queryEditor.setValue(query ?? "");
    variableEditor == null ? void 0 : variableEditor.setValue(variables ?? "");
    headerEditor == null ? void 0 : headerEditor.setValue(headers ?? "");
    setActive(props.item);
  }, [headerEditor, props.item, queryEditor, setActive, variableEditor]);
  const handleDeleteItemFromHistory = useCallback(
    (e) => {
      e.stopPropagation();
      deleteFromHistory(props.item);
    },
    [props.item, deleteFromHistory]
  );
  const handleToggleFavorite = useCallback(
    (e) => {
      e.stopPropagation();
      toggleFavorite(props.item);
    },
    [props.item, toggleFavorite]
  );
  return /* @__PURE__ */ jsx("li", { className: clsx("graphiql-history-item", isEditable && "editable"), children: isEditable ? /* @__PURE__ */ jsxs(Fragment, { children: [
    /* @__PURE__ */ jsx(
      "input",
      {
        type: "text",
        defaultValue: props.item.label,
        ref: inputRef,
        onKeyDown: (e) => {
          if (e.key === "Esc") {
            setIsEditable(false);
          } else if (e.key === "Enter") {
            setIsEditable(false);
            editLabel({ ...props.item, label: e.currentTarget.value });
          }
        },
        placeholder: "Type a label"
      }
    ),
    /* @__PURE__ */ jsx(UnStyledButton, { type: "button", ref: buttonRef, onClick: handleSave, children: "Save" }),
    /* @__PURE__ */ jsx(UnStyledButton, { type: "button", ref: buttonRef, onClick: handleClose, children: /* @__PURE__ */ jsx(CloseIcon, {}) })
  ] }) : /* @__PURE__ */ jsxs(Fragment, { children: [
    /* @__PURE__ */ jsx(Tooltip, { label: "Set active", children: /* @__PURE__ */ jsx(
      UnStyledButton,
      {
        type: "button",
        className: "graphiql-history-item-label",
        onClick: handleHistoryItemClick,
        "aria-label": "Set active",
        children: displayName
      }
    ) }),
    /* @__PURE__ */ jsx(Tooltip, { label: "Edit label", children: /* @__PURE__ */ jsx(
      UnStyledButton,
      {
        type: "button",
        className: "graphiql-history-item-action",
        onClick: handleEditLabel,
        "aria-label": "Edit label",
        children: /* @__PURE__ */ jsx(PenIcon, { "aria-hidden": "true" })
      }
    ) }),
    /* @__PURE__ */ jsx(
      Tooltip,
      {
        label: props.item.favorite ? "Remove favorite" : "Add favorite",
        children: /* @__PURE__ */ jsx(
          UnStyledButton,
          {
            type: "button",
            className: "graphiql-history-item-action",
            onClick: handleToggleFavorite,
            "aria-label": props.item.favorite ? "Remove favorite" : "Add favorite",
            children: props.item.favorite ? /* @__PURE__ */ jsx(StarFilledIcon, { "aria-hidden": "true" }) : /* @__PURE__ */ jsx(StarIcon, { "aria-hidden": "true" })
          }
        )
      }
    ),
    /* @__PURE__ */ jsx(Tooltip, { label: "Delete from history", children: /* @__PURE__ */ jsx(
      UnStyledButton,
      {
        type: "button",
        className: "graphiql-history-item-action",
        onClick: handleDeleteItemFromHistory,
        "aria-label": "Delete from history",
        children: /* @__PURE__ */ jsx(TrashIcon, { "aria-hidden": "true" })
      }
    ) })
  ] }) });
}
function formatQuery(query) {
  return query == null ? void 0 : query.split("\n").map((line) => line.replace(/#(.*)/, "")).join(" ").replaceAll("{", " { ").replaceAll("}", " } ").replaceAll(/[\s]{2,}/g, " ");
}
const ExecutionContext = createNullableContext("ExecutionContext");
function ExecutionContextProvider({
  fetcher,
  getDefaultFieldNames,
  children,
  operationName
}) {
  if (!fetcher) {
    throw new TypeError(
      "The `ExecutionContextProvider` component requires a `fetcher` function to be passed as prop."
    );
  }
  const {
    externalFragments,
    headerEditor,
    queryEditor,
    responseEditor,
    variableEditor,
    updateActiveTabValues
  } = useEditorContext({ nonNull: true, caller: ExecutionContextProvider });
  const history = useHistoryContext();
  const autoCompleteLeafs = useAutoCompleteLeafs({
    getDefaultFieldNames,
    caller: ExecutionContextProvider
  });
  const [isFetching, setIsFetching] = useState(false);
  const [subscription, setSubscription] = useState(null);
  const queryIdRef = useRef(0);
  const stop = useCallback(() => {
    subscription == null ? void 0 : subscription.unsubscribe();
    setIsFetching(false);
    setSubscription(null);
  }, [subscription]);
  const run = useCallback(async () => {
    if (!queryEditor || !responseEditor) {
      return;
    }
    if (subscription) {
      stop();
      return;
    }
    const setResponse = (value2) => {
      responseEditor.setValue(value2);
      updateActiveTabValues({ response: value2 });
    };
    queryIdRef.current += 1;
    const queryId = queryIdRef.current;
    let query = autoCompleteLeafs() || queryEditor.getValue();
    const variablesString = variableEditor == null ? void 0 : variableEditor.getValue();
    let variables;
    try {
      variables = tryParseJsonObject({
        json: variablesString,
        errorMessageParse: "Variables are invalid JSON",
        errorMessageType: "Variables are not a JSON object."
      });
    } catch (error) {
      setResponse(error instanceof Error ? error.message : `${error}`);
      return;
    }
    const headersString = headerEditor == null ? void 0 : headerEditor.getValue();
    let headers;
    try {
      headers = tryParseJsonObject({
        json: headersString,
        errorMessageParse: "Headers are invalid JSON",
        errorMessageType: "Headers are not a JSON object."
      });
    } catch (error) {
      setResponse(error instanceof Error ? error.message : `${error}`);
      return;
    }
    if (externalFragments) {
      const fragmentDependencies = queryEditor.documentAST ? getFragmentDependenciesForAST(
        queryEditor.documentAST,
        externalFragments
      ) : [];
      if (fragmentDependencies.length > 0) {
        query += "\n" + fragmentDependencies.map((node) => print(node)).join("\n");
      }
    }
    setResponse("");
    setIsFetching(true);
    const opName = operationName ?? queryEditor.operationName ?? void 0;
    history == null ? void 0 : history.addToHistory({
      query,
      variables: variablesString,
      headers: headersString,
      operationName: opName
    });
    try {
      const fullResponse = {};
      const handleResponse = (result) => {
        if (queryId !== queryIdRef.current) {
          return;
        }
        let maybeMultipart = Array.isArray(result) ? result : false;
        if (!maybeMultipart && typeof result === "object" && result !== null && "hasNext" in result) {
          maybeMultipart = [result];
        }
        if (maybeMultipart) {
          for (const part of maybeMultipart) {
            mergeIncrementalResult(fullResponse, part);
          }
          setIsFetching(false);
          setResponse(formatResult(fullResponse));
        } else {
          const response = formatResult(result);
          setIsFetching(false);
          setResponse(response);
        }
      };
      const fetch2 = fetcher(
        {
          query,
          variables,
          operationName: opName
        },
        {
          headers: headers ?? void 0,
          documentAST: queryEditor.documentAST ?? void 0
        }
      );
      const value2 = await Promise.resolve(fetch2);
      if (isObservable(value2)) {
        setSubscription(
          value2.subscribe({
            next(result) {
              handleResponse(result);
            },
            error(error) {
              setIsFetching(false);
              if (error) {
                setResponse(formatError(error));
              }
              setSubscription(null);
            },
            complete() {
              setIsFetching(false);
              setSubscription(null);
            }
          })
        );
      } else if (isAsyncIterable(value2)) {
        setSubscription({
          unsubscribe: () => {
            var _a, _b;
            return (_b = (_a = value2[Symbol.asyncIterator]()).return) == null ? void 0 : _b.call(_a);
          }
        });
        for await (const result of value2) {
          handleResponse(result);
        }
        setIsFetching(false);
        setSubscription(null);
      } else {
        handleResponse(value2);
      }
    } catch (error) {
      setIsFetching(false);
      setResponse(formatError(error));
      setSubscription(null);
    }
  }, [
    autoCompleteLeafs,
    externalFragments,
    fetcher,
    headerEditor,
    history,
    operationName,
    queryEditor,
    responseEditor,
    stop,
    subscription,
    updateActiveTabValues,
    variableEditor
  ]);
  const isSubscribed = Boolean(subscription);
  const value = useMemo(
    () => ({
      isFetching,
      isSubscribed,
      operationName: operationName ?? null,
      run,
      stop
    }),
    [isFetching, isSubscribed, operationName, run, stop]
  );
  return /* @__PURE__ */ jsx(ExecutionContext.Provider, { value, children });
}
const useExecutionContext = createContextHook(ExecutionContext);
function tryParseJsonObject({
  json,
  errorMessageParse,
  errorMessageType
}) {
  let parsed;
  try {
    parsed = json && json.trim() !== "" ? JSON.parse(json) : void 0;
  } catch (error) {
    throw new Error(
      `${errorMessageParse}: ${error instanceof Error ? error.message : error}.`
    );
  }
  const isObject = typeof parsed === "object" && parsed !== null && !Array.isArray(parsed);
  if (parsed !== void 0 && !isObject) {
    throw new Error(errorMessageType);
  }
  return parsed;
}
const pathsMap = /* @__PURE__ */ new WeakMap();
function mergeIncrementalResult(executionResult, incrementalResult) {
  var _a, _b, _c;
  let path = [
    "data",
    ...incrementalResult.path ?? []
  ];
  for (const result of [executionResult, incrementalResult]) {
    if (result.pending) {
      let paths = pathsMap.get(executionResult);
      if (paths === void 0) {
        paths = /* @__PURE__ */ new Map();
        pathsMap.set(executionResult, paths);
      }
      for (const { id, path: pendingPath } of result.pending) {
        paths.set(id, ["data", ...pendingPath]);
      }
    }
  }
  const { items } = incrementalResult;
  if (items) {
    const { id } = incrementalResult;
    if (id) {
      path = (_a = pathsMap.get(executionResult)) == null ? void 0 : _a.get(id);
      if (path === void 0) {
        throw new Error("Invalid incremental delivery format.");
      }
      const list = getValue(executionResult, path.join("."));
      list.push(...items);
    } else {
      path = ["data", ...incrementalResult.path ?? []];
      for (const item of items) {
        setValue(executionResult, path.join("."), item);
        path[path.length - 1]++;
      }
    }
  }
  const { data } = incrementalResult;
  if (data) {
    const { id } = incrementalResult;
    if (id) {
      path = (_b = pathsMap.get(executionResult)) == null ? void 0 : _b.get(id);
      if (path === void 0) {
        throw new Error("Invalid incremental delivery format.");
      }
      const { subPath } = incrementalResult;
      if (subPath !== void 0) {
        path = [...path, ...subPath];
      }
    }
    setValue(executionResult, path.join("."), data, {
      merge: true
    });
  }
  if (incrementalResult.errors) {
    executionResult.errors || (executionResult.errors = []);
    executionResult.errors.push(
      ...incrementalResult.errors
    );
  }
  if (incrementalResult.extensions) {
    setValue(executionResult, "extensions", incrementalResult.extensions, {
      merge: true
    });
  }
  if (incrementalResult.incremental) {
    for (const incrementalSubResult of incrementalResult.incremental) {
      mergeIncrementalResult(executionResult, incrementalSubResult);
    }
  }
  if (incrementalResult.completed) {
    for (const { id, errors } of incrementalResult.completed) {
      (_c = pathsMap.get(executionResult)) == null ? void 0 : _c.delete(id);
      if (errors) {
        executionResult.errors || (executionResult.errors = []);
        executionResult.errors.push(...errors);
      }
    }
  }
}
const isMacOs = typeof navigator !== "undefined" && navigator.userAgent.includes("Mac");
const DEFAULT_EDITOR_THEME = "graphiql";
const DEFAULT_KEY_MAP = "sublime";
const commonKeys = {
  // Persistent search box in Query Editor
  [isMacOs ? "Cmd-F" : "Ctrl-F"]: "findPersistent",
  "Cmd-G": "findPersistent",
  "Ctrl-G": "findPersistent",
  // Editor improvements
  "Ctrl-Left": "goSubwordLeft",
  "Ctrl-Right": "goSubwordRight",
  "Alt-Left": "goGroupLeft",
  "Alt-Right": "goGroupRight"
};
async function importCodeMirror(addons, options) {
  const CodeMirror = await import("./codemirror.es.js").then((n) => n.c).then(
    (c) => (
      // Depending on bundler and settings the dynamic import either returns a
      // function (e.g. parcel) or an object containing a `default` property
      typeof c === "function" ? c : c.default
    )
  );
  await Promise.all(
    (options == null ? void 0 : options.useCommonAddons) === false ? addons : [
      import("./show-hint.es.js").then((n) => n.s),
      import("./matchbrackets.es.js").then((n) => n.m),
      import("./closebrackets.es.js").then((n) => n.c),
      import("./brace-fold.es.js").then((n) => n.b),
      import("./foldgutter.es.js").then((n) => n.f),
      import("./lint.es.js").then((n) => n.l),
      import("./searchcursor.es.js").then((n) => n.s),
      import("./jump-to-line.es.js").then((n) => n.j),
      import("./dialog.es.js").then((n) => n.d),
      // @ts-expect-error
      import("./sublime.es.js").then((n) => n.s),
      ...addons
    ]
  );
  return CodeMirror;
}
const printDefault = (ast) => {
  if (!ast) {
    return "";
  }
  return print(ast);
};
function DefaultValue({ field }) {
  if (!("defaultValue" in field) || field.defaultValue === void 0) {
    return null;
  }
  const ast = astFromValue(field.defaultValue, field.type);
  if (!ast) {
    return null;
  }
  return /* @__PURE__ */ jsxs(Fragment, { children: [
    " = ",
    /* @__PURE__ */ jsx("span", { className: "graphiql-doc-explorer-default-value", children: printDefault(ast) })
  ] });
}
const SchemaContext = createNullableContext("SchemaContext");
function SchemaContextProvider(props) {
  if (!props.fetcher) {
    throw new TypeError(
      "The `SchemaContextProvider` component requires a `fetcher` function to be passed as prop."
    );
  }
  const { initialHeaders, headerEditor } = useEditorContext({
    nonNull: true,
    caller: SchemaContextProvider
  });
  const [schema, setSchema] = useState();
  const [isFetching, setIsFetching] = useState(false);
  const [fetchError, setFetchError] = useState(null);
  const counterRef = useRef(0);
  useEffect(() => {
    setSchema(
      isSchema(props.schema) || props.schema === null || props.schema === void 0 ? props.schema : void 0
    );
    counterRef.current++;
  }, [props.schema]);
  const headersRef = useRef(initialHeaders);
  useEffect(() => {
    if (headerEditor) {
      headersRef.current = headerEditor.getValue();
    }
  });
  const {
    introspectionQuery,
    introspectionQueryName,
    introspectionQuerySansSubscriptions
  } = useIntrospectionQuery({
    inputValueDeprecation: props.inputValueDeprecation,
    introspectionQueryName: props.introspectionQueryName,
    schemaDescription: props.schemaDescription
  });
  const { fetcher, onSchemaChange, dangerouslyAssumeSchemaIsValid, children } = props;
  const introspect = useCallback(() => {
    if (isSchema(props.schema) || props.schema === null) {
      return;
    }
    const counter = ++counterRef.current;
    const maybeIntrospectionData = props.schema;
    async function fetchIntrospectionData() {
      if (maybeIntrospectionData) {
        return maybeIntrospectionData;
      }
      const parsedHeaders = parseHeaderString(headersRef.current);
      if (!parsedHeaders.isValidJSON) {
        setFetchError("Introspection failed as headers are invalid.");
        return;
      }
      const fetcherOpts = parsedHeaders.headers ? { headers: parsedHeaders.headers } : {};
      const fetch2 = fetcherReturnToPromise(
        fetcher(
          {
            query: introspectionQuery,
            operationName: introspectionQueryName
          },
          fetcherOpts
        )
      );
      if (!isPromise(fetch2)) {
        setFetchError("Fetcher did not return a Promise for introspection.");
        return;
      }
      setIsFetching(true);
      setFetchError(null);
      let result = await fetch2;
      if (typeof result !== "object" || result === null || !("data" in result)) {
        const fetch22 = fetcherReturnToPromise(
          fetcher(
            {
              query: introspectionQuerySansSubscriptions,
              operationName: introspectionQueryName
            },
            fetcherOpts
          )
        );
        if (!isPromise(fetch22)) {
          throw new Error(
            "Fetcher did not return a Promise for introspection."
          );
        }
        result = await fetch22;
      }
      setIsFetching(false);
      if ((result == null ? void 0 : result.data) && "__schema" in result.data) {
        return result.data;
      }
      const responseString = typeof result === "string" ? result : formatResult(result);
      setFetchError(responseString);
    }
    fetchIntrospectionData().then((introspectionData) => {
      if (counter !== counterRef.current || !introspectionData) {
        return;
      }
      try {
        const newSchema = buildClientSchema(introspectionData);
        setSchema(newSchema);
        onSchemaChange == null ? void 0 : onSchemaChange(newSchema);
      } catch (error) {
        setFetchError(formatError(error));
      }
    }).catch((error) => {
      if (counter !== counterRef.current) {
        return;
      }
      setFetchError(formatError(error));
      setIsFetching(false);
    });
  }, [
    fetcher,
    introspectionQueryName,
    introspectionQuery,
    introspectionQuerySansSubscriptions,
    onSchemaChange,
    props.schema
  ]);
  useEffect(() => {
    introspect();
  }, [introspect]);
  useEffect(() => {
    function triggerIntrospection(event) {
      if (event.ctrlKey && event.key === "R") {
        introspect();
      }
    }
    window.addEventListener("keydown", triggerIntrospection);
    return () => window.removeEventListener("keydown", triggerIntrospection);
  });
  const validationErrors = useMemo(() => {
    if (!schema || dangerouslyAssumeSchemaIsValid) {
      return [];
    }
    return validateSchema(schema);
  }, [schema, dangerouslyAssumeSchemaIsValid]);
  const value = useMemo(
    () => ({
      fetchError,
      introspect,
      isFetching,
      schema,
      validationErrors
    }),
    [fetchError, introspect, isFetching, schema, validationErrors]
  );
  return /* @__PURE__ */ jsx(SchemaContext.Provider, { value, children });
}
const useSchemaContext = createContextHook(SchemaContext);
function useIntrospectionQuery({
  inputValueDeprecation,
  introspectionQueryName,
  schemaDescription
}) {
  return useMemo(() => {
    const queryName = introspectionQueryName || "IntrospectionQuery";
    let query = getIntrospectionQuery({
      inputValueDeprecation,
      schemaDescription
    });
    if (introspectionQueryName) {
      query = query.replace("query IntrospectionQuery", `query ${queryName}`);
    }
    const querySansSubscriptions = query.replace(
      "subscriptionType { name }",
      ""
    );
    return {
      introspectionQueryName: queryName,
      introspectionQuery: query,
      introspectionQuerySansSubscriptions: querySansSubscriptions
    };
  }, [inputValueDeprecation, introspectionQueryName, schemaDescription]);
}
function parseHeaderString(headersString) {
  let headers = null;
  let isValidJSON = true;
  try {
    if (headersString) {
      headers = JSON.parse(headersString);
    }
  } catch {
    isValidJSON = false;
  }
  return { headers, isValidJSON };
}
const initialNavStackItem = { name: "Docs" };
const ExplorerContext = createNullableContext("ExplorerContext");
function ExplorerContextProvider(props) {
  const { schema, validationErrors } = useSchemaContext({
    nonNull: true,
    caller: ExplorerContextProvider
  });
  const [navStack, setNavStack] = useState([
    initialNavStackItem
  ]);
  const push = useCallback((item) => {
    setNavStack((currentState) => {
      const lastItem = currentState.at(-1);
      return lastItem.def === item.def ? (
        // Avoid pushing duplicate items
        currentState
      ) : [...currentState, item];
    });
  }, []);
  const pop = useCallback(() => {
    setNavStack(
      (currentState) => currentState.length > 1 ? currentState.slice(0, -1) : currentState
    );
  }, []);
  const reset = useCallback(() => {
    setNavStack(
      (currentState) => currentState.length === 1 ? currentState : [initialNavStackItem]
    );
  }, []);
  useEffect(() => {
    if (schema == null || validationErrors.length > 0) {
      reset();
    } else {
      setNavStack((oldNavStack) => {
        if (oldNavStack.length === 1) {
          return oldNavStack;
        }
        const newNavStack = [initialNavStackItem];
        let lastEntity = null;
        for (const item of oldNavStack) {
          if (item === initialNavStackItem) {
            continue;
          }
          if (item.def) {
            if (isNamedType(item.def)) {
              const newType = schema.getType(item.def.name);
              if (newType) {
                newNavStack.push({
                  name: item.name,
                  def: newType
                });
                lastEntity = newType;
              } else {
                break;
              }
            } else if (lastEntity === null) {
              break;
            } else if (isObjectType(lastEntity) || isInputObjectType(lastEntity)) {
              const field = lastEntity.getFields()[item.name];
              if (field) {
                newNavStack.push({
                  name: item.name,
                  def: field
                });
              } else {
                break;
              }
            } else if (isScalarType(lastEntity) || isEnumType(lastEntity) || isInterfaceType(lastEntity) || isUnionType(lastEntity)) {
              break;
            } else {
              const field = lastEntity;
              const arg = field.args.find((a) => a.name === item.name);
              if (arg) {
                newNavStack.push({
                  name: item.name,
                  def: field
                });
              } else {
                break;
              }
            }
          } else {
            lastEntity = null;
            newNavStack.push(item);
          }
        }
        return newNavStack;
      });
    }
  }, [reset, schema, validationErrors]);
  const value = useMemo(
    () => ({ explorerNavStack: navStack, push, pop, reset }),
    [navStack, push, pop, reset]
  );
  return /* @__PURE__ */ jsx(ExplorerContext.Provider, { value, children: props.children });
}
const useExplorerContext = createContextHook(ExplorerContext);
function renderType(type, renderNamedType) {
  if (isNonNullType(type)) {
    return /* @__PURE__ */ jsxs(Fragment, { children: [
      renderType(type.ofType, renderNamedType),
      "!"
    ] });
  }
  if (isListType(type)) {
    return /* @__PURE__ */ jsxs(Fragment, { children: [
      "[",
      renderType(type.ofType, renderNamedType),
      "]"
    ] });
  }
  return renderNamedType(type);
}
function TypeLink(props) {
  const { push } = useExplorerContext({ nonNull: true, caller: TypeLink });
  if (!props.type) {
    return null;
  }
  return renderType(props.type, (namedType) => /* @__PURE__ */ jsx(
    "a",
    {
      className: "graphiql-doc-explorer-type-name",
      onClick: (event) => {
        event.preventDefault();
        push({ name: namedType.name, def: namedType });
      },
      href: "#",
      children: namedType.name
    }
  ));
}
function Argument({ arg, showDefaultValue, inline }) {
  const definition = /* @__PURE__ */ jsxs("span", { children: [
    /* @__PURE__ */ jsx("span", { className: "graphiql-doc-explorer-argument-name", children: arg.name }),
    ": ",
    /* @__PURE__ */ jsx(TypeLink, { type: arg.type }),
    showDefaultValue !== false && /* @__PURE__ */ jsx(DefaultValue, { field: arg })
  ] });
  if (inline) {
    return definition;
  }
  return /* @__PURE__ */ jsxs("div", { className: "graphiql-doc-explorer-argument", children: [
    definition,
    arg.description ? /* @__PURE__ */ jsx(MarkdownContent, { type: "description", children: arg.description }) : null,
    arg.deprecationReason ? /* @__PURE__ */ jsxs("div", { className: "graphiql-doc-explorer-argument-deprecation", children: [
      /* @__PURE__ */ jsx("div", { className: "graphiql-doc-explorer-argument-deprecation-label", children: "Deprecated" }),
      /* @__PURE__ */ jsx(MarkdownContent, { type: "deprecation", children: arg.deprecationReason })
    ] }) : null
  ] });
}
function DeprecationReason(props) {
  return props.children ? /* @__PURE__ */ jsxs("div", { className: "graphiql-doc-explorer-deprecation", children: [
    /* @__PURE__ */ jsx("div", { className: "graphiql-doc-explorer-deprecation-label", children: "Deprecated" }),
    /* @__PURE__ */ jsx(
      MarkdownContent,
      {
        type: "deprecation",
        onlyShowFirstChild: props.preview ?? true,
        children: props.children
      }
    )
  ] }) : null;
}
function Directive({ directive }) {
  return /* @__PURE__ */ jsxs("span", { className: "graphiql-doc-explorer-directive", children: [
    "@",
    directive.name.value
  ] });
}
function ExplorerSection(props) {
  const Icon2 = TYPE_TO_ICON[props.title];
  return /* @__PURE__ */ jsxs("div", { children: [
    /* @__PURE__ */ jsxs("div", { className: "graphiql-doc-explorer-section-title", children: [
      /* @__PURE__ */ jsx(Icon2, {}),
      props.title
    ] }),
    /* @__PURE__ */ jsx("div", { className: "graphiql-doc-explorer-section-content", children: props.children })
  ] });
}
const TYPE_TO_ICON = {
  Arguments: ArgumentIcon,
  "Deprecated Arguments": DeprecatedArgumentIcon,
  "Deprecated Enum Values": DeprecatedEnumValueIcon,
  "Deprecated Fields": DeprecatedFieldIcon,
  Directives: DirectiveIcon,
  "Enum Values": EnumValueIcon,
  Fields: FieldIcon,
  Implements: ImplementsIcon,
  Implementations: TypeIcon,
  "Possible Types": TypeIcon,
  "Root Types": RootTypeIcon,
  Type: TypeIcon,
  "All Schema Types": TypeIcon
};
function FieldDocumentation(props) {
  return /* @__PURE__ */ jsxs(Fragment, { children: [
    props.field.description ? /* @__PURE__ */ jsx(MarkdownContent, { type: "description", children: props.field.description }) : null,
    /* @__PURE__ */ jsx(DeprecationReason, { preview: false, children: props.field.deprecationReason }),
    /* @__PURE__ */ jsx(ExplorerSection, { title: "Type", children: /* @__PURE__ */ jsx(TypeLink, { type: props.field.type }) }),
    /* @__PURE__ */ jsx(Arguments, { field: props.field }),
    /* @__PURE__ */ jsx(Directives, { field: props.field })
  ] });
}
function Arguments({ field }) {
  const [showDeprecated, setShowDeprecated] = useState(false);
  const handleShowDeprecated = useCallback(() => {
    setShowDeprecated(true);
  }, []);
  if (!("args" in field)) {
    return null;
  }
  const args = [];
  const deprecatedArgs = [];
  for (const argument of field.args) {
    if (argument.deprecationReason) {
      deprecatedArgs.push(argument);
    } else {
      args.push(argument);
    }
  }
  return /* @__PURE__ */ jsxs(Fragment, { children: [
    args.length > 0 ? /* @__PURE__ */ jsx(ExplorerSection, { title: "Arguments", children: args.map((arg) => /* @__PURE__ */ jsx(Argument, { arg }, arg.name)) }) : null,
    deprecatedArgs.length > 0 ? showDeprecated || args.length === 0 ? /* @__PURE__ */ jsx(ExplorerSection, { title: "Deprecated Arguments", children: deprecatedArgs.map((arg) => /* @__PURE__ */ jsx(Argument, { arg }, arg.name)) }) : /* @__PURE__ */ jsx(Button$1, { type: "button", onClick: handleShowDeprecated, children: "Show Deprecated Arguments" }) : null
  ] });
}
function Directives({ field }) {
  var _a;
  const directives = ((_a = field.astNode) == null ? void 0 : _a.directives) || [];
  if (!directives || directives.length === 0) {
    return null;
  }
  return /* @__PURE__ */ jsx(ExplorerSection, { title: "Directives", children: directives.map((directive) => /* @__PURE__ */ jsx("div", { children: /* @__PURE__ */ jsx(Directive, { directive }) }, directive.name.value)) });
}
function SchemaDocumentation(props) {
  var _a, _b, _c, _d;
  const queryType = props.schema.getQueryType();
  const mutationType = (_b = (_a = props.schema).getMutationType) == null ? void 0 : _b.call(_a);
  const subscriptionType = (_d = (_c = props.schema).getSubscriptionType) == null ? void 0 : _d.call(_c);
  const typeMap = props.schema.getTypeMap();
  const ignoreTypesInAllSchema = [
    queryType == null ? void 0 : queryType.name,
    mutationType == null ? void 0 : mutationType.name,
    subscriptionType == null ? void 0 : subscriptionType.name
  ];
  return /* @__PURE__ */ jsxs(Fragment, { children: [
    /* @__PURE__ */ jsx(MarkdownContent, { type: "description", children: props.schema.description || "A GraphQL schema provides a root type for each kind of operation." }),
    /* @__PURE__ */ jsxs(ExplorerSection, { title: "Root Types", children: [
      queryType ? /* @__PURE__ */ jsxs("div", { children: [
        /* @__PURE__ */ jsx("span", { className: "graphiql-doc-explorer-root-type", children: "query" }),
        ": ",
        /* @__PURE__ */ jsx(TypeLink, { type: queryType })
      ] }) : null,
      mutationType && /* @__PURE__ */ jsxs("div", { children: [
        /* @__PURE__ */ jsx("span", { className: "graphiql-doc-explorer-root-type", children: "mutation" }),
        ": ",
        /* @__PURE__ */ jsx(TypeLink, { type: mutationType })
      ] }),
      subscriptionType && /* @__PURE__ */ jsxs("div", { children: [
        /* @__PURE__ */ jsx("span", { className: "graphiql-doc-explorer-root-type", children: "subscription" }),
        ": ",
        /* @__PURE__ */ jsx(TypeLink, { type: subscriptionType })
      ] })
    ] }),
    /* @__PURE__ */ jsx(ExplorerSection, { title: "All Schema Types", children: typeMap && /* @__PURE__ */ jsx("div", { children: Object.values(typeMap).map((type) => {
      if (ignoreTypesInAllSchema.includes(type.name) || type.name.startsWith("__")) {
        return null;
      }
      return /* @__PURE__ */ jsx("div", { children: /* @__PURE__ */ jsx(TypeLink, { type }) }, type.name);
    }) }) })
  ] });
}
function debounce(duration, fn) {
  let timeout;
  return function(...args) {
    if (timeout) {
      window.clearTimeout(timeout);
    }
    timeout = window.setTimeout(() => {
      timeout = null;
      fn(...args);
    }, duration);
  };
}
function Search() {
  const { explorerNavStack, push } = useExplorerContext({
    nonNull: true,
    caller: Search
  });
  const inputRef = useRef(null);
  const getSearchResults = useSearchResults();
  const [searchValue, setSearchValue] = useState("");
  const [results, setResults] = useState(getSearchResults(searchValue));
  const debouncedGetSearchResults = useMemo(
    () => debounce(200, (search) => {
      setResults(getSearchResults(search));
    }),
    [getSearchResults]
  );
  useEffect(() => {
    debouncedGetSearchResults(searchValue);
  }, [debouncedGetSearchResults, searchValue]);
  useEffect(() => {
    function handleKeyDown(event) {
      var _a;
      if (event.metaKey && event.key === "k") {
        (_a = inputRef.current) == null ? void 0 : _a.focus();
      }
    }
    window.addEventListener("keydown", handleKeyDown);
    return () => window.removeEventListener("keydown", handleKeyDown);
  }, []);
  const navItem = explorerNavStack.at(-1);
  const onSelect = useCallback(
    (def) => {
      push(
        "field" in def ? { name: def.field.name, def: def.field } : { name: def.type.name, def: def.type }
      );
    },
    [push]
  );
  const isFocused = useRef(false);
  const handleFocus = useCallback((e) => {
    isFocused.current = e.type === "focus";
  }, []);
  const shouldSearchBoxAppear = explorerNavStack.length === 1 || isObjectType(navItem.def) || isInterfaceType(navItem.def) || isInputObjectType(navItem.def);
  if (!shouldSearchBoxAppear) {
    return null;
  }
  return /* @__PURE__ */ jsxs(
    Combobox,
    {
      as: "div",
      className: "graphiql-doc-explorer-search",
      onChange: onSelect,
      "data-state": isFocused ? void 0 : "idle",
      "aria-label": `Search ${navItem.name}...`,
      children: [
        /* @__PURE__ */ jsxs(
          "div",
          {
            className: "graphiql-doc-explorer-search-input",
            onClick: () => {
              var _a;
              (_a = inputRef.current) == null ? void 0 : _a.focus();
            },
            children: [
              /* @__PURE__ */ jsx(MagnifyingGlassIcon, {}),
              /* @__PURE__ */ jsx(
                Combobox.Input,
                {
                  autoComplete: "off",
                  onFocus: handleFocus,
                  onBlur: handleFocus,
                  onChange: (event) => setSearchValue(event.target.value),
                  placeholder: `${isMacOs ? "⌘" : "Ctrl"} K`,
                  ref: inputRef,
                  value: searchValue,
                  "data-cy": "doc-explorer-input"
                }
              )
            ]
          }
        ),
        isFocused.current && /* @__PURE__ */ jsxs(Combobox.Options, { "data-cy": "doc-explorer-list", children: [
          results.within.length + results.types.length + results.fields.length === 0 ? /* @__PURE__ */ jsx("li", { className: "graphiql-doc-explorer-search-empty", children: "No results found" }) : results.within.map((result, i) => /* @__PURE__ */ jsx(
            Combobox.Option,
            {
              value: result,
              "data-cy": "doc-explorer-option",
              children: /* @__PURE__ */ jsx(Field$1, { field: result.field, argument: result.argument })
            },
            `within-${i}`
          )),
          results.within.length > 0 && results.types.length + results.fields.length > 0 ? /* @__PURE__ */ jsx("div", { className: "graphiql-doc-explorer-search-divider", children: "Other results" }) : null,
          results.types.map((result, i) => /* @__PURE__ */ jsx(
            Combobox.Option,
            {
              value: result,
              "data-cy": "doc-explorer-option",
              children: /* @__PURE__ */ jsx(Type, { type: result.type })
            },
            `type-${i}`
          )),
          results.fields.map((result, i) => /* @__PURE__ */ jsxs(
            Combobox.Option,
            {
              value: result,
              "data-cy": "doc-explorer-option",
              children: [
                /* @__PURE__ */ jsx(Type, { type: result.type }),
                ".",
                /* @__PURE__ */ jsx(Field$1, { field: result.field, argument: result.argument })
              ]
            },
            `field-${i}`
          ))
        ] })
      ]
    }
  );
}
function useSearchResults(caller) {
  const { explorerNavStack } = useExplorerContext({
    nonNull: true,
    caller: caller || useSearchResults
  });
  const { schema } = useSchemaContext({
    nonNull: true,
    caller: caller || useSearchResults
  });
  const navItem = explorerNavStack.at(-1);
  return useCallback(
    (searchValue) => {
      const matches = {
        within: [],
        types: [],
        fields: []
      };
      if (!schema) {
        return matches;
      }
      const withinType = navItem.def;
      const typeMap = schema.getTypeMap();
      let typeNames = Object.keys(typeMap);
      if (withinType) {
        typeNames = typeNames.filter((n) => n !== withinType.name);
        typeNames.unshift(withinType.name);
      }
      for (const typeName of typeNames) {
        if (matches.within.length + matches.types.length + matches.fields.length >= 100) {
          break;
        }
        const type = typeMap[typeName];
        if (withinType !== type && isMatch(typeName, searchValue)) {
          matches.types.push({ type });
        }
        if (!isObjectType(type) && !isInterfaceType(type) && !isInputObjectType(type)) {
          continue;
        }
        const fields = type.getFields();
        for (const fieldName in fields) {
          const field = fields[fieldName];
          let matchingArgs;
          if (!isMatch(fieldName, searchValue)) {
            if ("args" in field) {
              matchingArgs = field.args.filter(
                (arg) => isMatch(arg.name, searchValue)
              );
              if (matchingArgs.length === 0) {
                continue;
              }
            } else {
              continue;
            }
          }
          matches[withinType === type ? "within" : "fields"].push(
            ...matchingArgs ? matchingArgs.map((argument) => ({ type, field, argument })) : [{ type, field }]
          );
        }
      }
      return matches;
    },
    [navItem.def, schema]
  );
}
function isMatch(sourceText, searchValue) {
  try {
    const escaped = searchValue.replaceAll(/[^_0-9A-Za-z]/g, (ch) => "\\" + ch);
    return sourceText.search(new RegExp(escaped, "i")) !== -1;
  } catch {
    return sourceText.toLowerCase().includes(searchValue.toLowerCase());
  }
}
function Type(props) {
  return /* @__PURE__ */ jsx("span", { className: "graphiql-doc-explorer-search-type", children: props.type.name });
}
function Field$1({ field, argument }) {
  return /* @__PURE__ */ jsxs(Fragment, { children: [
    /* @__PURE__ */ jsx("span", { className: "graphiql-doc-explorer-search-field", children: field.name }),
    argument ? /* @__PURE__ */ jsxs(Fragment, { children: [
      "(",
      /* @__PURE__ */ jsx("span", { className: "graphiql-doc-explorer-search-argument", children: argument.name }),
      ":",
      " ",
      renderType(argument.type, (namedType) => /* @__PURE__ */ jsx(Type, { type: namedType })),
      ")"
    ] }) : null
  ] });
}
function FieldLink(props) {
  const { push } = useExplorerContext({ nonNull: true });
  return /* @__PURE__ */ jsx(
    "a",
    {
      className: "graphiql-doc-explorer-field-name",
      onClick: (event) => {
        event.preventDefault();
        push({ name: props.field.name, def: props.field });
      },
      href: "#",
      children: props.field.name
    }
  );
}
function TypeDocumentation(props) {
  return isNamedType(props.type) ? /* @__PURE__ */ jsxs(Fragment, { children: [
    props.type.description ? /* @__PURE__ */ jsx(MarkdownContent, { type: "description", children: props.type.description }) : null,
    /* @__PURE__ */ jsx(ImplementsInterfaces, { type: props.type }),
    /* @__PURE__ */ jsx(Fields, { type: props.type }),
    /* @__PURE__ */ jsx(EnumValues, { type: props.type }),
    /* @__PURE__ */ jsx(PossibleTypes, { type: props.type })
  ] }) : null;
}
function ImplementsInterfaces({ type }) {
  if (!isObjectType(type)) {
    return null;
  }
  const interfaces = type.getInterfaces();
  return interfaces.length > 0 ? /* @__PURE__ */ jsx(ExplorerSection, { title: "Implements", children: type.getInterfaces().map((implementedInterface) => /* @__PURE__ */ jsx("div", { children: /* @__PURE__ */ jsx(TypeLink, { type: implementedInterface }) }, implementedInterface.name)) }) : null;
}
function Fields({ type }) {
  const [showDeprecated, setShowDeprecated] = useState(false);
  const handleShowDeprecated = useCallback(() => {
    setShowDeprecated(true);
  }, []);
  if (!isObjectType(type) && !isInterfaceType(type) && !isInputObjectType(type)) {
    return null;
  }
  const fieldMap = type.getFields();
  const fields = [];
  const deprecatedFields = [];
  for (const field of Object.keys(fieldMap).map((name) => fieldMap[name])) {
    if (field.deprecationReason) {
      deprecatedFields.push(field);
    } else {
      fields.push(field);
    }
  }
  return /* @__PURE__ */ jsxs(Fragment, { children: [
    fields.length > 0 ? /* @__PURE__ */ jsx(ExplorerSection, { title: "Fields", children: fields.map((field) => /* @__PURE__ */ jsx(Field, { field }, field.name)) }) : null,
    deprecatedFields.length > 0 ? showDeprecated || fields.length === 0 ? /* @__PURE__ */ jsx(ExplorerSection, { title: "Deprecated Fields", children: deprecatedFields.map((field) => /* @__PURE__ */ jsx(Field, { field }, field.name)) }) : /* @__PURE__ */ jsx(Button$1, { type: "button", onClick: handleShowDeprecated, children: "Show Deprecated Fields" }) : null
  ] });
}
function Field({ field }) {
  const args = "args" in field ? field.args.filter((arg) => !arg.deprecationReason) : [];
  return /* @__PURE__ */ jsxs("div", { className: "graphiql-doc-explorer-item", children: [
    /* @__PURE__ */ jsxs("div", { children: [
      /* @__PURE__ */ jsx(FieldLink, { field }),
      args.length > 0 ? /* @__PURE__ */ jsxs(Fragment, { children: [
        "(",
        /* @__PURE__ */ jsx("span", { children: args.map(
          (arg) => args.length === 1 ? /* @__PURE__ */ jsx(Argument, { arg, inline: true }, arg.name) : /* @__PURE__ */ jsx(
            "div",
            {
              className: "graphiql-doc-explorer-argument-multiple",
              children: /* @__PURE__ */ jsx(Argument, { arg, inline: true })
            },
            arg.name
          )
        ) }),
        ")"
      ] }) : null,
      ": ",
      /* @__PURE__ */ jsx(TypeLink, { type: field.type }),
      /* @__PURE__ */ jsx(DefaultValue, { field })
    ] }),
    field.description ? /* @__PURE__ */ jsx(MarkdownContent, { type: "description", onlyShowFirstChild: true, children: field.description }) : null,
    /* @__PURE__ */ jsx(DeprecationReason, { children: field.deprecationReason })
  ] });
}
function EnumValues({ type }) {
  const [showDeprecated, setShowDeprecated] = useState(false);
  const handleShowDeprecated = useCallback(() => {
    setShowDeprecated(true);
  }, []);
  if (!isEnumType(type)) {
    return null;
  }
  const values = [];
  const deprecatedValues = [];
  for (const value of type.getValues()) {
    if (value.deprecationReason) {
      deprecatedValues.push(value);
    } else {
      values.push(value);
    }
  }
  return /* @__PURE__ */ jsxs(Fragment, { children: [
    values.length > 0 ? /* @__PURE__ */ jsx(ExplorerSection, { title: "Enum Values", children: values.map((value) => /* @__PURE__ */ jsx(EnumValue, { value }, value.name)) }) : null,
    deprecatedValues.length > 0 ? showDeprecated || values.length === 0 ? /* @__PURE__ */ jsx(ExplorerSection, { title: "Deprecated Enum Values", children: deprecatedValues.map((value) => /* @__PURE__ */ jsx(EnumValue, { value }, value.name)) }) : /* @__PURE__ */ jsx(Button$1, { type: "button", onClick: handleShowDeprecated, children: "Show Deprecated Values" }) : null
  ] });
}
function EnumValue({ value }) {
  return /* @__PURE__ */ jsxs("div", { className: "graphiql-doc-explorer-item", children: [
    /* @__PURE__ */ jsx("div", { className: "graphiql-doc-explorer-enum-value", children: value.name }),
    value.description ? /* @__PURE__ */ jsx(MarkdownContent, { type: "description", children: value.description }) : null,
    value.deprecationReason ? /* @__PURE__ */ jsx(MarkdownContent, { type: "deprecation", children: value.deprecationReason }) : null
  ] });
}
function PossibleTypes({ type }) {
  const { schema } = useSchemaContext({ nonNull: true });
  if (!schema || !isAbstractType(type)) {
    return null;
  }
  return /* @__PURE__ */ jsx(
    ExplorerSection,
    {
      title: isInterfaceType(type) ? "Implementations" : "Possible Types",
      children: schema.getPossibleTypes(type).map((possibleType) => /* @__PURE__ */ jsx("div", { children: /* @__PURE__ */ jsx(TypeLink, { type: possibleType }) }, possibleType.name))
    }
  );
}
function DocExplorer() {
  const { fetchError, isFetching, schema, validationErrors } = useSchemaContext(
    { nonNull: true, caller: DocExplorer }
  );
  const { explorerNavStack, pop } = useExplorerContext({
    nonNull: true,
    caller: DocExplorer
  });
  const navItem = explorerNavStack.at(-1);
  let content = null;
  if (fetchError) {
    content = /* @__PURE__ */ jsx("div", { className: "graphiql-doc-explorer-error", children: "Error fetching schema" });
  } else if (validationErrors.length > 0) {
    content = /* @__PURE__ */ jsxs("div", { className: "graphiql-doc-explorer-error", children: [
      "Schema is invalid: ",
      validationErrors[0].message
    ] });
  } else if (isFetching) {
    content = /* @__PURE__ */ jsx(Spinner, {});
  } else if (!schema) {
    content = /* @__PURE__ */ jsx("div", { className: "graphiql-doc-explorer-error", children: "No GraphQL schema available" });
  } else if (explorerNavStack.length === 1) {
    content = /* @__PURE__ */ jsx(SchemaDocumentation, { schema });
  } else if (isType(navItem.def)) {
    content = /* @__PURE__ */ jsx(TypeDocumentation, { type: navItem.def });
  } else if (navItem.def) {
    content = /* @__PURE__ */ jsx(FieldDocumentation, { field: navItem.def });
  }
  let prevName;
  if (explorerNavStack.length > 1) {
    prevName = explorerNavStack.at(-2).name;
  }
  return /* @__PURE__ */ jsxs(
    "section",
    {
      className: "graphiql-doc-explorer",
      "aria-label": "Documentation Explorer",
      children: [
        /* @__PURE__ */ jsxs("div", { className: "graphiql-doc-explorer-header", children: [
          /* @__PURE__ */ jsxs("div", { className: "graphiql-doc-explorer-header-content", children: [
            prevName && /* @__PURE__ */ jsxs(
              "a",
              {
                href: "#",
                className: "graphiql-doc-explorer-back",
                onClick: (event) => {
                  event.preventDefault();
                  pop();
                },
                "aria-label": `Go back to ${prevName}`,
                children: [
                  /* @__PURE__ */ jsx(ChevronLeftIcon, {}),
                  prevName
                ]
              }
            ),
            /* @__PURE__ */ jsx("div", { className: "graphiql-doc-explorer-title", children: navItem.name })
          ] }),
          /* @__PURE__ */ jsx(Search, {}, navItem.name)
        ] }),
        /* @__PURE__ */ jsx("div", { className: "graphiql-doc-explorer-content", children: content })
      ]
    }
  );
}
const DOC_EXPLORER_PLUGIN = {
  title: "Documentation Explorer",
  icon: function Icon() {
    const pluginContext = usePluginContext();
    return (pluginContext == null ? void 0 : pluginContext.visiblePlugin) === DOC_EXPLORER_PLUGIN ? /* @__PURE__ */ jsx(DocsFilledIcon, {}) : /* @__PURE__ */ jsx(DocsIcon, {});
  },
  content: DocExplorer
};
const HISTORY_PLUGIN = {
  title: "History",
  icon: HistoryIcon,
  content: History
};
const PluginContext = createNullableContext("PluginContext");
function PluginContextProvider(props) {
  const storage = useStorageContext();
  const explorerContext = useExplorerContext();
  const historyContext = useHistoryContext();
  const hasExplorerContext = Boolean(explorerContext);
  const hasHistoryContext = Boolean(historyContext);
  const plugins = useMemo(() => {
    const pluginList = [];
    const pluginTitles = {};
    if (hasExplorerContext) {
      pluginList.push(DOC_EXPLORER_PLUGIN);
      pluginTitles[DOC_EXPLORER_PLUGIN.title] = true;
    }
    if (hasHistoryContext) {
      pluginList.push(HISTORY_PLUGIN);
      pluginTitles[HISTORY_PLUGIN.title] = true;
    }
    for (const plugin of props.plugins || []) {
      if (typeof plugin.title !== "string" || !plugin.title) {
        throw new Error("All GraphiQL plugins must have a unique title");
      }
      if (pluginTitles[plugin.title]) {
        throw new Error(
          `All GraphiQL plugins must have a unique title, found two plugins with the title '${plugin.title}'`
        );
      } else {
        pluginList.push(plugin);
        pluginTitles[plugin.title] = true;
      }
    }
    return pluginList;
  }, [hasExplorerContext, hasHistoryContext, props.plugins]);
  const [visiblePlugin, internalSetVisiblePlugin] = useState(() => {
    const storedValue = storage == null ? void 0 : storage.get(STORAGE_KEY$4);
    const pluginForStoredValue = plugins.find(
      (plugin) => plugin.title === storedValue
    );
    if (pluginForStoredValue) {
      return pluginForStoredValue;
    }
    if (storedValue) {
      storage == null ? void 0 : storage.set(STORAGE_KEY$4, "");
    }
    if (!props.visiblePlugin) {
      return null;
    }
    return plugins.find(
      (plugin) => (typeof props.visiblePlugin === "string" ? plugin.title : plugin) === props.visiblePlugin
    ) || null;
  });
  const { onTogglePluginVisibility, children } = props;
  const setVisiblePlugin = useCallback(
    (plugin) => {
      const newVisiblePlugin = plugin ? plugins.find(
        (p) => (typeof plugin === "string" ? p.title : p) === plugin
      ) || null : null;
      internalSetVisiblePlugin((current) => {
        if (newVisiblePlugin === current) {
          return current;
        }
        onTogglePluginVisibility == null ? void 0 : onTogglePluginVisibility(newVisiblePlugin);
        return newVisiblePlugin;
      });
    },
    [onTogglePluginVisibility, plugins]
  );
  useEffect(() => {
    if (props.visiblePlugin) {
      setVisiblePlugin(props.visiblePlugin);
    }
  }, [plugins, props.visiblePlugin, setVisiblePlugin]);
  const value = useMemo(
    () => ({ plugins, setVisiblePlugin, visiblePlugin }),
    [plugins, setVisiblePlugin, visiblePlugin]
  );
  return /* @__PURE__ */ jsx(PluginContext.Provider, { value, children });
}
const usePluginContext = createContextHook(PluginContext);
const STORAGE_KEY$4 = "visiblePlugin";
function onHasCompletion(_cm, data, schema, explorer, plugin, callback) {
  void importCodeMirror([], { useCommonAddons: false }).then((CodeMirror) => {
    let information;
    let fieldName;
    let typeNamePill;
    let typeNamePrefix;
    let typeName;
    let typeNameSuffix;
    let description;
    let deprecation;
    let deprecationReason;
    CodeMirror.on(
      data,
      "select",
      // @ts-expect-error
      (ctx, el) => {
        if (!information) {
          const hintsUl = el.parentNode;
          information = document.createElement("div");
          information.className = "CodeMirror-hint-information";
          hintsUl.append(information);
          const header = document.createElement("header");
          header.className = "CodeMirror-hint-information-header";
          information.append(header);
          fieldName = document.createElement("span");
          fieldName.className = "CodeMirror-hint-information-field-name";
          header.append(fieldName);
          typeNamePill = document.createElement("span");
          typeNamePill.className = "CodeMirror-hint-information-type-name-pill";
          header.append(typeNamePill);
          typeNamePrefix = document.createElement("span");
          typeNamePill.append(typeNamePrefix);
          typeName = document.createElement("a");
          typeName.className = "CodeMirror-hint-information-type-name";
          typeName.href = "javascript:void 0";
          typeName.addEventListener("click", onClickHintInformation);
          typeNamePill.append(typeName);
          typeNameSuffix = document.createElement("span");
          typeNamePill.append(typeNameSuffix);
          description = document.createElement("div");
          description.className = "CodeMirror-hint-information-description";
          information.append(description);
          deprecation = document.createElement("div");
          deprecation.className = "CodeMirror-hint-information-deprecation";
          information.append(deprecation);
          const deprecationLabel = document.createElement("span");
          deprecationLabel.className = "CodeMirror-hint-information-deprecation-label";
          deprecationLabel.textContent = "Deprecated";
          deprecation.append(deprecationLabel);
          deprecationReason = document.createElement("div");
          deprecationReason.className = "CodeMirror-hint-information-deprecation-reason";
          deprecation.append(deprecationReason);
          const defaultInformationPadding = parseInt(
            window.getComputedStyle(information).paddingBottom.replace(/px$/, ""),
            10
          ) || 0;
          const defaultInformationMaxHeight = parseInt(
            window.getComputedStyle(information).maxHeight.replace(/px$/, ""),
            10
          ) || 0;
          const handleScroll = () => {
            if (information) {
              information.style.paddingTop = hintsUl.scrollTop + defaultInformationPadding + "px";
              information.style.maxHeight = hintsUl.scrollTop + defaultInformationMaxHeight + "px";
            }
          };
          hintsUl.addEventListener("scroll", handleScroll);
          let onRemoveFn;
          hintsUl.addEventListener(
            "DOMNodeRemoved",
            onRemoveFn = (event) => {
              if (event.target !== hintsUl) {
                return;
              }
              hintsUl.removeEventListener("scroll", handleScroll);
              hintsUl.removeEventListener("DOMNodeRemoved", onRemoveFn);
              if (information) {
                information.removeEventListener(
                  "click",
                  onClickHintInformation
                );
              }
              information = null;
              fieldName = null;
              typeNamePill = null;
              typeNamePrefix = null;
              typeName = null;
              typeNameSuffix = null;
              description = null;
              deprecation = null;
              deprecationReason = null;
              onRemoveFn = null;
            }
          );
        }
        if (fieldName) {
          fieldName.textContent = ctx.text;
        }
        if (typeNamePill && typeNamePrefix && typeName && typeNameSuffix) {
          if (ctx.type) {
            typeNamePill.style.display = "inline";
            const renderType2 = (type) => {
              if (isNonNullType(type)) {
                typeNameSuffix.textContent = "!" + typeNameSuffix.textContent;
                renderType2(type.ofType);
              } else if (isListType(type)) {
                typeNamePrefix.textContent += "[";
                typeNameSuffix.textContent = "]" + typeNameSuffix.textContent;
                renderType2(type.ofType);
              } else {
                typeName.textContent = type.name;
              }
            };
            typeNamePrefix.textContent = "";
            typeNameSuffix.textContent = "";
            renderType2(ctx.type);
          } else {
            typeNamePrefix.textContent = "";
            typeName.textContent = "";
            typeNameSuffix.textContent = "";
            typeNamePill.style.display = "none";
          }
        }
        if (description) {
          if (ctx.description) {
            description.style.display = "block";
            description.innerHTML = markdown.render(ctx.description);
          } else {
            description.style.display = "none";
            description.innerHTML = "";
          }
        }
        if (deprecation && deprecationReason) {
          if (ctx.deprecationReason) {
            deprecation.style.display = "block";
            deprecationReason.innerHTML = markdown.render(
              ctx.deprecationReason
            );
          } else {
            deprecation.style.display = "none";
            deprecationReason.innerHTML = "";
          }
        }
      }
    );
  });
  function onClickHintInformation(event) {
    if (!schema || !explorer || !plugin || !(event.currentTarget instanceof HTMLElement)) {
      return;
    }
    const typeName = event.currentTarget.textContent || "";
    const type = schema.getType(typeName);
    if (type) {
      plugin.setVisiblePlugin(DOC_EXPLORER_PLUGIN);
      explorer.push({ name: type.name, def: type });
      callback == null ? void 0 : callback(type);
    }
  }
}
function useSynchronizeValue(editor, value) {
  useEffect(() => {
    if (editor && typeof value === "string" && value !== editor.getValue()) {
      editor.setValue(value);
    }
  }, [editor, value]);
}
function useSynchronizeOption(editor, option, value) {
  useEffect(() => {
    if (editor) {
      editor.setOption(option, value);
    }
  }, [editor, option, value]);
}
function useChangeHandler(editor, callback, storageKey, tabProperty, caller) {
  const { updateActiveTabValues } = useEditorContext({ nonNull: true, caller });
  const storage = useStorageContext();
  useEffect(() => {
    if (!editor) {
      return;
    }
    const store = debounce(500, (value) => {
      if (!storage || storageKey === null) {
        return;
      }
      storage.set(storageKey, value);
    });
    const updateTab = debounce(100, (value) => {
      updateActiveTabValues({ [tabProperty]: value });
    });
    const handleChange = (editorInstance, changeObj) => {
      if (!changeObj) {
        return;
      }
      const newValue = editorInstance.getValue();
      store(newValue);
      updateTab(newValue);
      callback == null ? void 0 : callback(newValue);
    };
    editor.on("change", handleChange);
    return () => editor.off("change", handleChange);
  }, [
    callback,
    editor,
    storage,
    storageKey,
    tabProperty,
    updateActiveTabValues
  ]);
}
function useCompletion(editor, callback, caller) {
  const { schema } = useSchemaContext({ nonNull: true, caller });
  const explorer = useExplorerContext();
  const plugin = usePluginContext();
  useEffect(() => {
    if (!editor) {
      return;
    }
    const handleCompletion = (instance, changeObj) => {
      onHasCompletion(instance, changeObj, schema, explorer, plugin, (type) => {
        callback == null ? void 0 : callback({ kind: "Type", type, schema: schema || void 0 });
      });
    };
    editor.on(
      // @ts-expect-error @TODO additional args for hasCompletion event
      "hasCompletion",
      handleCompletion
    );
    return () => editor.off(
      // @ts-expect-error @TODO additional args for hasCompletion event
      "hasCompletion",
      handleCompletion
    );
  }, [callback, editor, explorer, plugin, schema]);
}
function useKeyMap(editor, keys, callback) {
  useEffect(() => {
    if (!editor) {
      return;
    }
    for (const key of keys) {
      editor.removeKeyMap(key);
    }
    if (callback) {
      const keyMap = {};
      for (const key of keys) {
        keyMap[key] = () => callback();
      }
      editor.addKeyMap(keyMap);
    }
  }, [editor, keys, callback]);
}
function useCopyQuery({ caller, onCopyQuery } = {}) {
  const { queryEditor } = useEditorContext({
    nonNull: true,
    caller: caller || useCopyQuery
  });
  return useCallback(() => {
    if (!queryEditor) {
      return;
    }
    const query = queryEditor.getValue();
    copyToClipboard(query);
    onCopyQuery == null ? void 0 : onCopyQuery(query);
  }, [queryEditor, onCopyQuery]);
}
function useMergeQuery({ caller } = {}) {
  const { queryEditor } = useEditorContext({
    nonNull: true,
    caller: caller || useMergeQuery
  });
  const { schema } = useSchemaContext({ nonNull: true, caller: useMergeQuery });
  return useCallback(() => {
    const documentAST = queryEditor == null ? void 0 : queryEditor.documentAST;
    const query = queryEditor == null ? void 0 : queryEditor.getValue();
    if (!documentAST || !query) {
      return;
    }
    queryEditor.setValue(print(mergeAst(documentAST, schema)));
  }, [queryEditor, schema]);
}
function usePrettifyEditors({ caller } = {}) {
  const { queryEditor, headerEditor, variableEditor } = useEditorContext({
    nonNull: true,
    caller: caller || usePrettifyEditors
  });
  return useCallback(() => {
    if (variableEditor) {
      const variableEditorContent = variableEditor.getValue();
      try {
        const prettifiedVariableEditorContent = JSON.stringify(
          JSON.parse(variableEditorContent),
          null,
          2
        );
        if (prettifiedVariableEditorContent !== variableEditorContent) {
          variableEditor.setValue(prettifiedVariableEditorContent);
        }
      } catch {
      }
    }
    if (headerEditor) {
      const headerEditorContent = headerEditor.getValue();
      try {
        const prettifiedHeaderEditorContent = JSON.stringify(
          JSON.parse(headerEditorContent),
          null,
          2
        );
        if (prettifiedHeaderEditorContent !== headerEditorContent) {
          headerEditor.setValue(prettifiedHeaderEditorContent);
        }
      } catch {
      }
    }
    if (queryEditor) {
      const editorContent = queryEditor.getValue();
      const prettifiedEditorContent = print(parse(editorContent));
      if (prettifiedEditorContent !== editorContent) {
        queryEditor.setValue(prettifiedEditorContent);
      }
    }
  }, [queryEditor, variableEditor, headerEditor]);
}
function useAutoCompleteLeafs({
  getDefaultFieldNames,
  caller
} = {}) {
  const { schema } = useSchemaContext({
    nonNull: true,
    caller: caller || useAutoCompleteLeafs
  });
  const { queryEditor } = useEditorContext({
    nonNull: true,
    caller: caller || useAutoCompleteLeafs
  });
  return useCallback(() => {
    if (!queryEditor) {
      return;
    }
    const query = queryEditor.getValue();
    const { insertions, result } = fillLeafs(
      schema,
      query,
      getDefaultFieldNames
    );
    if (insertions && insertions.length > 0) {
      queryEditor.operation(() => {
        const cursor = queryEditor.getCursor();
        const cursorIndex = queryEditor.indexFromPos(cursor);
        queryEditor.setValue(result || "");
        let added = 0;
        const markers = insertions.map(
          ({ index, string }) => queryEditor.markText(
            queryEditor.posFromIndex(index + added),
            queryEditor.posFromIndex(index + (added += string.length)),
            {
              className: "auto-inserted-leaf",
              clearOnEnter: true,
              title: "Automatically added leaf fields"
            }
          )
        );
        setTimeout(() => {
          for (const marker of markers) {
            marker.clear();
          }
        }, 7e3);
        let newCursorIndex = cursorIndex;
        for (const { index, string } of insertions) {
          if (index < cursorIndex) {
            newCursorIndex += string.length;
          }
        }
        queryEditor.setCursor(queryEditor.posFromIndex(newCursorIndex));
      });
    }
    return result;
  }, [getDefaultFieldNames, queryEditor, schema]);
}
const useEditorState = (editor) => {
  const context = useEditorContext({
    nonNull: true
  });
  const editorInstance = context[`${editor}Editor`];
  let valueString = "";
  const editorValue = (editorInstance == null ? void 0 : editorInstance.getValue()) ?? false;
  if (editorValue && editorValue.length > 0) {
    valueString = editorValue;
  }
  const handleEditorValue = useCallback(
    (value) => editorInstance == null ? void 0 : editorInstance.setValue(value),
    [editorInstance]
  );
  return useMemo(
    () => [valueString, handleEditorValue],
    [valueString, handleEditorValue]
  );
};
const useOperationsEditorState = () => {
  return useEditorState("query");
};
const useVariablesEditorState = () => {
  return useEditorState("variable");
};
const useHeadersEditorState = () => {
  return useEditorState("header");
};
function useOptimisticState([
  upstreamState,
  upstreamSetState
]) {
  const lastStateRef = useRef({
    /** The last thing that we sent upstream; we're expecting this back */
    pending: null,
    /** The last thing we received from upstream */
    last: upstreamState
  });
  const [state, setOperationsText] = useState(upstreamState);
  useEffect(() => {
    if (lastStateRef.current.last === upstreamState) ;
    else {
      lastStateRef.current.last = upstreamState;
      if (lastStateRef.current.pending === null) {
        setOperationsText(upstreamState);
      } else if (lastStateRef.current.pending === upstreamState) {
        lastStateRef.current.pending = null;
        if (upstreamState !== state) {
          lastStateRef.current.pending = state;
          upstreamSetState(state);
        }
      } else {
        lastStateRef.current.pending = null;
        setOperationsText(upstreamState);
      }
    }
  }, [upstreamState, state, upstreamSetState]);
  const setState = useCallback(
    (newState) => {
      setOperationsText(newState);
      if (lastStateRef.current.pending === null && lastStateRef.current.last !== newState) {
        lastStateRef.current.pending = newState;
        upstreamSetState(newState);
      }
    },
    [upstreamSetState]
  );
  return useMemo(() => [state, setState], [state, setState]);
}
function useHeaderEditor({
  editorTheme = DEFAULT_EDITOR_THEME,
  keyMap = DEFAULT_KEY_MAP,
  onEdit,
  readOnly = false
} = {}, caller) {
  const {
    initialHeaders,
    headerEditor,
    setHeaderEditor,
    shouldPersistHeaders
  } = useEditorContext({
    nonNull: true,
    caller: caller || useHeaderEditor
  });
  const executionContext = useExecutionContext();
  const merge = useMergeQuery({ caller: caller || useHeaderEditor });
  const prettify = usePrettifyEditors({ caller: caller || useHeaderEditor });
  const ref = useRef(null);
  useEffect(() => {
    let isActive = true;
    void importCodeMirror([
      // @ts-expect-error
      import("./javascript.es.js").then((n) => n.j)
    ]).then((CodeMirror) => {
      if (!isActive) {
        return;
      }
      const container = ref.current;
      if (!container) {
        return;
      }
      const newEditor = CodeMirror(container, {
        value: initialHeaders,
        lineNumbers: true,
        tabSize: 2,
        mode: { name: "javascript", json: true },
        theme: editorTheme,
        autoCloseBrackets: true,
        matchBrackets: true,
        showCursorWhenSelecting: true,
        readOnly: readOnly ? "nocursor" : false,
        foldGutter: true,
        gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"],
        extraKeys: commonKeys
      });
      newEditor.addKeyMap({
        "Cmd-Space"() {
          newEditor.showHint({ completeSingle: false, container });
        },
        "Ctrl-Space"() {
          newEditor.showHint({ completeSingle: false, container });
        },
        "Alt-Space"() {
          newEditor.showHint({ completeSingle: false, container });
        },
        "Shift-Space"() {
          newEditor.showHint({ completeSingle: false, container });
        }
      });
      newEditor.on("keyup", (editorInstance, event) => {
        const { code, key, shiftKey } = event;
        const isLetter = code.startsWith("Key");
        const isNumber = !shiftKey && code.startsWith("Digit");
        if (isLetter || isNumber || key === "_" || key === '"') {
          editorInstance.execCommand("autocomplete");
        }
      });
      setHeaderEditor(newEditor);
    });
    return () => {
      isActive = false;
    };
  }, [editorTheme, initialHeaders, readOnly, setHeaderEditor]);
  useSynchronizeOption(headerEditor, "keyMap", keyMap);
  useChangeHandler(
    headerEditor,
    onEdit,
    shouldPersistHeaders ? STORAGE_KEY$3 : null,
    "headers",
    useHeaderEditor
  );
  useKeyMap(headerEditor, ["Cmd-Enter", "Ctrl-Enter"], executionContext == null ? void 0 : executionContext.run);
  useKeyMap(headerEditor, ["Shift-Ctrl-P"], prettify);
  useKeyMap(headerEditor, ["Shift-Ctrl-M"], merge);
  return ref;
}
const STORAGE_KEY$3 = "headers";
const invalidCharacters = Array.from({ length: 11 }, (_, i) => {
  return String.fromCharCode(8192 + i);
}).concat(["\u2028", "\u2029", " ", " "]);
const sanitizeRegex = new RegExp("[" + invalidCharacters.join("") + "]", "g");
function normalizeWhitespace(line) {
  return line.replace(sanitizeRegex, " ");
}
function useQueryEditor({
  editorTheme = DEFAULT_EDITOR_THEME,
  keyMap = DEFAULT_KEY_MAP,
  onClickReference,
  onCopyQuery,
  onEdit,
  readOnly = false
} = {}, caller) {
  const { schema } = useSchemaContext({
    nonNull: true,
    caller: caller || useQueryEditor
  });
  const {
    externalFragments,
    initialQuery,
    queryEditor,
    setOperationName,
    setQueryEditor,
    validationRules,
    variableEditor,
    updateActiveTabValues
  } = useEditorContext({
    nonNull: true,
    caller: caller || useQueryEditor
  });
  const executionContext = useExecutionContext();
  const storage = useStorageContext();
  const explorer = useExplorerContext();
  const plugin = usePluginContext();
  const copy = useCopyQuery({ caller: caller || useQueryEditor, onCopyQuery });
  const merge = useMergeQuery({ caller: caller || useQueryEditor });
  const prettify = usePrettifyEditors({ caller: caller || useQueryEditor });
  const ref = useRef(null);
  const codeMirrorRef = useRef();
  const onClickReferenceRef = useRef(() => {
  });
  useEffect(() => {
    onClickReferenceRef.current = (reference) => {
      if (!explorer || !plugin) {
        return;
      }
      plugin.setVisiblePlugin(DOC_EXPLORER_PLUGIN);
      switch (reference.kind) {
        case "Type": {
          explorer.push({ name: reference.type.name, def: reference.type });
          break;
        }
        case "Field": {
          explorer.push({ name: reference.field.name, def: reference.field });
          break;
        }
        case "Argument": {
          if (reference.field) {
            explorer.push({ name: reference.field.name, def: reference.field });
          }
          break;
        }
        case "EnumValue": {
          if (reference.type) {
            explorer.push({ name: reference.type.name, def: reference.type });
          }
          break;
        }
      }
      onClickReference == null ? void 0 : onClickReference(reference);
    };
  }, [explorer, onClickReference, plugin]);
  useEffect(() => {
    let isActive = true;
    void importCodeMirror([
      import("./comment.es.js").then((n) => n.c),
      import("./search.es.js").then((n) => n.s),
      import("./hint.es.js"),
      import("./lint.es2.js"),
      import("./info.es.js"),
      import("./jump.es.js"),
      import("./mode.es.js")
    ]).then((CodeMirror) => {
      if (!isActive) {
        return;
      }
      codeMirrorRef.current = CodeMirror;
      const container = ref.current;
      if (!container) {
        return;
      }
      const newEditor = CodeMirror(container, {
        value: initialQuery,
        lineNumbers: true,
        tabSize: 2,
        foldGutter: true,
        mode: "graphql",
        theme: editorTheme,
        autoCloseBrackets: true,
        matchBrackets: true,
        showCursorWhenSelecting: true,
        readOnly: readOnly ? "nocursor" : false,
        lint: {
          // @ts-expect-error
          schema: void 0,
          validationRules: null,
          // linting accepts string or FragmentDefinitionNode[]
          externalFragments: void 0
        },
        hintOptions: {
          // @ts-expect-error
          schema: void 0,
          closeOnUnfocus: false,
          completeSingle: false,
          container,
          externalFragments: void 0,
          autocompleteOptions: {
            // for the query editor, restrict to executable type definitions
            mode: GraphQLDocumentMode.EXECUTABLE
          }
        },
        info: {
          schema: void 0,
          renderDescription: (text) => markdown.render(text),
          onClick(reference) {
            onClickReferenceRef.current(reference);
          }
        },
        jump: {
          schema: void 0,
          onClick(reference) {
            onClickReferenceRef.current(reference);
          }
        },
        gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"],
        extraKeys: {
          ...commonKeys,
          "Cmd-S"() {
          },
          "Ctrl-S"() {
          }
        }
      });
      newEditor.addKeyMap({
        "Cmd-Space"() {
          newEditor.showHint({ completeSingle: true, container });
        },
        "Ctrl-Space"() {
          newEditor.showHint({ completeSingle: true, container });
        },
        "Alt-Space"() {
          newEditor.showHint({ completeSingle: true, container });
        },
        "Shift-Space"() {
          newEditor.showHint({ completeSingle: true, container });
        },
        "Shift-Alt-Space"() {
          newEditor.showHint({ completeSingle: true, container });
        }
      });
      newEditor.on("keyup", (editorInstance, event) => {
        if (AUTO_COMPLETE_AFTER_KEY.test(event.key)) {
          editorInstance.execCommand("autocomplete");
        }
      });
      let showingHints = false;
      newEditor.on("startCompletion", () => {
        showingHints = true;
      });
      newEditor.on("endCompletion", () => {
        showingHints = false;
      });
      newEditor.on("keydown", (editorInstance, event) => {
        if (event.key === "Escape" && showingHints) {
          event.stopPropagation();
        }
      });
      newEditor.on("beforeChange", (editorInstance, change) => {
        var _a;
        if (change.origin === "paste") {
          const text = change.text.map(normalizeWhitespace);
          (_a = change.update) == null ? void 0 : _a.call(change, change.from, change.to, text);
        }
      });
      newEditor.documentAST = null;
      newEditor.operationName = null;
      newEditor.operations = null;
      newEditor.variableToType = null;
      setQueryEditor(newEditor);
    });
    return () => {
      isActive = false;
    };
  }, [editorTheme, initialQuery, readOnly, setQueryEditor]);
  useSynchronizeOption(queryEditor, "keyMap", keyMap);
  useEffect(() => {
    if (!queryEditor) {
      return;
    }
    function getAndUpdateOperationFacts(editorInstance) {
      var _a;
      const operationFacts = getOperationFacts(
        schema,
        editorInstance.getValue()
      );
      const operationName = getSelectedOperationName(
        editorInstance.operations ?? void 0,
        editorInstance.operationName ?? void 0,
        operationFacts == null ? void 0 : operationFacts.operations
      );
      editorInstance.documentAST = (operationFacts == null ? void 0 : operationFacts.documentAST) ?? null;
      editorInstance.operationName = operationName ?? null;
      editorInstance.operations = (operationFacts == null ? void 0 : operationFacts.operations) ?? null;
      if (variableEditor) {
        variableEditor.state.lint.linterOptions.variableToType = operationFacts == null ? void 0 : operationFacts.variableToType;
        variableEditor.options.lint.variableToType = operationFacts == null ? void 0 : operationFacts.variableToType;
        variableEditor.options.hintOptions.variableToType = operationFacts == null ? void 0 : operationFacts.variableToType;
        (_a = codeMirrorRef.current) == null ? void 0 : _a.signal(variableEditor, "change", variableEditor);
      }
      return operationFacts ? { ...operationFacts, operationName } : null;
    }
    const handleChange = debounce(
      100,
      (editorInstance) => {
        const query = editorInstance.getValue();
        storage == null ? void 0 : storage.set(STORAGE_KEY_QUERY, query);
        const currentOperationName = editorInstance.operationName;
        const operationFacts = getAndUpdateOperationFacts(editorInstance);
        if ((operationFacts == null ? void 0 : operationFacts.operationName) !== void 0) {
          storage == null ? void 0 : storage.set(
            STORAGE_KEY_OPERATION_NAME,
            operationFacts.operationName
          );
        }
        onEdit == null ? void 0 : onEdit(query, operationFacts == null ? void 0 : operationFacts.documentAST);
        if ((operationFacts == null ? void 0 : operationFacts.operationName) && currentOperationName !== operationFacts.operationName) {
          setOperationName(operationFacts.operationName);
        }
        updateActiveTabValues({
          query,
          operationName: (operationFacts == null ? void 0 : operationFacts.operationName) ?? null
        });
      }
    );
    getAndUpdateOperationFacts(queryEditor);
    queryEditor.on("change", handleChange);
    return () => queryEditor.off("change", handleChange);
  }, [
    onEdit,
    queryEditor,
    schema,
    setOperationName,
    storage,
    variableEditor,
    updateActiveTabValues
  ]);
  useSynchronizeSchema(queryEditor, schema ?? null, codeMirrorRef);
  useSynchronizeValidationRules(
    queryEditor,
    validationRules ?? null,
    codeMirrorRef
  );
  useSynchronizeExternalFragments(
    queryEditor,
    externalFragments,
    codeMirrorRef
  );
  useCompletion(queryEditor, onClickReference || null, useQueryEditor);
  const run = executionContext == null ? void 0 : executionContext.run;
  const runAtCursor = useCallback(() => {
    var _a;
    if (!run || !queryEditor || !queryEditor.operations || !queryEditor.hasFocus()) {
      run == null ? void 0 : run();
      return;
    }
    const cursorIndex = queryEditor.indexFromPos(queryEditor.getCursor());
    let operationName;
    for (const operation of queryEditor.operations) {
      if (operation.loc && operation.loc.start <= cursorIndex && operation.loc.end >= cursorIndex) {
        operationName = (_a = operation.name) == null ? void 0 : _a.value;
      }
    }
    if (operationName && operationName !== queryEditor.operationName) {
      setOperationName(operationName);
    }
    run();
  }, [queryEditor, run, setOperationName]);
  useKeyMap(queryEditor, ["Cmd-Enter", "Ctrl-Enter"], runAtCursor);
  useKeyMap(queryEditor, ["Shift-Ctrl-C"], copy);
  useKeyMap(
    queryEditor,
    [
      "Shift-Ctrl-P",
      // Shift-Ctrl-P is hard coded in Firefox for private browsing so adding an alternative to prettify
      "Shift-Ctrl-F"
    ],
    prettify
  );
  useKeyMap(queryEditor, ["Shift-Ctrl-M"], merge);
  return ref;
}
function useSynchronizeSchema(editor, schema, codeMirrorRef) {
  useEffect(() => {
    if (!editor) {
      return;
    }
    const didChange = editor.options.lint.schema !== schema;
    editor.state.lint.linterOptions.schema = schema;
    editor.options.lint.schema = schema;
    editor.options.hintOptions.schema = schema;
    editor.options.info.schema = schema;
    editor.options.jump.schema = schema;
    if (didChange && codeMirrorRef.current) {
      codeMirrorRef.current.signal(editor, "change", editor);
    }
  }, [editor, schema, codeMirrorRef]);
}
function useSynchronizeValidationRules(editor, validationRules, codeMirrorRef) {
  useEffect(() => {
    if (!editor) {
      return;
    }
    const didChange = editor.options.lint.validationRules !== validationRules;
    editor.state.lint.linterOptions.validationRules = validationRules;
    editor.options.lint.validationRules = validationRules;
    if (didChange && codeMirrorRef.current) {
      codeMirrorRef.current.signal(editor, "change", editor);
    }
  }, [editor, validationRules, codeMirrorRef]);
}
function useSynchronizeExternalFragments(editor, externalFragments, codeMirrorRef) {
  const externalFragmentList = useMemo(
    () => [...externalFragments.values()],
    [externalFragments]
  );
  useEffect(() => {
    if (!editor) {
      return;
    }
    const didChange = editor.options.lint.externalFragments !== externalFragmentList;
    editor.state.lint.linterOptions.externalFragments = externalFragmentList;
    editor.options.lint.externalFragments = externalFragmentList;
    editor.options.hintOptions.externalFragments = externalFragmentList;
    if (didChange && codeMirrorRef.current) {
      codeMirrorRef.current.signal(editor, "change", editor);
    }
  }, [editor, externalFragmentList, codeMirrorRef]);
}
const AUTO_COMPLETE_AFTER_KEY = /^[a-zA-Z0-9_@(]$/;
const STORAGE_KEY_QUERY = "query";
const STORAGE_KEY_OPERATION_NAME = "operationName";
function getDefaultTabState({
  defaultQuery,
  defaultHeaders,
  headers,
  defaultTabs,
  query,
  variables,
  storage,
  shouldPersistHeaders
}) {
  const storedState = storage == null ? void 0 : storage.get(STORAGE_KEY$2);
  try {
    if (!storedState) {
      throw new Error("Storage for tabs is empty");
    }
    const parsed = JSON.parse(storedState);
    const headersForHash = shouldPersistHeaders ? headers : void 0;
    if (isTabsState(parsed)) {
      const expectedHash = hashFromTabContents({
        query,
        variables,
        headers: headersForHash
      });
      let matchingTabIndex = -1;
      for (let index = 0; index < parsed.tabs.length; index++) {
        const tab = parsed.tabs[index];
        tab.hash = hashFromTabContents({
          query: tab.query,
          variables: tab.variables,
          headers: tab.headers
        });
        if (tab.hash === expectedHash) {
          matchingTabIndex = index;
        }
      }
      if (matchingTabIndex >= 0) {
        parsed.activeTabIndex = matchingTabIndex;
      } else {
        const operationName = query ? fuzzyExtractOperationName(query) : null;
        parsed.tabs.push({
          id: guid(),
          hash: expectedHash,
          title: operationName || DEFAULT_TITLE,
          query,
          variables,
          headers,
          operationName,
          response: null
        });
        parsed.activeTabIndex = parsed.tabs.length - 1;
      }
      return parsed;
    }
    throw new Error("Storage for tabs is invalid");
  } catch {
    return {
      activeTabIndex: 0,
      tabs: (defaultTabs || [
        {
          query: query ?? defaultQuery,
          variables,
          headers: headers ?? defaultHeaders
        }
      ]).map(createTab)
    };
  }
}
function isTabsState(obj) {
  return obj && typeof obj === "object" && !Array.isArray(obj) && hasNumberKey(obj, "activeTabIndex") && "tabs" in obj && Array.isArray(obj.tabs) && obj.tabs.every(isTabState);
}
function isTabState(obj) {
  return obj && typeof obj === "object" && !Array.isArray(obj) && hasStringKey(obj, "id") && hasStringKey(obj, "title") && hasStringOrNullKey(obj, "query") && hasStringOrNullKey(obj, "variables") && hasStringOrNullKey(obj, "headers") && hasStringOrNullKey(obj, "operationName") && hasStringOrNullKey(obj, "response");
}
function hasNumberKey(obj, key) {
  return key in obj && typeof obj[key] === "number";
}
function hasStringKey(obj, key) {
  return key in obj && typeof obj[key] === "string";
}
function hasStringOrNullKey(obj, key) {
  return key in obj && (typeof obj[key] === "string" || obj[key] === null);
}
function useSynchronizeActiveTabValues({
  queryEditor,
  variableEditor,
  headerEditor,
  responseEditor
}) {
  return useCallback(
    (state) => {
      const query = (queryEditor == null ? void 0 : queryEditor.getValue()) ?? null;
      const variables = (variableEditor == null ? void 0 : variableEditor.getValue()) ?? null;
      const headers = (headerEditor == null ? void 0 : headerEditor.getValue()) ?? null;
      const operationName = (queryEditor == null ? void 0 : queryEditor.operationName) ?? null;
      const response = (responseEditor == null ? void 0 : responseEditor.getValue()) ?? null;
      return setPropertiesInActiveTab(state, {
        query,
        variables,
        headers,
        response,
        operationName
      });
    },
    [queryEditor, variableEditor, headerEditor, responseEditor]
  );
}
function serializeTabState(tabState, shouldPersistHeaders = false) {
  return JSON.stringify(
    tabState,
    (key, value) => key === "hash" || key === "response" || !shouldPersistHeaders && key === "headers" ? null : value
  );
}
function useStoreTabs({
  storage,
  shouldPersistHeaders
}) {
  const store = useMemo(
    () => debounce(500, (value) => {
      storage == null ? void 0 : storage.set(STORAGE_KEY$2, value);
    }),
    [storage]
  );
  return useCallback(
    (currentState) => {
      store(serializeTabState(currentState, shouldPersistHeaders));
    },
    [shouldPersistHeaders, store]
  );
}
function useSetEditorValues({
  queryEditor,
  variableEditor,
  headerEditor,
  responseEditor,
  defaultHeaders
}) {
  return useCallback(
    ({
      query,
      variables,
      headers,
      response
    }) => {
      queryEditor == null ? void 0 : queryEditor.setValue(query ?? "");
      variableEditor == null ? void 0 : variableEditor.setValue(variables ?? "");
      headerEditor == null ? void 0 : headerEditor.setValue(headers ?? defaultHeaders ?? "");
      responseEditor == null ? void 0 : responseEditor.setValue(response ?? "");
    },
    [headerEditor, queryEditor, responseEditor, variableEditor, defaultHeaders]
  );
}
function createTab({
  query = null,
  variables = null,
  headers = null
} = {}) {
  return {
    id: guid(),
    hash: hashFromTabContents({ query, variables, headers }),
    title: query && fuzzyExtractOperationName(query) || DEFAULT_TITLE,
    query,
    variables,
    headers,
    operationName: null,
    response: null
  };
}
function setPropertiesInActiveTab(state, partialTab) {
  return {
    ...state,
    tabs: state.tabs.map((tab, index) => {
      if (index !== state.activeTabIndex) {
        return tab;
      }
      const newTab = { ...tab, ...partialTab };
      return {
        ...newTab,
        hash: hashFromTabContents(newTab),
        title: newTab.operationName || (newTab.query ? fuzzyExtractOperationName(newTab.query) : void 0) || DEFAULT_TITLE
      };
    })
  };
}
function guid() {
  const s4 = () => {
    return Math.floor((1 + Math.random()) * 65536).toString(16).slice(1);
  };
  return `${s4()}${s4()}-${s4()}-${s4()}-${s4()}-${s4()}${s4()}${s4()}`;
}
function hashFromTabContents(args) {
  return [args.query ?? "", args.variables ?? "", args.headers ?? ""].join("|");
}
function fuzzyExtractOperationName(str) {
  const regex = /^(?!#).*(query|subscription|mutation)\s+([a-zA-Z0-9_]+)/m;
  const match = regex.exec(str);
  return (match == null ? void 0 : match[2]) ?? null;
}
function clearHeadersFromTabs(storage) {
  const persistedTabs = storage == null ? void 0 : storage.get(STORAGE_KEY$2);
  if (persistedTabs) {
    const parsedTabs = JSON.parse(persistedTabs);
    storage == null ? void 0 : storage.set(
      STORAGE_KEY$2,
      JSON.stringify(
        parsedTabs,
        (key, value) => key === "headers" ? null : value
      )
    );
  }
}
const DEFAULT_TITLE = "<untitled>";
const STORAGE_KEY$2 = "tabState";
function useVariableEditor({
  editorTheme = DEFAULT_EDITOR_THEME,
  keyMap = DEFAULT_KEY_MAP,
  onClickReference,
  onEdit,
  readOnly = false
} = {}, caller) {
  const { initialVariables, variableEditor, setVariableEditor } = useEditorContext({
    nonNull: true,
    caller: caller || useVariableEditor
  });
  const executionContext = useExecutionContext();
  const merge = useMergeQuery({ caller: caller || useVariableEditor });
  const prettify = usePrettifyEditors({ caller: caller || useVariableEditor });
  const ref = useRef(null);
  const codeMirrorRef = useRef();
  useEffect(() => {
    let isActive = true;
    void importCodeMirror([
      import("./hint.es2.js"),
      import("./lint.es3.js"),
      import("./mode.es2.js")
    ]).then((CodeMirror) => {
      if (!isActive) {
        return;
      }
      codeMirrorRef.current = CodeMirror;
      const container = ref.current;
      if (!container) {
        return;
      }
      const newEditor = CodeMirror(container, {
        value: initialVariables,
        lineNumbers: true,
        tabSize: 2,
        mode: "graphql-variables",
        theme: editorTheme,
        autoCloseBrackets: true,
        matchBrackets: true,
        showCursorWhenSelecting: true,
        readOnly: readOnly ? "nocursor" : false,
        foldGutter: true,
        lint: {
          // @ts-expect-error
          variableToType: void 0
        },
        hintOptions: {
          closeOnUnfocus: false,
          completeSingle: false,
          container,
          // @ts-expect-error
          variableToType: void 0
        },
        gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"],
        extraKeys: commonKeys
      });
      newEditor.addKeyMap({
        "Cmd-Space"() {
          newEditor.showHint({ completeSingle: false, container });
        },
        "Ctrl-Space"() {
          newEditor.showHint({ completeSingle: false, container });
        },
        "Alt-Space"() {
          newEditor.showHint({ completeSingle: false, container });
        },
        "Shift-Space"() {
          newEditor.showHint({ completeSingle: false, container });
        }
      });
      newEditor.on("keyup", (editorInstance, event) => {
        const { code, key, shiftKey } = event;
        const isLetter = code.startsWith("Key");
        const isNumber = !shiftKey && code.startsWith("Digit");
        if (isLetter || isNumber || key === "_" || key === '"') {
          editorInstance.execCommand("autocomplete");
        }
      });
      setVariableEditor(newEditor);
    });
    return () => {
      isActive = false;
    };
  }, [editorTheme, initialVariables, readOnly, setVariableEditor]);
  useSynchronizeOption(variableEditor, "keyMap", keyMap);
  useChangeHandler(
    variableEditor,
    onEdit,
    STORAGE_KEY$1,
    "variables",
    useVariableEditor
  );
  useCompletion(variableEditor, onClickReference || null, useVariableEditor);
  useKeyMap(variableEditor, ["Cmd-Enter", "Ctrl-Enter"], executionContext == null ? void 0 : executionContext.run);
  useKeyMap(variableEditor, ["Shift-Ctrl-P"], prettify);
  useKeyMap(variableEditor, ["Shift-Ctrl-M"], merge);
  return ref;
}
const STORAGE_KEY$1 = "variables";
const EditorContext = createNullableContext("EditorContext");
function EditorContextProvider(props) {
  const storage = useStorageContext();
  const [headerEditor, setHeaderEditor] = useState(
    null
  );
  const [queryEditor, setQueryEditor] = useState(null);
  const [responseEditor, setResponseEditor] = useState(
    null
  );
  const [variableEditor, setVariableEditor] = useState(
    null
  );
  const [shouldPersistHeaders, setShouldPersistHeadersInternal] = useState(
    () => {
      const isStored = (storage == null ? void 0 : storage.get(PERSIST_HEADERS_STORAGE_KEY)) !== null;
      return props.shouldPersistHeaders !== false && isStored ? (storage == null ? void 0 : storage.get(PERSIST_HEADERS_STORAGE_KEY)) === "true" : Boolean(props.shouldPersistHeaders);
    }
  );
  useSynchronizeValue(headerEditor, props.headers);
  useSynchronizeValue(queryEditor, props.query);
  useSynchronizeValue(responseEditor, props.response);
  useSynchronizeValue(variableEditor, props.variables);
  const storeTabs = useStoreTabs({
    storage,
    shouldPersistHeaders
  });
  const [initialState] = useState(() => {
    const query = props.query ?? (storage == null ? void 0 : storage.get(STORAGE_KEY_QUERY)) ?? null;
    const variables = props.variables ?? (storage == null ? void 0 : storage.get(STORAGE_KEY$1)) ?? null;
    const headers = props.headers ?? (storage == null ? void 0 : storage.get(STORAGE_KEY$3)) ?? null;
    const response = props.response ?? "";
    const tabState2 = getDefaultTabState({
      query,
      variables,
      headers,
      defaultTabs: props.defaultTabs,
      defaultQuery: props.defaultQuery || DEFAULT_QUERY,
      defaultHeaders: props.defaultHeaders,
      storage,
      shouldPersistHeaders
    });
    storeTabs(tabState2);
    return {
      query: query ?? (tabState2.activeTabIndex === 0 ? tabState2.tabs[0].query : null) ?? "",
      variables: variables ?? "",
      headers: headers ?? props.defaultHeaders ?? "",
      response,
      tabState: tabState2
    };
  });
  const [tabState, setTabState] = useState(initialState.tabState);
  const setShouldPersistHeaders = useCallback(
    (persist) => {
      if (persist) {
        storage == null ? void 0 : storage.set(STORAGE_KEY$3, (headerEditor == null ? void 0 : headerEditor.getValue()) ?? "");
        const serializedTabs = serializeTabState(tabState, true);
        storage == null ? void 0 : storage.set(STORAGE_KEY$2, serializedTabs);
      } else {
        storage == null ? void 0 : storage.set(STORAGE_KEY$3, "");
        clearHeadersFromTabs(storage);
      }
      setShouldPersistHeadersInternal(persist);
      storage == null ? void 0 : storage.set(PERSIST_HEADERS_STORAGE_KEY, persist.toString());
    },
    [storage, tabState, headerEditor]
  );
  const lastShouldPersistHeadersProp = useRef();
  useEffect(() => {
    const propValue = Boolean(props.shouldPersistHeaders);
    if ((lastShouldPersistHeadersProp == null ? void 0 : lastShouldPersistHeadersProp.current) !== propValue) {
      setShouldPersistHeaders(propValue);
      lastShouldPersistHeadersProp.current = propValue;
    }
  }, [props.shouldPersistHeaders, setShouldPersistHeaders]);
  const synchronizeActiveTabValues = useSynchronizeActiveTabValues({
    queryEditor,
    variableEditor,
    headerEditor,
    responseEditor
  });
  const { onTabChange, defaultHeaders, defaultQuery, children } = props;
  const setEditorValues = useSetEditorValues({
    queryEditor,
    variableEditor,
    headerEditor,
    responseEditor,
    defaultHeaders
  });
  const addTab = useCallback(() => {
    setTabState((current) => {
      const updatedValues = synchronizeActiveTabValues(current);
      const updated = {
        tabs: [
          ...updatedValues.tabs,
          createTab({
            headers: defaultHeaders,
            query: defaultQuery ?? DEFAULT_QUERY
          })
        ],
        activeTabIndex: updatedValues.tabs.length
      };
      storeTabs(updated);
      setEditorValues(updated.tabs[updated.activeTabIndex]);
      onTabChange == null ? void 0 : onTabChange(updated);
      return updated;
    });
  }, [
    defaultHeaders,
    defaultQuery,
    onTabChange,
    setEditorValues,
    storeTabs,
    synchronizeActiveTabValues
  ]);
  const changeTab = useCallback(
    (index) => {
      setTabState((current) => {
        const updated = {
          ...current,
          activeTabIndex: index
        };
        storeTabs(updated);
        setEditorValues(updated.tabs[updated.activeTabIndex]);
        onTabChange == null ? void 0 : onTabChange(updated);
        return updated;
      });
    },
    [onTabChange, setEditorValues, storeTabs]
  );
  const moveTab = useCallback(
    (newOrder) => {
      setTabState((current) => {
        const activeTab = current.tabs[current.activeTabIndex];
        const updated = {
          tabs: newOrder,
          activeTabIndex: newOrder.indexOf(activeTab)
        };
        storeTabs(updated);
        setEditorValues(updated.tabs[updated.activeTabIndex]);
        onTabChange == null ? void 0 : onTabChange(updated);
        return updated;
      });
    },
    [onTabChange, setEditorValues, storeTabs]
  );
  const closeTab = useCallback(
    (index) => {
      setTabState((current) => {
        const updated = {
          tabs: current.tabs.filter((_tab, i) => index !== i),
          activeTabIndex: Math.max(current.activeTabIndex - 1, 0)
        };
        storeTabs(updated);
        setEditorValues(updated.tabs[updated.activeTabIndex]);
        onTabChange == null ? void 0 : onTabChange(updated);
        return updated;
      });
    },
    [onTabChange, setEditorValues, storeTabs]
  );
  const updateActiveTabValues = useCallback(
    (partialTab) => {
      setTabState((current) => {
        const updated = setPropertiesInActiveTab(current, partialTab);
        storeTabs(updated);
        onTabChange == null ? void 0 : onTabChange(updated);
        return updated;
      });
    },
    [onTabChange, storeTabs]
  );
  const { onEditOperationName } = props;
  const setOperationName = useCallback(
    (operationName) => {
      if (!queryEditor) {
        return;
      }
      queryEditor.operationName = operationName;
      updateActiveTabValues({ operationName });
      onEditOperationName == null ? void 0 : onEditOperationName(operationName);
    },
    [onEditOperationName, queryEditor, updateActiveTabValues]
  );
  const externalFragments = useMemo(() => {
    const map = /* @__PURE__ */ new Map();
    if (Array.isArray(props.externalFragments)) {
      for (const fragment of props.externalFragments) {
        map.set(fragment.name.value, fragment);
      }
    } else if (typeof props.externalFragments === "string") {
      visit(parse(props.externalFragments, {}), {
        FragmentDefinition(fragment) {
          map.set(fragment.name.value, fragment);
        }
      });
    } else if (props.externalFragments) {
      throw new Error(
        "The `externalFragments` prop must either be a string that contains the fragment definitions in SDL or a list of FragmentDefinitionNode objects."
      );
    }
    return map;
  }, [props.externalFragments]);
  const validationRules = useMemo(
    () => props.validationRules || [],
    [props.validationRules]
  );
  const value = useMemo(
    () => ({
      ...tabState,
      addTab,
      changeTab,
      moveTab,
      closeTab,
      updateActiveTabValues,
      headerEditor,
      queryEditor,
      responseEditor,
      variableEditor,
      setHeaderEditor,
      setQueryEditor,
      setResponseEditor,
      setVariableEditor,
      setOperationName,
      initialQuery: initialState.query,
      initialVariables: initialState.variables,
      initialHeaders: initialState.headers,
      initialResponse: initialState.response,
      externalFragments,
      validationRules,
      shouldPersistHeaders,
      setShouldPersistHeaders
    }),
    [
      tabState,
      addTab,
      changeTab,
      moveTab,
      closeTab,
      updateActiveTabValues,
      headerEditor,
      queryEditor,
      responseEditor,
      variableEditor,
      setOperationName,
      initialState,
      externalFragments,
      validationRules,
      shouldPersistHeaders,
      setShouldPersistHeaders
    ]
  );
  return /* @__PURE__ */ jsx(EditorContext.Provider, { value, children });
}
const useEditorContext = createContextHook(EditorContext);
const PERSIST_HEADERS_STORAGE_KEY = "shouldPersistHeaders";
const DEFAULT_QUERY = `# Welcome to GraphiQL
#
# GraphiQL is an in-browser tool for writing, validating, and
# testing GraphQL queries.
#
# Type queries into this side of the screen, and you will see intelligent
# typeaheads aware of the current GraphQL type schema and live syntax and
# validation errors highlighted within the text.
#
# GraphQL queries typically start with a "{" character. Lines that start
# with a # are ignored.
#
# An example GraphQL query might look like:
#
#     {
#       field(arg: "value") {
#         subField
#       }
#     }
#
# Keyboard shortcuts:
#
#   Prettify query:  Shift-Ctrl-P (or press the prettify button)
#
#  Merge fragments:  Shift-Ctrl-M (or press the merge button)
#
#        Run Query:  Ctrl-Enter (or press the play button)
#
#    Auto Complete:  Ctrl-Space (or just start typing)
#

`;
function HeaderEditor({ isHidden, ...hookArgs }) {
  const { headerEditor } = useEditorContext({
    nonNull: true,
    caller: HeaderEditor
  });
  const ref = useHeaderEditor(hookArgs, HeaderEditor);
  useEffect(() => {
    if (!isHidden) {
      headerEditor == null ? void 0 : headerEditor.refresh();
    }
  }, [headerEditor, isHidden]);
  return /* @__PURE__ */ jsx("div", { className: clsx("graphiql-editor", isHidden && "hidden"), ref });
}
function ImagePreview(props) {
  var _a;
  const [dimensions, setDimensions] = useState({
    width: null,
    height: null
  });
  const [mime, setMime] = useState(null);
  const ref = useRef(null);
  const src = (_a = tokenToURL(props.token)) == null ? void 0 : _a.href;
  useEffect(() => {
    if (!ref.current) {
      return;
    }
    if (!src) {
      setDimensions({ width: null, height: null });
      setMime(null);
      return;
    }
    fetch(src, { method: "HEAD" }).then((response) => {
      setMime(response.headers.get("Content-Type"));
    }).catch(() => {
      setMime(null);
    });
  }, [src]);
  const dims = dimensions.width !== null && dimensions.height !== null ? /* @__PURE__ */ jsxs("div", { children: [
    dimensions.width,
    "x",
    dimensions.height,
    mime === null ? null : " " + mime
  ] }) : null;
  return /* @__PURE__ */ jsxs("div", { children: [
    /* @__PURE__ */ jsx(
      "img",
      {
        onLoad: () => {
          var _a2, _b;
          setDimensions({
            width: ((_a2 = ref.current) == null ? void 0 : _a2.naturalWidth) ?? null,
            height: ((_b = ref.current) == null ? void 0 : _b.naturalHeight) ?? null
          });
        },
        ref,
        src
      }
    ),
    dims
  ] });
}
ImagePreview.shouldRender = function shouldRender(token) {
  const url = tokenToURL(token);
  return url ? isImageURL(url) : false;
};
function tokenToURL(token) {
  if (token.type !== "string") {
    return;
  }
  const value = token.string.slice(1).slice(0, -1).trim();
  try {
    const { location } = window;
    return new URL(value, location.protocol + "//" + location.host);
  } catch {
    return;
  }
}
function isImageURL(url) {
  return /\.(bmp|gif|jpe?g|png|svg|webp)$/.test(url.pathname);
}
function QueryEditor(props) {
  const ref = useQueryEditor(props, QueryEditor);
  return /* @__PURE__ */ jsx("div", { className: "graphiql-editor", ref });
}
function useResponseEditor({
  responseTooltip,
  editorTheme = DEFAULT_EDITOR_THEME,
  keyMap = DEFAULT_KEY_MAP
} = {}, caller) {
  const { fetchError, validationErrors } = useSchemaContext({
    nonNull: true,
    caller: caller || useResponseEditor
  });
  const { initialResponse, responseEditor, setResponseEditor } = useEditorContext({
    nonNull: true,
    caller: caller || useResponseEditor
  });
  const ref = useRef(null);
  const responseTooltipRef = useRef(
    responseTooltip
  );
  useEffect(() => {
    responseTooltipRef.current = responseTooltip;
  }, [responseTooltip]);
  useEffect(() => {
    let isActive = true;
    void importCodeMirror(
      [
        import("./foldgutter.es.js").then((n) => n.f),
        import("./brace-fold.es.js").then((n) => n.b),
        import("./dialog.es.js").then((n) => n.d),
        import("./search.es.js").then((n) => n.s),
        import("./searchcursor.es.js").then((n) => n.s),
        import("./jump-to-line.es.js").then((n) => n.j),
        // @ts-expect-error
        import("./sublime.es.js").then((n) => n.s),
        import("./mode.es3.js"),
        import("./info-addon.es.js")
      ],
      { useCommonAddons: false }
    ).then((CodeMirror) => {
      if (!isActive) {
        return;
      }
      const tooltipDiv = document.createElement("div");
      CodeMirror.registerHelper(
        "info",
        "graphql-results",
        (token, _options, _cm, pos) => {
          const infoElements = [];
          const ResponseTooltipComponent = responseTooltipRef.current;
          if (ResponseTooltipComponent) {
            infoElements.push(
              /* @__PURE__ */ jsx(ResponseTooltipComponent, { pos, token })
            );
          }
          if (ImagePreview.shouldRender(token)) {
            infoElements.push(
              /* @__PURE__ */ jsx(ImagePreview, { token }, "image-preview")
            );
          }
          if (!infoElements.length) {
            ReactDOM.unmountComponentAtNode(tooltipDiv);
            return null;
          }
          ReactDOM.render(infoElements, tooltipDiv);
          return tooltipDiv;
        }
      );
      const container = ref.current;
      if (!container) {
        return;
      }
      const newEditor = CodeMirror(container, {
        value: initialResponse,
        lineWrapping: true,
        readOnly: true,
        theme: editorTheme,
        mode: "graphql-results",
        foldGutter: true,
        gutters: ["CodeMirror-foldgutter"],
        // @ts-expect-error
        info: true,
        extraKeys: commonKeys
      });
      setResponseEditor(newEditor);
    });
    return () => {
      isActive = false;
    };
  }, [editorTheme, initialResponse, setResponseEditor]);
  useSynchronizeOption(responseEditor, "keyMap", keyMap);
  useEffect(() => {
    if (fetchError) {
      responseEditor == null ? void 0 : responseEditor.setValue(fetchError);
    }
    if (validationErrors.length > 0) {
      responseEditor == null ? void 0 : responseEditor.setValue(formatError(validationErrors));
    }
  }, [responseEditor, fetchError, validationErrors]);
  return ref;
}
function ResponseEditor(props) {
  const ref = useResponseEditor(props, ResponseEditor);
  return /* @__PURE__ */ jsx(
    "section",
    {
      className: "result-window",
      "aria-label": "Result Window",
      "aria-live": "polite",
      "aria-atomic": "true",
      ref
    }
  );
}
function VariableEditor({ isHidden, ...hookArgs }) {
  const { variableEditor } = useEditorContext({
    nonNull: true,
    caller: VariableEditor
  });
  const ref = useVariableEditor(hookArgs, VariableEditor);
  useEffect(() => {
    if (variableEditor && !isHidden) {
      variableEditor.refresh();
    }
  }, [variableEditor, isHidden]);
  return /* @__PURE__ */ jsx("div", { className: clsx("graphiql-editor", isHidden && "hidden"), ref });
}
function GraphiQLProvider({
  children,
  dangerouslyAssumeSchemaIsValid,
  defaultQuery,
  defaultHeaders,
  defaultTabs,
  externalFragments,
  fetcher,
  getDefaultFieldNames,
  headers,
  inputValueDeprecation,
  introspectionQueryName,
  maxHistoryLength,
  onEditOperationName,
  onSchemaChange,
  onTabChange,
  onTogglePluginVisibility,
  operationName,
  plugins,
  query,
  response,
  schema,
  schemaDescription,
  shouldPersistHeaders,
  storage,
  validationRules,
  variables,
  visiblePlugin
}) {
  return /* @__PURE__ */ jsx(StorageContextProvider, { storage, children: /* @__PURE__ */ jsx(HistoryContextProvider, { maxHistoryLength, children: /* @__PURE__ */ jsx(
    EditorContextProvider,
    {
      defaultQuery,
      defaultHeaders,
      defaultTabs,
      externalFragments,
      headers,
      onEditOperationName,
      onTabChange,
      query,
      response,
      shouldPersistHeaders,
      validationRules,
      variables,
      children: /* @__PURE__ */ jsx(
        SchemaContextProvider,
        {
          dangerouslyAssumeSchemaIsValid,
          fetcher,
          inputValueDeprecation,
          introspectionQueryName,
          onSchemaChange,
          schema,
          schemaDescription,
          children: /* @__PURE__ */ jsx(
            ExecutionContextProvider,
            {
              getDefaultFieldNames,
              fetcher,
              operationName,
              children: /* @__PURE__ */ jsx(ExplorerContextProvider, { children: /* @__PURE__ */ jsx(
                PluginContextProvider,
                {
                  onTogglePluginVisibility,
                  plugins,
                  visiblePlugin,
                  children
                }
              ) })
            }
          )
        }
      )
    }
  ) }) });
}
function useTheme(defaultTheme = null) {
  const storageContext = useStorageContext();
  const [theme, setThemeInternal] = useState(() => {
    if (!storageContext) {
      return null;
    }
    const stored = storageContext.get(STORAGE_KEY);
    switch (stored) {
      case "light":
        return "light";
      case "dark":
        return "dark";
      default:
        if (typeof stored === "string") {
          storageContext.set(STORAGE_KEY, "");
        }
        return defaultTheme;
    }
  });
  useLayoutEffect(() => {
    if (typeof window === "undefined") {
      return;
    }
    document.body.classList.remove("graphiql-light", "graphiql-dark");
    if (theme) {
      document.body.classList.add(`graphiql-${theme}`);
    }
  }, [theme]);
  const setTheme = useCallback(
    (newTheme) => {
      storageContext == null ? void 0 : storageContext.set(STORAGE_KEY, newTheme || "");
      setThemeInternal(newTheme);
    },
    [storageContext]
  );
  return useMemo(() => ({ theme, setTheme }), [theme, setTheme]);
}
const STORAGE_KEY = "theme";
function useDragResize({
  defaultSizeRelation = DEFAULT_FLEX,
  direction,
  initiallyHidden,
  onHiddenElementChange,
  sizeThresholdFirst = 100,
  sizeThresholdSecond = 100,
  storageKey
}) {
  const storage = useStorageContext();
  const store = useMemo(
    () => debounce(500, (value) => {
      if (storageKey) {
        storage == null ? void 0 : storage.set(storageKey, value);
      }
    }),
    [storage, storageKey]
  );
  const [hiddenElement, setHiddenElement] = useState(
    () => {
      const storedValue = storageKey && (storage == null ? void 0 : storage.get(storageKey));
      if (storedValue === HIDE_FIRST || initiallyHidden === "first") {
        return "first";
      }
      if (storedValue === HIDE_SECOND || initiallyHidden === "second") {
        return "second";
      }
      return null;
    }
  );
  const setHiddenElementWithCallback = useCallback(
    (element) => {
      if (element !== hiddenElement) {
        setHiddenElement(element);
        onHiddenElementChange == null ? void 0 : onHiddenElementChange(element);
      }
    },
    [hiddenElement, onHiddenElementChange]
  );
  const firstRef = useRef(null);
  const dragBarRef = useRef(null);
  const secondRef = useRef(null);
  const defaultFlexRef = useRef(`${defaultSizeRelation}`);
  useLayoutEffect(() => {
    const storedValue = storageKey && (storage == null ? void 0 : storage.get(storageKey)) || defaultFlexRef.current;
    if (firstRef.current) {
      firstRef.current.style.display = "flex";
      firstRef.current.style.flex = storedValue === HIDE_FIRST || storedValue === HIDE_SECOND ? defaultFlexRef.current : storedValue;
    }
    if (secondRef.current) {
      secondRef.current.style.display = "flex";
      secondRef.current.style.flex = "1";
    }
    if (dragBarRef.current) {
      dragBarRef.current.style.display = "flex";
    }
  }, [direction, storage, storageKey]);
  const hide = useCallback((resizableElement) => {
    const element = resizableElement === "first" ? firstRef.current : secondRef.current;
    if (!element) {
      return;
    }
    element.style.left = "-1000px";
    element.style.position = "absolute";
    element.style.opacity = "0";
    element.style.height = "500px";
    element.style.width = "500px";
    if (firstRef.current) {
      const flex = parseFloat(firstRef.current.style.flex);
      if (!Number.isFinite(flex) || flex < 1) {
        firstRef.current.style.flex = "1";
      }
    }
  }, []);
  const show = useCallback(
    (resizableElement) => {
      const element = resizableElement === "first" ? firstRef.current : secondRef.current;
      if (!element) {
        return;
      }
      element.style.width = "";
      element.style.height = "";
      element.style.opacity = "";
      element.style.position = "";
      element.style.left = "";
      if (storage && storageKey) {
        const storedValue = storage.get(storageKey);
        if (firstRef.current && storedValue !== HIDE_FIRST && storedValue !== HIDE_SECOND) {
          firstRef.current.style.flex = storedValue || defaultFlexRef.current;
        }
      }
    },
    [storage, storageKey]
  );
  useLayoutEffect(() => {
    if (hiddenElement === "first") {
      hide("first");
    } else {
      show("first");
    }
    if (hiddenElement === "second") {
      hide("second");
    } else {
      show("second");
    }
  }, [hiddenElement, hide, show]);
  useEffect(() => {
    if (!dragBarRef.current || !firstRef.current || !secondRef.current) {
      return;
    }
    const dragBarContainer = dragBarRef.current;
    const firstContainer = firstRef.current;
    const wrapper = firstContainer.parentElement;
    const eventProperty = direction === "horizontal" ? "clientX" : "clientY";
    const rectProperty = direction === "horizontal" ? "left" : "top";
    const adjacentRectProperty = direction === "horizontal" ? "right" : "bottom";
    const sizeProperty = direction === "horizontal" ? "clientWidth" : "clientHeight";
    function handleMouseDown(downEvent) {
      downEvent.preventDefault();
      const offset = downEvent[eventProperty] - dragBarContainer.getBoundingClientRect()[rectProperty];
      function handleMouseMove(moveEvent) {
        if (moveEvent.buttons === 0) {
          return handleMouseUp();
        }
        const firstSize = moveEvent[eventProperty] - wrapper.getBoundingClientRect()[rectProperty] - offset;
        const secondSize = wrapper.getBoundingClientRect()[adjacentRectProperty] - moveEvent[eventProperty] + offset - dragBarContainer[sizeProperty];
        if (firstSize < sizeThresholdFirst) {
          setHiddenElementWithCallback("first");
          store(HIDE_FIRST);
        } else if (secondSize < sizeThresholdSecond) {
          setHiddenElementWithCallback("second");
          store(HIDE_SECOND);
        } else {
          setHiddenElementWithCallback(null);
          const newFlex = `${firstSize / secondSize}`;
          firstContainer.style.flex = newFlex;
          store(newFlex);
        }
      }
      function handleMouseUp() {
        document.removeEventListener("mousemove", handleMouseMove);
        document.removeEventListener("mouseup", handleMouseUp);
      }
      document.addEventListener("mousemove", handleMouseMove);
      document.addEventListener("mouseup", handleMouseUp);
    }
    dragBarContainer.addEventListener("mousedown", handleMouseDown);
    function reset() {
      if (firstRef.current) {
        firstRef.current.style.flex = defaultFlexRef.current;
      }
      store(defaultFlexRef.current);
      setHiddenElementWithCallback(null);
    }
    dragBarContainer.addEventListener("dblclick", reset);
    return () => {
      dragBarContainer.removeEventListener("mousedown", handleMouseDown);
      dragBarContainer.removeEventListener("dblclick", reset);
    };
  }, [
    direction,
    setHiddenElementWithCallback,
    sizeThresholdFirst,
    sizeThresholdSecond,
    store
  ]);
  return useMemo(
    () => ({
      dragBarRef,
      hiddenElement,
      firstRef,
      setHiddenElement,
      secondRef
    }),
    [hiddenElement, setHiddenElement]
  );
}
const DEFAULT_FLEX = 1;
const HIDE_FIRST = "hide-first";
const HIDE_SECOND = "hide-second";
const ToolbarButton = forwardRef(({ label, onClick, ...props }, ref) => {
  const [error, setError] = useState(null);
  const handleClick = useCallback(
    (event) => {
      try {
        onClick == null ? void 0 : onClick(event);
        setError(null);
      } catch (err) {
        setError(
          err instanceof Error ? err : new Error(`Toolbar button click failed: ${err}`)
        );
      }
    },
    [onClick]
  );
  return /* @__PURE__ */ jsx(Tooltip, { label, children: /* @__PURE__ */ jsx(
    UnStyledButton,
    {
      ...props,
      ref,
      type: "button",
      className: clsx(
        "graphiql-toolbar-button",
        error && "error",
        props.className
      ),
      onClick: handleClick,
      "aria-label": error ? error.message : label,
      "aria-invalid": error ? "true" : props["aria-invalid"]
    }
  ) });
});
ToolbarButton.displayName = "ToolbarButton";
function ExecuteButton() {
  const { queryEditor, setOperationName } = useEditorContext({
    nonNull: true,
    caller: ExecuteButton
  });
  const { isFetching, isSubscribed, operationName, run, stop } = useExecutionContext({
    nonNull: true,
    caller: ExecuteButton
  });
  const operations = (queryEditor == null ? void 0 : queryEditor.operations) || [];
  const hasOptions = operations.length > 1 && typeof operationName !== "string";
  const isRunning = isFetching || isSubscribed;
  const label = `${isRunning ? "Stop" : "Execute"} query (Ctrl-Enter)`;
  const buttonProps = {
    type: "button",
    className: "graphiql-execute-button",
    children: isRunning ? /* @__PURE__ */ jsx(StopIcon, {}) : /* @__PURE__ */ jsx(PlayIcon, {}),
    "aria-label": label
  };
  return hasOptions && !isRunning ? /* @__PURE__ */ jsxs(DropdownMenu, { children: [
    /* @__PURE__ */ jsx(Tooltip, { label, children: /* @__PURE__ */ jsx(DropdownMenu.Button, { ...buttonProps }) }),
    /* @__PURE__ */ jsx(DropdownMenu.Content, { children: operations.map((operation, i) => {
      const opName = operation.name ? operation.name.value : `<Unnamed ${operation.operation}>`;
      return /* @__PURE__ */ jsx(
        DropdownMenu.Item,
        {
          onSelect: () => {
            var _a;
            const selectedOperationName = (_a = operation.name) == null ? void 0 : _a.value;
            if (queryEditor && selectedOperationName && selectedOperationName !== queryEditor.operationName) {
              setOperationName(selectedOperationName);
            }
            run();
          },
          children: opName
        },
        `${opName}-${i}`
      );
    }) })
  ] }) : /* @__PURE__ */ jsx(Tooltip, { label, children: /* @__PURE__ */ jsx(
    "button",
    {
      ...buttonProps,
      onClick: () => {
        if (isRunning) {
          stop();
        } else {
          run();
        }
      }
    }
  ) });
}
const ToolbarMenuRoot = ({
  button,
  children,
  label,
  ...props
}) => /* @__PURE__ */ jsxs(DropdownMenu, { ...props, children: [
  /* @__PURE__ */ jsx(Tooltip, { label, children: /* @__PURE__ */ jsx(
    DropdownMenu.Button,
    {
      className: clsx(
        "graphiql-un-styled graphiql-toolbar-menu",
        props.className
      ),
      "aria-label": label,
      children: button
    }
  ) }),
  /* @__PURE__ */ jsx(DropdownMenu.Content, { children })
] });
const ToolbarMenu = createComponentGroup(ToolbarMenuRoot, {
  Item: DropdownMenu.Item
});
export {
  Argument,
  ArgumentIcon,
  Button$1 as Button,
  ButtonGroup,
  ChevronDownIcon,
  ChevronLeftIcon,
  ChevronUpIcon,
  CloseIcon,
  CopyIcon,
  DOC_EXPLORER_PLUGIN,
  DefaultValue,
  DeprecatedArgumentIcon,
  DeprecatedEnumValueIcon,
  DeprecatedFieldIcon,
  DeprecationReason,
  Dialog,
  DialogRoot,
  Directive,
  DirectiveIcon,
  DocExplorer,
  DocsFilledIcon,
  DocsIcon,
  DropdownMenu,
  EditorContext,
  EditorContextProvider,
  EnumValueIcon,
  ExecuteButton,
  ExecutionContext,
  ExecutionContextProvider,
  ExplorerContext,
  ExplorerContextProvider,
  ExplorerSection,
  FieldDocumentation,
  FieldIcon,
  FieldLink,
  GraphiQLProvider,
  HISTORY_PLUGIN,
  HeaderEditor,
  History,
  HistoryContext,
  HistoryContextProvider,
  HistoryIcon,
  ImagePreview,
  ImplementsIcon,
  KeyboardShortcutIcon,
  MagnifyingGlassIcon,
  MarkdownContent,
  MergeIcon,
  PenIcon,
  PlayIcon,
  PluginContext,
  PluginContextProvider,
  PlusIcon,
  PrettifyIcon,
  QueryEditor,
  ReloadIcon,
  ResponseEditor,
  RootTypeIcon,
  SchemaContext,
  SchemaContextProvider,
  SchemaDocumentation,
  Search,
  SettingsIcon,
  Spinner,
  StarFilledIcon,
  StarIcon,
  StopIcon,
  StorageContext,
  StorageContextProvider,
  Tab,
  Tabs,
  ToolbarButton,
  ToolbarMenu,
  Tooltip,
  TooltipRoot,
  TrashIcon,
  TypeDocumentation,
  TypeIcon,
  TypeLink,
  UnStyledButton,
  VariableEditor,
  isMacOs,
  useAutoCompleteLeafs,
  useCopyQuery,
  useDragResize,
  useEditorContext,
  useEditorState,
  useExecutionContext,
  useExplorerContext,
  useHeaderEditor,
  useHeadersEditorState,
  useHistoryContext,
  useMergeQuery,
  useOperationsEditorState,
  useOptimisticState,
  usePluginContext,
  usePrettifyEditors,
  useQueryEditor,
  useResponseEditor,
  useSchemaContext,
  useStorageContext,
  useTheme,
  useVariableEditor,
  useVariablesEditorState
};
//# sourceMappingURL=index.mjs.map
