"use strict";
const codemirror = require("./codemirror.cjs2.js");
const dialog = require("./dialog.cjs.js");
function _mergeNamespaces(n, m) {
  for (var i = 0; i < m.length; i++) {
    const e = m[i];
    if (typeof e !== "string" && !Array.isArray(e)) {
      for (const k in e) {
        if (k !== "default" && !(k in n)) {
          const d = Object.getOwnPropertyDescriptor(e, k);
          if (d) {
            Object.defineProperty(n, k, d.get ? d : {
              enumerable: true,
              get: () => e[k]
            });
          }
        }
      }
    }
  }
  return Object.freeze(Object.defineProperty(n, Symbol.toStringTag, { value: "Module" }));
}
var jumpToLine$2 = { exports: {} };
(function(module2, exports2) {
  (function(mod) {
    mod(codemirror.requireCodemirror(), dialog.dialogExports);
  })(function(CodeMirror) {
    CodeMirror.defineOption("search", { bottom: false });
    function dialog2(cm, text, shortText, deflt, f) {
      if (cm.openDialog) cm.openDialog(text, f, { value: deflt, selectValueOnOpen: true, bottom: cm.options.search.bottom });
      else f(prompt(shortText, deflt));
    }
    function getJumpDialog(cm) {
      return cm.phrase("Jump to line:") + ' <input type="text" style="width: 10em" class="CodeMirror-search-field"/> <span style="color: #888" class="CodeMirror-search-hint">' + cm.phrase("(Use line:column or scroll% syntax)") + "</span>";
    }
    function interpretLine(cm, string) {
      var num = Number(string);
      if (/^[-+]/.test(string)) return cm.getCursor().line + num;
      else return num - 1;
    }
    CodeMirror.commands.jumpToLine = function(cm) {
      var cur = cm.getCursor();
      dialog2(cm, getJumpDialog(cm), cm.phrase("Jump to line:"), cur.line + 1 + ":" + cur.ch, function(posStr) {
        if (!posStr) return;
        var match;
        if (match = /^\s*([\+\-]?\d+)\s*\:\s*(\d+)\s*$/.exec(posStr)) {
          cm.setCursor(interpretLine(cm, match[1]), Number(match[2]));
        } else if (match = /^\s*([\+\-]?\d+(\.\d+)?)\%\s*/.exec(posStr)) {
          var line = Math.round(cm.lineCount() * Number(match[1]) / 100);
          if (/^[-+]/.test(match[1])) line = cur.line + line + 1;
          cm.setCursor(line - 1, cur.ch);
        } else if (match = /^\s*\:?\s*([\+\-]?\d+)\s*/.exec(posStr)) {
          cm.setCursor(interpretLine(cm, match[1]), cur.ch);
        }
      });
    };
    CodeMirror.keyMap["default"]["Alt-G"] = "jumpToLine";
  });
})();
var jumpToLineExports = jumpToLine$2.exports;
const jumpToLine = /* @__PURE__ */ codemirror.getDefaultExportFromCjs(jumpToLineExports);
const jumpToLine$1 = /* @__PURE__ */ _mergeNamespaces({
  __proto__: null,
  default: jumpToLine
}, [jumpToLineExports]);
exports.jumpToLine = jumpToLine$1;
//# sourceMappingURL=jump-to-line.cjs.js.map
