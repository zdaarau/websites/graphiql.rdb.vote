"use strict";
const codemirror$1 = require("./codemirror.cjs2.js");
function _mergeNamespaces(n, m) {
  for (var i = 0; i < m.length; i++) {
    const e = m[i];
    if (typeof e !== "string" && !Array.isArray(e)) {
      for (const k in e) {
        if (k !== "default" && !(k in n)) {
          const d = Object.getOwnPropertyDescriptor(e, k);
          if (d) {
            Object.defineProperty(n, k, d.get ? d : {
              enumerable: true,
              get: () => e[k]
            });
          }
        }
      }
    }
  }
  return Object.freeze(Object.defineProperty(n, Symbol.toStringTag, { value: "Module" }));
}
var codemirrorExports = codemirror$1.requireCodemirror();
const CodeMirror = /* @__PURE__ */ codemirror$1.getDefaultExportFromCjs(codemirrorExports);
const codemirror = /* @__PURE__ */ _mergeNamespaces({
  __proto__: null,
  default: CodeMirror
}, [codemirrorExports]);
exports.CodeMirror = CodeMirror;
exports.codemirror = codemirror;
//# sourceMappingURL=codemirror.cjs.js.map
