"use strict";
const codemirror = require("./codemirror.cjs.js");
const graphqlLanguageService = require("graphql-language-service");
const modeIndent = require("./mode-indent.cjs.js");
const graphqlModeFactory = (config) => {
  const parser = graphqlLanguageService.onlineParser({
    eatWhitespace: (stream) => stream.eatWhile(graphqlLanguageService.isIgnored),
    lexRules: graphqlLanguageService.LexRules,
    parseRules: graphqlLanguageService.ParseRules,
    editorConfig: { tabSize: config.tabSize }
  });
  return {
    config,
    startState: parser.startState,
    token: parser.token,
    indent: modeIndent.indent,
    electricInput: /^\s*[})\]]/,
    fold: "brace",
    lineComment: "#",
    closeBrackets: {
      pairs: '()[]{}""',
      explode: "()[]{}"
    }
  };
};
codemirror.CodeMirror.defineMode("graphql", graphqlModeFactory);
//# sourceMappingURL=mode.cjs.js.map
