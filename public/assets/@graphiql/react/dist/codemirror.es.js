import { r as requireCodemirror, g as getDefaultExportFromCjs } from "./codemirror.es2.js";
function _mergeNamespaces(n, m) {
  for (var i = 0; i < m.length; i++) {
    const e = m[i];
    if (typeof e !== "string" && !Array.isArray(e)) {
      for (const k in e) {
        if (k !== "default" && !(k in n)) {
          const d = Object.getOwnPropertyDescriptor(e, k);
          if (d) {
            Object.defineProperty(n, k, d.get ? d : {
              enumerable: true,
              get: () => e[k]
            });
          }
        }
      }
    }
  }
  return Object.freeze(Object.defineProperty(n, Symbol.toStringTag, { value: "Module" }));
}
var codemirrorExports = requireCodemirror();
const CodeMirror = /* @__PURE__ */ getDefaultExportFromCjs(codemirrorExports);
const codemirror = /* @__PURE__ */ _mergeNamespaces({
  __proto__: null,
  default: CodeMirror
}, [codemirrorExports]);
export {
  CodeMirror as C,
  codemirror as c
};
//# sourceMappingURL=codemirror.es.js.map
