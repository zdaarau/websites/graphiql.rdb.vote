import { g as getDefaultExportFromCjs } from "./codemirror.es2.js";
import { r as requireSearchcursor } from "./searchcursor.es2.js";
function _mergeNamespaces(n, m) {
  for (var i = 0; i < m.length; i++) {
    const e = m[i];
    if (typeof e !== "string" && !Array.isArray(e)) {
      for (const k in e) {
        if (k !== "default" && !(k in n)) {
          const d = Object.getOwnPropertyDescriptor(e, k);
          if (d) {
            Object.defineProperty(n, k, d.get ? d : {
              enumerable: true,
              get: () => e[k]
            });
          }
        }
      }
    }
  }
  return Object.freeze(Object.defineProperty(n, Symbol.toStringTag, { value: "Module" }));
}
var searchcursorExports = requireSearchcursor();
const searchcursor = /* @__PURE__ */ getDefaultExportFromCjs(searchcursorExports);
const searchcursor$1 = /* @__PURE__ */ _mergeNamespaces({
  __proto__: null,
  default: searchcursor
}, [searchcursorExports]);
export {
  searchcursor$1 as s
};
//# sourceMappingURL=searchcursor.es.js.map
