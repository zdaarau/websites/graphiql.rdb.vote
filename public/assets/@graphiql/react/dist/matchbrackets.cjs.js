"use strict";
const codemirror = require("./codemirror.cjs2.js");
const matchbrackets$2 = require("./matchbrackets.cjs2.js");
function _mergeNamespaces(n, m) {
  for (var i = 0; i < m.length; i++) {
    const e = m[i];
    if (typeof e !== "string" && !Array.isArray(e)) {
      for (const k in e) {
        if (k !== "default" && !(k in n)) {
          const d = Object.getOwnPropertyDescriptor(e, k);
          if (d) {
            Object.defineProperty(n, k, d.get ? d : {
              enumerable: true,
              get: () => e[k]
            });
          }
        }
      }
    }
  }
  return Object.freeze(Object.defineProperty(n, Symbol.toStringTag, { value: "Module" }));
}
var matchbracketsExports = matchbrackets$2.requireMatchbrackets();
const matchbrackets = /* @__PURE__ */ codemirror.getDefaultExportFromCjs(matchbracketsExports);
const matchbrackets$1 = /* @__PURE__ */ _mergeNamespaces({
  __proto__: null,
  default: matchbrackets
}, [matchbracketsExports]);
exports.matchbrackets = matchbrackets$1;
//# sourceMappingURL=matchbrackets.cjs.js.map
