"use strict";
const codemirror = require("./codemirror.cjs.js");
const graphqlLanguageService = require("graphql-language-service");
const SEVERITY = ["error", "warning", "information", "hint"];
const TYPE = {
  "GraphQL: Validation": "validation",
  "GraphQL: Deprecation": "deprecation",
  "GraphQL: Syntax": "syntax"
};
codemirror.CodeMirror.registerHelper("lint", "graphql", (text, options) => {
  const { schema, validationRules, externalFragments } = options;
  const rawResults = graphqlLanguageService.getDiagnostics(text, schema, validationRules, void 0, externalFragments);
  const results = rawResults.map((error) => ({
    message: error.message,
    severity: error.severity ? SEVERITY[error.severity - 1] : SEVERITY[0],
    type: error.source ? TYPE[error.source] : void 0,
    from: codemirror.CodeMirror.Pos(error.range.start.line, error.range.start.character),
    to: codemirror.CodeMirror.Pos(error.range.end.line, error.range.end.character)
  }));
  return results;
});
//# sourceMappingURL=lint.cjs2.js.map
