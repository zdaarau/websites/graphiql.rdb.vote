"use strict";
const codemirror = require("./codemirror.cjs.js");
const graphqlLanguageService = require("graphql-language-service");
const modeIndent = require("./mode-indent.cjs.js");
codemirror.CodeMirror.defineMode("graphql-variables", (config) => {
  const parser = graphqlLanguageService.onlineParser({
    eatWhitespace: (stream) => stream.eatSpace(),
    lexRules: LexRules,
    parseRules: ParseRules,
    editorConfig: { tabSize: config.tabSize }
  });
  return {
    config,
    startState: parser.startState,
    token: parser.token,
    indent: modeIndent.indent,
    electricInput: /^\s*[}\]]/,
    fold: "brace",
    closeBrackets: {
      pairs: '[]{}""',
      explode: "[]{}"
    }
  };
});
const LexRules = {
  Punctuation: /^\[|]|\{|\}|:|,/,
  Number: /^-?(?:0|(?:[1-9][0-9]*))(?:\.[0-9]*)?(?:[eE][+-]?[0-9]+)?/,
  String: /^"(?:[^"\\]|\\(?:"|\/|\\|b|f|n|r|t|u[0-9a-fA-F]{4}))*"?/,
  Keyword: /^true|false|null/
};
const ParseRules = {
  Document: [graphqlLanguageService.p("{"), graphqlLanguageService.list("Variable", graphqlLanguageService.opt(graphqlLanguageService.p(","))), graphqlLanguageService.p("}")],
  Variable: [namedKey("variable"), graphqlLanguageService.p(":"), "Value"],
  Value(token) {
    switch (token.kind) {
      case "Number":
        return "NumberValue";
      case "String":
        return "StringValue";
      case "Punctuation":
        switch (token.value) {
          case "[":
            return "ListValue";
          case "{":
            return "ObjectValue";
        }
        return null;
      case "Keyword":
        switch (token.value) {
          case "true":
          case "false":
            return "BooleanValue";
          case "null":
            return "NullValue";
        }
        return null;
    }
  },
  NumberValue: [graphqlLanguageService.t("Number", "number")],
  StringValue: [graphqlLanguageService.t("String", "string")],
  BooleanValue: [graphqlLanguageService.t("Keyword", "builtin")],
  NullValue: [graphqlLanguageService.t("Keyword", "keyword")],
  ListValue: [graphqlLanguageService.p("["), graphqlLanguageService.list("Value", graphqlLanguageService.opt(graphqlLanguageService.p(","))), graphqlLanguageService.p("]")],
  ObjectValue: [graphqlLanguageService.p("{"), graphqlLanguageService.list("ObjectField", graphqlLanguageService.opt(graphqlLanguageService.p(","))), graphqlLanguageService.p("}")],
  ObjectField: [namedKey("attribute"), graphqlLanguageService.p(":"), "Value"]
};
function namedKey(style) {
  return {
    style,
    match: (token) => token.kind === "String",
    update(state, token) {
      state.name = token.value.slice(1, -1);
    }
  };
}
//# sourceMappingURL=mode.cjs2.js.map
