import { C as CodeMirror } from "./codemirror.es.js";
import { onlineParser, isIgnored, LexRules, ParseRules } from "graphql-language-service";
import { i as indent } from "./mode-indent.es.js";
const graphqlModeFactory = (config) => {
  const parser = onlineParser({
    eatWhitespace: (stream) => stream.eatWhile(isIgnored),
    lexRules: LexRules,
    parseRules: ParseRules,
    editorConfig: { tabSize: config.tabSize }
  });
  return {
    config,
    startState: parser.startState,
    token: parser.token,
    indent,
    electricInput: /^\s*[})\]]/,
    fold: "brace",
    lineComment: "#",
    closeBrackets: {
      pairs: '()[]{}""',
      explode: "()[]{}"
    }
  };
};
CodeMirror.defineMode("graphql", graphqlModeFactory);
//# sourceMappingURL=mode.es.js.map
