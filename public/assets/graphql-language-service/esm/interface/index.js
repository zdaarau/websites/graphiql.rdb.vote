export * from './autocompleteUtils';
export * from './getAutocompleteSuggestions';
export * from './getDefinition';
export * from './getDiagnostics';
export { getOutline } from './getOutline';
export { getHoverInformation } from './getHoverInformation';
//# sourceMappingURL=index.js.map