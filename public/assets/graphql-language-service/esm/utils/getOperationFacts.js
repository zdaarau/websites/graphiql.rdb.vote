import { parse, visit } from 'graphql';
import { collectVariables } from './collectVariables';
export function getOperationASTFacts(documentAST, schema) {
    const variableToType = schema
        ? collectVariables(schema, documentAST)
        : undefined;
    const operations = [];
    visit(documentAST, {
        OperationDefinition(node) {
            operations.push(node);
        },
    });
    return { variableToType, operations };
}
export default function getOperationFacts(schema, documentString) {
    if (!documentString) {
        return;
    }
    try {
        const documentAST = parse(documentString);
        return Object.assign(Object.assign({}, getOperationASTFacts(documentAST, schema)), { documentAST });
    }
    catch (_a) {
        return;
    }
}
export const getQueryFacts = getOperationFacts;
//# sourceMappingURL=getOperationFacts.js.map