export * from './autocompleteUtils';
export * from './getAutocompleteSuggestions';
export * from './getDefinition';
export * from './getDiagnostics';
export { getOutline } from './getOutline';
export { getHoverInformation, HoverConfig } from './getHoverInformation';
//# sourceMappingURL=index.d.ts.map