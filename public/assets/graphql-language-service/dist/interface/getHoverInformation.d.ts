import { GraphQLSchema, GraphQLType } from 'graphql';
import type { ContextToken } from '../parser';
import { AllTypeInfo, IPosition } from '../types';
import { Hover } from 'vscode-languageserver-types';
export declare type HoverConfig = {
    useMarkdown?: boolean;
};
export declare function getHoverInformation(schema: GraphQLSchema, queryText: string, cursor: IPosition, contextToken?: ContextToken, config?: HoverConfig): Hover['contents'];
export declare function renderField(into: string[], typeInfo: AllTypeInfo, options: any): void;
export declare function renderDirective(into: string[], typeInfo: AllTypeInfo, _options: any): void;
export declare function renderArg(into: string[], typeInfo: AllTypeInfo, options: any): void;
export declare function renderEnumValue(into: string[], typeInfo: AllTypeInfo, options: any): void;
export declare function renderType(into: string[], typeInfo: AllTypeInfo, options: any, t: GraphQLType): void;
//# sourceMappingURL=getHoverInformation.d.ts.map