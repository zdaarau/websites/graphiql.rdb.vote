export { getFragmentDependencies, getFragmentDependenciesForAST, } from './fragmentDependencies';
export { getVariablesJSONSchema, JSONSchema6, JSONSchema6TypeName, JSONSchemaOptions, } from './getVariablesJSONSchema';
export { getASTNodeAtPosition, pointToOffset } from './getASTNodeAtPosition';
export { Position, Range, locToRange, offsetToPosition } from './Range';
export { validateWithCustomRules } from './validateWithCustomRules';
export { collectVariables, VariableToType } from './collectVariables';
export { default as getOperationFacts, getOperationASTFacts, getQueryFacts, OperationFacts, QueryFacts, } from './getOperationFacts';
//# sourceMappingURL=index.d.ts.map