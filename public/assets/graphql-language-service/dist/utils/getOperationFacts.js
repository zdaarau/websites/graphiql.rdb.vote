"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getQueryFacts = exports.getOperationASTFacts = void 0;
const graphql_1 = require("graphql");
const collectVariables_1 = require("./collectVariables");
function getOperationASTFacts(documentAST, schema) {
    const variableToType = schema
        ? (0, collectVariables_1.collectVariables)(schema, documentAST)
        : undefined;
    const operations = [];
    (0, graphql_1.visit)(documentAST, {
        OperationDefinition(node) {
            operations.push(node);
        },
    });
    return { variableToType, operations };
}
exports.getOperationASTFacts = getOperationASTFacts;
function getOperationFacts(schema, documentString) {
    if (!documentString) {
        return;
    }
    try {
        const documentAST = (0, graphql_1.parse)(documentString);
        return Object.assign(Object.assign({}, getOperationASTFacts(documentAST, schema)), { documentAST });
    }
    catch (_a) {
        return;
    }
}
exports.default = getOperationFacts;
exports.getQueryFacts = getOperationFacts;
//# sourceMappingURL=getOperationFacts.js.map