"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var GraphiQLReact = __importStar(require("@graphiql/react"));
var toolkit_1 = require("@graphiql/toolkit");
var GraphQL = __importStar(require("graphql"));
var GraphiQL_1 = require("./components/GraphiQL");
require("@graphiql/react/font/roboto.css");
require("@graphiql/react/font/fira-code.css");
require("@graphiql/react/dist/style.css");
require("./style.css");
GraphiQL_1.GraphiQL.createFetcher = toolkit_1.createGraphiQLFetcher;
GraphiQL_1.GraphiQL.createLocalStorage = toolkit_1.createLocalStorage;
GraphiQL_1.GraphiQL.GraphQL = GraphQL;
GraphiQL_1.GraphiQL.React = GraphiQLReact;
exports.default = GraphiQL_1.GraphiQL;
//# sourceMappingURL=cdn.js.map