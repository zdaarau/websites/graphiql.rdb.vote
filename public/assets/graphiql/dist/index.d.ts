export { GraphiQLProvider } from '@graphiql/react';
export type { GraphiQLProps, GraphiQLInterfaceProps, } from './components/GraphiQL';
export type { GraphiQLProviderProps } from '@graphiql/react';
export { GraphiQLInterface, GraphiQL, GraphiQL as default, } from './components/GraphiQL';
//# sourceMappingURL=index.d.ts.map