"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = exports.GraphiQL = exports.GraphiQLInterface = exports.GraphiQLProvider = void 0;
var react_1 = require("@graphiql/react");
Object.defineProperty(exports, "GraphiQLProvider", { enumerable: true, get: function () { return react_1.GraphiQLProvider; } });
var GraphiQL_1 = require("./components/GraphiQL");
Object.defineProperty(exports, "GraphiQLInterface", { enumerable: true, get: function () { return GraphiQL_1.GraphiQLInterface; } });
Object.defineProperty(exports, "GraphiQL", { enumerable: true, get: function () { return GraphiQL_1.GraphiQL; } });
Object.defineProperty(exports, "default", { enumerable: true, get: function () { return GraphiQL_1.GraphiQL; } });
//# sourceMappingURL=index.js.map