import * as GraphiQLReact from '@graphiql/react';
import { createGraphiQLFetcher, createLocalStorage } from '@graphiql/toolkit';
import * as GraphQL from 'graphql';
import { GraphiQL } from './components/GraphiQL';
import '@graphiql/react/font/roboto.css';
import '@graphiql/react/font/fira-code.css';
import '@graphiql/react/dist/style.css';
import './style.css';
GraphiQL.createFetcher = createGraphiQLFetcher;
GraphiQL.createLocalStorage = createLocalStorage;
GraphiQL.GraphQL = GraphQL;
GraphiQL.React = GraphiQLReact;
export default GraphiQL;
//# sourceMappingURL=cdn.js.map