var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
import React, { Fragment, useCallback, useState, useEffect, useMemo, } from 'react';
import { Button, ButtonGroup, ChevronDownIcon, ChevronUpIcon, CopyIcon, Dialog, ExecuteButton, GraphiQLProvider, HeaderEditor, KeyboardShortcutIcon, MergeIcon, PlusIcon, PrettifyIcon, QueryEditor, ReloadIcon, ResponseEditor, SettingsIcon, Spinner, Tab, Tabs, ToolbarButton, Tooltip, UnStyledButton, useCopyQuery, useDragResize, useEditorContext, useExecutionContext, useMergeQuery, usePluginContext, usePrettifyEditors, useSchemaContext, useStorageContext, useTheme, VariableEditor, isMacOs, } from '@graphiql/react';
var majorVersion = parseInt(React.version.slice(0, 2), 10);
if (majorVersion < 16) {
    throw new Error([
        'GraphiQL 0.18.0 and after is not compatible with React 15 or below.',
        'If you are using a CDN source (jsdelivr, unpkg, etc), follow this example:',
        'https://github.com/graphql/graphiql/blob/master/examples/graphiql-cdn/index.html#L49',
    ].join('\n'));
}
export function GraphiQL(_a) {
    var _b;
    var dangerouslyAssumeSchemaIsValid = _a.dangerouslyAssumeSchemaIsValid, confirmCloseTab = _a.confirmCloseTab, defaultQuery = _a.defaultQuery, defaultTabs = _a.defaultTabs, externalFragments = _a.externalFragments, fetcher = _a.fetcher, getDefaultFieldNames = _a.getDefaultFieldNames, headers = _a.headers, inputValueDeprecation = _a.inputValueDeprecation, introspectionQueryName = _a.introspectionQueryName, maxHistoryLength = _a.maxHistoryLength, onEditOperationName = _a.onEditOperationName, onSchemaChange = _a.onSchemaChange, onTabChange = _a.onTabChange, onTogglePluginVisibility = _a.onTogglePluginVisibility, operationName = _a.operationName, plugins = _a.plugins, query = _a.query, response = _a.response, schema = _a.schema, schemaDescription = _a.schemaDescription, shouldPersistHeaders = _a.shouldPersistHeaders, storage = _a.storage, validationRules = _a.validationRules, variables = _a.variables, visiblePlugin = _a.visiblePlugin, defaultHeaders = _a.defaultHeaders, props = __rest(_a, ["dangerouslyAssumeSchemaIsValid", "confirmCloseTab", "defaultQuery", "defaultTabs", "externalFragments", "fetcher", "getDefaultFieldNames", "headers", "inputValueDeprecation", "introspectionQueryName", "maxHistoryLength", "onEditOperationName", "onSchemaChange", "onTabChange", "onTogglePluginVisibility", "operationName", "plugins", "query", "response", "schema", "schemaDescription", "shouldPersistHeaders", "storage", "validationRules", "variables", "visiblePlugin", "defaultHeaders"]);
    if (typeof fetcher !== 'function') {
        throw new TypeError('The `GraphiQL` component requires a `fetcher` function to be passed as prop.');
    }
    return (React.createElement(GraphiQLProvider, { getDefaultFieldNames: getDefaultFieldNames, dangerouslyAssumeSchemaIsValid: dangerouslyAssumeSchemaIsValid, defaultQuery: defaultQuery, defaultHeaders: defaultHeaders, defaultTabs: defaultTabs, externalFragments: externalFragments, fetcher: fetcher, headers: headers, inputValueDeprecation: inputValueDeprecation, introspectionQueryName: introspectionQueryName, maxHistoryLength: maxHistoryLength, onEditOperationName: onEditOperationName, onSchemaChange: onSchemaChange, onTabChange: onTabChange, onTogglePluginVisibility: onTogglePluginVisibility, plugins: plugins, visiblePlugin: visiblePlugin, operationName: operationName, query: query, response: response, schema: schema, schemaDescription: schemaDescription, shouldPersistHeaders: shouldPersistHeaders, storage: storage, validationRules: validationRules, variables: variables },
        React.createElement(GraphiQLInterface, __assign({ confirmCloseTab: confirmCloseTab, showPersistHeadersSettings: shouldPersistHeaders !== false, disableTabs: (_b = props.disableTabs) !== null && _b !== void 0 ? _b : false, forcedTheme: props.forcedTheme }, props))));
}
GraphiQL.Logo = GraphiQLLogo;
GraphiQL.Toolbar = GraphiQLToolbar;
GraphiQL.Footer = GraphiQLFooter;
var THEMES = ['light', 'dark', 'system'];
var TAB_CLASS_PREFIX = 'graphiql-session-tab-';
export function GraphiQLInterface(props) {
    var _this = this;
    var _a, _b, _c, _d;
    var isHeadersEditorEnabled = (_a = props.isHeadersEditorEnabled) !== null && _a !== void 0 ? _a : true;
    var editorContext = useEditorContext({ nonNull: true });
    var executionContext = useExecutionContext({ nonNull: true });
    var schemaContext = useSchemaContext({ nonNull: true });
    var storageContext = useStorageContext();
    var pluginContext = usePluginContext();
    var forcedTheme = useMemo(function () {
        return props.forcedTheme && THEMES.includes(props.forcedTheme)
            ? props.forcedTheme
            : undefined;
    }, [props.forcedTheme]);
    var copy = useCopyQuery({ onCopyQuery: props.onCopyQuery });
    var merge = useMergeQuery();
    var prettify = usePrettifyEditors();
    var _e = useTheme(props.defaultTheme), theme = _e.theme, setTheme = _e.setTheme;
    useEffect(function () {
        if (forcedTheme === 'system') {
            setTheme(null);
        }
        else if (forcedTheme === 'light' || forcedTheme === 'dark') {
            setTheme(forcedTheme);
        }
    }, [forcedTheme, setTheme]);
    var PluginContent = (_b = pluginContext === null || pluginContext === void 0 ? void 0 : pluginContext.visiblePlugin) === null || _b === void 0 ? void 0 : _b.content;
    var pluginResize = useDragResize({
        defaultSizeRelation: 1 / 3,
        direction: 'horizontal',
        initiallyHidden: (pluginContext === null || pluginContext === void 0 ? void 0 : pluginContext.visiblePlugin) ? undefined : 'first',
        onHiddenElementChange: function (resizableElement) {
            if (resizableElement === 'first') {
                pluginContext === null || pluginContext === void 0 ? void 0 : pluginContext.setVisiblePlugin(null);
            }
        },
        sizeThresholdSecond: 200,
        storageKey: 'docExplorerFlex',
    });
    var editorResize = useDragResize({
        direction: 'horizontal',
        storageKey: 'editorFlex',
    });
    var editorToolsResize = useDragResize({
        defaultSizeRelation: 3,
        direction: 'vertical',
        initiallyHidden: (function () {
            if (props.defaultEditorToolsVisibility === 'variables' ||
                props.defaultEditorToolsVisibility === 'headers') {
                return;
            }
            if (typeof props.defaultEditorToolsVisibility === 'boolean') {
                return props.defaultEditorToolsVisibility ? undefined : 'second';
            }
            return editorContext.initialVariables || editorContext.initialHeaders
                ? undefined
                : 'second';
        })(),
        sizeThresholdSecond: 60,
        storageKey: 'secondaryEditorFlex',
    });
    var _f = __read(useState(function () {
        if (props.defaultEditorToolsVisibility === 'variables' ||
            props.defaultEditorToolsVisibility === 'headers') {
            return props.defaultEditorToolsVisibility;
        }
        return !editorContext.initialVariables &&
            editorContext.initialHeaders &&
            isHeadersEditorEnabled
            ? 'headers'
            : 'variables';
    }), 2), activeSecondaryEditor = _f[0], setActiveSecondaryEditor = _f[1];
    var _g = __read(useState(null), 2), showDialog = _g[0], setShowDialog = _g[1];
    var _h = __read(useState(null), 2), clearStorageStatus = _h[0], setClearStorageStatus = _h[1];
    var children = React.Children.toArray(props.children);
    var logo = children.find(function (child) {
        return isChildComponentType(child, GraphiQL.Logo);
    }) || React.createElement(GraphiQL.Logo, null);
    var toolbar = children.find(function (child) {
        return isChildComponentType(child, GraphiQL.Toolbar);
    }) || (React.createElement(React.Fragment, null,
        React.createElement(ToolbarButton, { onClick: prettify, label: "Prettify query (Shift-Ctrl-P)" },
            React.createElement(PrettifyIcon, { className: "graphiql-toolbar-icon", "aria-hidden": "true" })),
        React.createElement(ToolbarButton, { onClick: merge, label: "Merge fragments into query (Shift-Ctrl-M)" },
            React.createElement(MergeIcon, { className: "graphiql-toolbar-icon", "aria-hidden": "true" })),
        React.createElement(ToolbarButton, { onClick: copy, label: "Copy query (Shift-Ctrl-C)" },
            React.createElement(CopyIcon, { className: "graphiql-toolbar-icon", "aria-hidden": "true" })), (_c = props.toolbar) === null || _c === void 0 ? void 0 :
        _c.additionalContent,
        ((_d = props.toolbar) === null || _d === void 0 ? void 0 : _d.additionalComponent) && (React.createElement(props.toolbar.additionalComponent, null))));
    var footer = children.find(function (child) {
        return isChildComponentType(child, GraphiQL.Footer);
    });
    var onClickReference = useCallback(function () {
        if (pluginResize.hiddenElement === 'first') {
            pluginResize.setHiddenElement(null);
        }
    }, [pluginResize]);
    var handleClearData = useCallback(function () {
        try {
            storageContext === null || storageContext === void 0 ? void 0 : storageContext.clear();
            setClearStorageStatus('success');
        }
        catch (_a) {
            setClearStorageStatus('error');
        }
    }, [storageContext]);
    var handlePersistHeaders = useCallback(function (event) {
        editorContext.setShouldPersistHeaders(event.currentTarget.dataset.value === 'true');
    }, [editorContext]);
    var handleChangeTheme = useCallback(function (event) {
        var selectedTheme = event.currentTarget.dataset.theme;
        setTheme(selectedTheme || null);
    }, [setTheme]);
    var handleAddTab = editorContext.addTab;
    var handleRefetchSchema = schemaContext.introspect;
    var handleReorder = editorContext.moveTab;
    var handleShowDialog = useCallback(function (event) {
        setShowDialog(event.currentTarget.dataset.value);
    }, []);
    var handlePluginClick = useCallback(function (event) {
        var context = pluginContext;
        var pluginIndex = Number(event.currentTarget.dataset.index);
        var plugin = context.plugins.find(function (_, index) { return pluginIndex === index; });
        var isVisible = plugin === context.visiblePlugin;
        if (isVisible) {
            context.setVisiblePlugin(null);
            pluginResize.setHiddenElement('first');
        }
        else {
            context.setVisiblePlugin(plugin);
            pluginResize.setHiddenElement(null);
        }
    }, [pluginContext, pluginResize]);
    var handleToolsTabClick = useCallback(function (event) {
        if (editorToolsResize.hiddenElement === 'second') {
            editorToolsResize.setHiddenElement(null);
        }
        setActiveSecondaryEditor(event.currentTarget.dataset.name);
    }, [editorToolsResize]);
    var toggleEditorTools = useCallback(function () {
        editorToolsResize.setHiddenElement(editorToolsResize.hiddenElement === 'second' ? null : 'second');
    }, [editorToolsResize]);
    var handleOpenShortKeysDialog = useCallback(function (isOpen) {
        if (!isOpen) {
            setShowDialog(null);
        }
    }, []);
    var handleOpenSettingsDialog = useCallback(function (isOpen) {
        if (!isOpen) {
            setShowDialog(null);
            setClearStorageStatus(null);
        }
    }, []);
    var addTab = (React.createElement(Tooltip, { label: "Add tab" },
        React.createElement(UnStyledButton, { type: "button", className: "graphiql-tab-add", onClick: handleAddTab, "aria-label": "Add tab" },
            React.createElement(PlusIcon, { "aria-hidden": "true" }))));
    var className = props.className ? " ".concat(props.className) : '';
    var confirmClose = props.confirmCloseTab;
    var handleTabClose = useCallback(function (event) { return __awaiter(_this, void 0, void 0, function () {
        var tabButton, index, shouldCloseTab, _a;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    tabButton = event.currentTarget
                        .previousSibling;
                    index = Number(tabButton.id.replace(TAB_CLASS_PREFIX, ''));
                    if (!confirmClose) return [3, 2];
                    return [4, confirmClose(index)];
                case 1:
                    _a = _b.sent();
                    return [3, 3];
                case 2:
                    _a = true;
                    _b.label = 3;
                case 3:
                    shouldCloseTab = _a;
                    if (!shouldCloseTab) {
                        return [2];
                    }
                    if (editorContext.activeTabIndex === index) {
                        executionContext.stop();
                    }
                    editorContext.closeTab(index);
                    return [2];
            }
        });
    }); }, [confirmClose, editorContext, executionContext]);
    var handleTabClick = useCallback(function (event) {
        var index = Number(event.currentTarget.id.replace(TAB_CLASS_PREFIX, ''));
        executionContext.stop();
        editorContext.changeTab(index);
    }, [editorContext, executionContext]);
    return (React.createElement(Tooltip.Provider, null,
        React.createElement("div", { "data-testid": "graphiql-container", className: "graphiql-container".concat(className) },
            React.createElement("div", { className: "graphiql-sidebar" },
                React.createElement("div", { className: "graphiql-sidebar-section" }, pluginContext === null || pluginContext === void 0 ? void 0 : pluginContext.plugins.map(function (plugin, index) {
                    var isVisible = plugin === pluginContext.visiblePlugin;
                    var label = "".concat(isVisible ? 'Hide' : 'Show', " ").concat(plugin.title);
                    return (React.createElement(Tooltip, { key: plugin.title, label: label },
                        React.createElement(UnStyledButton, { type: "button", className: isVisible ? 'active' : '', onClick: handlePluginClick, "data-index": index, "aria-label": label },
                            React.createElement(plugin.icon, { "aria-hidden": "true" }))));
                })),
                React.createElement("div", { className: "graphiql-sidebar-section" },
                    React.createElement(Tooltip, { label: "Re-fetch GraphQL schema" },
                        React.createElement(UnStyledButton, { type: "button", disabled: schemaContext.isFetching, onClick: handleRefetchSchema, "aria-label": "Re-fetch GraphQL schema" },
                            React.createElement(ReloadIcon, { className: schemaContext.isFetching ? 'graphiql-spin' : '', "aria-hidden": "true" }))),
                    React.createElement(Tooltip, { label: "Open short keys dialog" },
                        React.createElement(UnStyledButton, { type: "button", "data-value": "short-keys", onClick: handleShowDialog, "aria-label": "Open short keys dialog" },
                            React.createElement(KeyboardShortcutIcon, { "aria-hidden": "true" }))),
                    React.createElement(Tooltip, { label: "Open settings dialog" },
                        React.createElement(UnStyledButton, { type: "button", "data-value": "settings", onClick: handleShowDialog, "aria-label": "Open settings dialog" },
                            React.createElement(SettingsIcon, { "aria-hidden": "true" }))))),
            React.createElement("div", { className: "graphiql-main" },
                React.createElement("div", { ref: pluginResize.firstRef, style: {
                        minWidth: '200px',
                    } },
                    React.createElement("div", { className: "graphiql-plugin" }, PluginContent ? React.createElement(PluginContent, null) : null)),
                (pluginContext === null || pluginContext === void 0 ? void 0 : pluginContext.visiblePlugin) && (React.createElement("div", { className: "graphiql-horizontal-drag-bar", ref: pluginResize.dragBarRef })),
                React.createElement("div", { ref: pluginResize.secondRef, className: "graphiql-sessions" },
                    React.createElement("div", { className: "graphiql-session-header" },
                        !props.disableTabs && (React.createElement(Tabs, { values: editorContext.tabs, onReorder: handleReorder, "aria-label": "Select active operation" }, editorContext.tabs.length > 1 && (React.createElement(React.Fragment, null,
                            editorContext.tabs.map(function (tab, index) { return (React.createElement(Tab, { key: tab.id, value: tab, isActive: index === editorContext.activeTabIndex },
                                React.createElement(Tab.Button, { "aria-controls": "graphiql-session", id: "".concat(TAB_CLASS_PREFIX).concat(index), onClick: handleTabClick }, tab.title),
                                React.createElement(Tab.Close, { onClick: handleTabClose }))); }),
                            addTab)))),
                        React.createElement("div", { className: "graphiql-session-header-right" },
                            editorContext.tabs.length === 1 && addTab,
                            logo)),
                    React.createElement("div", { role: "tabpanel", id: "graphiql-session", className: "graphiql-session", "aria-labelledby": "".concat(TAB_CLASS_PREFIX).concat(editorContext.activeTabIndex) },
                        React.createElement("div", { ref: editorResize.firstRef },
                            React.createElement("div", { className: "graphiql-editors".concat(editorContext.tabs.length === 1 ? ' full-height' : '') },
                                React.createElement("div", { ref: editorToolsResize.firstRef },
                                    React.createElement("section", { className: "graphiql-query-editor", "aria-label": "Query Editor" },
                                        React.createElement(QueryEditor, { editorTheme: props.editorTheme, keyMap: props.keyMap, onClickReference: onClickReference, onCopyQuery: props.onCopyQuery, onEdit: props.onEditQuery, readOnly: props.readOnly }),
                                        React.createElement("div", { className: "graphiql-toolbar", role: "toolbar", "aria-label": "Editor Commands" },
                                            React.createElement(ExecuteButton, null),
                                            toolbar))),
                                React.createElement("div", { ref: editorToolsResize.dragBarRef },
                                    React.createElement("div", { className: "graphiql-editor-tools" },
                                        React.createElement(UnStyledButton, { type: "button", className: activeSecondaryEditor === 'variables' &&
                                                editorToolsResize.hiddenElement !== 'second'
                                                ? 'active'
                                                : '', onClick: handleToolsTabClick, "data-name": "variables" }, "Variables"),
                                        isHeadersEditorEnabled && (React.createElement(UnStyledButton, { type: "button", className: activeSecondaryEditor === 'headers' &&
                                                editorToolsResize.hiddenElement !== 'second'
                                                ? 'active'
                                                : '', onClick: handleToolsTabClick, "data-name": "headers" }, "Headers")),
                                        React.createElement(Tooltip, { label: editorToolsResize.hiddenElement === 'second'
                                                ? 'Show editor tools'
                                                : 'Hide editor tools' },
                                            React.createElement(UnStyledButton, { type: "button", onClick: toggleEditorTools, "aria-label": editorToolsResize.hiddenElement === 'second'
                                                    ? 'Show editor tools'
                                                    : 'Hide editor tools', className: "graphiql-toggle-editor-tools" }, editorToolsResize.hiddenElement === 'second' ? (React.createElement(ChevronUpIcon, { className: "graphiql-chevron-icon", "aria-hidden": "true" })) : (React.createElement(ChevronDownIcon, { className: "graphiql-chevron-icon", "aria-hidden": "true" })))))),
                                React.createElement("div", { ref: editorToolsResize.secondRef },
                                    React.createElement("section", { className: "graphiql-editor-tool", "aria-label": activeSecondaryEditor === 'variables'
                                            ? 'Variables'
                                            : 'Headers' },
                                        React.createElement(VariableEditor, { editorTheme: props.editorTheme, isHidden: activeSecondaryEditor !== 'variables', keyMap: props.keyMap, onEdit: props.onEditVariables, onClickReference: onClickReference, readOnly: props.readOnly }),
                                        isHeadersEditorEnabled && (React.createElement(HeaderEditor, { editorTheme: props.editorTheme, isHidden: activeSecondaryEditor !== 'headers', keyMap: props.keyMap, onEdit: props.onEditHeaders, readOnly: props.readOnly })))))),
                        React.createElement("div", { className: "graphiql-horizontal-drag-bar", ref: editorResize.dragBarRef }),
                        React.createElement("div", { ref: editorResize.secondRef },
                            React.createElement("div", { className: "graphiql-response" },
                                executionContext.isFetching ? React.createElement(Spinner, null) : null,
                                React.createElement(ResponseEditor, { editorTheme: props.editorTheme, responseTooltip: props.responseTooltip, keyMap: props.keyMap }),
                                footer))))),
            React.createElement(Dialog, { open: showDialog === 'short-keys', onOpenChange: handleOpenShortKeysDialog },
                React.createElement("div", { className: "graphiql-dialog-header" },
                    React.createElement(Dialog.Title, { className: "graphiql-dialog-title" }, "Short Keys"),
                    React.createElement(Dialog.Close, null)),
                React.createElement("div", { className: "graphiql-dialog-section" },
                    React.createElement(ShortKeys, { keyMap: props.keyMap || 'sublime' }))),
            React.createElement(Dialog, { open: showDialog === 'settings', onOpenChange: handleOpenSettingsDialog },
                React.createElement("div", { className: "graphiql-dialog-header" },
                    React.createElement(Dialog.Title, { className: "graphiql-dialog-title" }, "Settings"),
                    React.createElement(Dialog.Close, null)),
                props.showPersistHeadersSettings ? (React.createElement("div", { className: "graphiql-dialog-section" },
                    React.createElement("div", null,
                        React.createElement("div", { className: "graphiql-dialog-section-title" }, "Persist headers"),
                        React.createElement("div", { className: "graphiql-dialog-section-caption" },
                            "Save headers upon reloading.",
                            ' ',
                            React.createElement("span", { className: "graphiql-warning-text" }, "Only enable if you trust this device."))),
                    React.createElement(ButtonGroup, null,
                        React.createElement(Button, { type: "button", id: "enable-persist-headers", className: editorContext.shouldPersistHeaders ? 'active' : '', "data-value": "true", onClick: handlePersistHeaders }, "On"),
                        React.createElement(Button, { type: "button", id: "disable-persist-headers", className: editorContext.shouldPersistHeaders ? '' : 'active', onClick: handlePersistHeaders }, "Off")))) : null,
                !forcedTheme && (React.createElement("div", { className: "graphiql-dialog-section" },
                    React.createElement("div", null,
                        React.createElement("div", { className: "graphiql-dialog-section-title" }, "Theme"),
                        React.createElement("div", { className: "graphiql-dialog-section-caption" }, "Adjust how the interface appears.")),
                    React.createElement(ButtonGroup, null,
                        React.createElement(Button, { type: "button", className: theme === null ? 'active' : '', onClick: handleChangeTheme }, "System"),
                        React.createElement(Button, { type: "button", className: theme === 'light' ? 'active' : '', "data-theme": "light", onClick: handleChangeTheme }, "Light"),
                        React.createElement(Button, { type: "button", className: theme === 'dark' ? 'active' : '', "data-theme": "dark", onClick: handleChangeTheme }, "Dark")))),
                storageContext ? (React.createElement("div", { className: "graphiql-dialog-section" },
                    React.createElement("div", null,
                        React.createElement("div", { className: "graphiql-dialog-section-title" }, "Clear storage"),
                        React.createElement("div", { className: "graphiql-dialog-section-caption" }, "Remove all locally stored data and start fresh.")),
                    React.createElement(Button, { type: "button", state: clearStorageStatus || undefined, disabled: clearStorageStatus === 'success', onClick: handleClearData }, {
                        success: 'Cleared data',
                        error: 'Failed',
                    }[clearStorageStatus] || 'Clear data'))) : null))));
}
var modifier = isMacOs ? '⌘' : 'Ctrl';
var SHORT_KEYS = Object.entries({
    'Search in editor': [modifier, 'F'],
    'Search in documentation': [modifier, 'K'],
    'Execute query': [modifier, 'Enter'],
    'Prettify editors': ['Ctrl', 'Shift', 'P'],
    'Merge fragments definitions into operation definition': [
        'Ctrl',
        'Shift',
        'M',
    ],
    'Copy query': ['Ctrl', 'Shift', 'C'],
    'Re-fetch schema using introspection': ['Ctrl', 'Shift', 'R'],
});
function ShortKeys(_a) {
    var keyMap = _a.keyMap;
    return (React.createElement("div", null,
        React.createElement("table", { className: "graphiql-table" },
            React.createElement("thead", null,
                React.createElement("tr", null,
                    React.createElement("th", null, "Short Key"),
                    React.createElement("th", null, "Function"))),
            React.createElement("tbody", null, SHORT_KEYS.map(function (_a) {
                var _b = __read(_a, 2), title = _b[0], keys = _b[1];
                return (React.createElement("tr", { key: title },
                    React.createElement("td", null, keys.map(function (key, index, array) { return (React.createElement(Fragment, { key: key },
                        React.createElement("code", { className: "graphiql-key" }, key),
                        index !== array.length - 1 && ' + ')); })),
                    React.createElement("td", null, title)));
            }))),
        React.createElement("p", null,
            "The editors use",
            ' ',
            React.createElement("a", { href: "https://codemirror.net/5/doc/manual.html#keymaps", target: "_blank", rel: "noopener noreferrer" }, "CodeMirror Key Maps"),
            ' ',
            "that add more short keys. This instance of Graph",
            React.createElement("em", null, "i"),
            "QL uses",
            ' ',
            React.createElement("code", null, keyMap),
            ".")));
}
function GraphiQLLogo(props) {
    return (React.createElement("div", { className: "graphiql-logo" }, props.children || (React.createElement("a", { className: "graphiql-logo-link", href: "https://github.com/graphql/graphiql", target: "_blank", rel: "noreferrer" },
        "Graph",
        React.createElement("em", null, "i"),
        "QL"))));
}
GraphiQLLogo.displayName = 'GraphiQLLogo';
function GraphiQLToolbar(props) {
    return React.createElement(React.Fragment, null, props.children);
}
GraphiQLToolbar.displayName = 'GraphiQLToolbar';
function GraphiQLFooter(props) {
    return React.createElement("div", { className: "graphiql-footer" }, props.children);
}
GraphiQLFooter.displayName = 'GraphiQLFooter';
function isChildComponentType(child, component) {
    var _a;
    if (((_a = child === null || child === void 0 ? void 0 : child.type) === null || _a === void 0 ? void 0 : _a.displayName) &&
        child.type.displayName === component.displayName) {
        return true;
    }
    return child.type === component;
}
//# sourceMappingURL=GraphiQL.js.map