import React, { PropsWithChildren, ReactNode } from 'react';
import { GraphiQLProviderProps, Theme, UseHeaderEditorArgs, UseQueryEditorArgs, UseResponseEditorArgs, UseVariableEditorArgs, WriteableEditorProps } from '@graphiql/react';
export declare type GraphiQLToolbarConfig = {
    additionalContent?: React.ReactNode;
    additionalComponent?: React.JSXElementConstructor<any>;
};
export declare type GraphiQLProps = Omit<GraphiQLProviderProps, 'children'> & GraphiQLInterfaceProps;
export declare function GraphiQL({ dangerouslyAssumeSchemaIsValid, confirmCloseTab, defaultQuery, defaultTabs, externalFragments, fetcher, getDefaultFieldNames, headers, inputValueDeprecation, introspectionQueryName, maxHistoryLength, onEditOperationName, onSchemaChange, onTabChange, onTogglePluginVisibility, operationName, plugins, query, response, schema, schemaDescription, shouldPersistHeaders, storage, validationRules, variables, visiblePlugin, defaultHeaders, ...props }: GraphiQLProps): React.JSX.Element;
export declare namespace GraphiQL {
    var Logo: typeof GraphiQLLogo;
    var Toolbar: typeof GraphiQLToolbar;
    var Footer: typeof GraphiQLFooter;
}
declare type AddSuffix<Obj extends Record<string, any>, Suffix extends string> = {
    [Key in keyof Obj as `${string & Key}${Suffix}`]: Obj[Key];
};
export declare type GraphiQLInterfaceProps = WriteableEditorProps & AddSuffix<Pick<UseQueryEditorArgs, 'onEdit'>, 'Query'> & Pick<UseQueryEditorArgs, 'onCopyQuery'> & AddSuffix<Pick<UseVariableEditorArgs, 'onEdit'>, 'Variables'> & AddSuffix<Pick<UseHeaderEditorArgs, 'onEdit'>, 'Headers'> & Pick<UseResponseEditorArgs, 'responseTooltip'> & {
    children?: ReactNode;
    defaultEditorToolsVisibility?: boolean | 'variables' | 'headers';
    isHeadersEditorEnabled?: boolean;
    toolbar?: GraphiQLToolbarConfig;
    showPersistHeadersSettings?: boolean;
    defaultTheme?: Theme;
    disableTabs?: boolean;
    forcedTheme?: (typeof THEMES)[number];
    className?: string;
    confirmCloseTab?(index: number): Promise<boolean> | boolean;
};
declare const THEMES: readonly ["light", "dark", "system"];
export declare function GraphiQLInterface(props: GraphiQLInterfaceProps): React.JSX.Element;
declare function GraphiQLLogo<TProps>(props: PropsWithChildren<TProps>): React.JSX.Element;
declare namespace GraphiQLLogo {
    var displayName: string;
}
declare function GraphiQLToolbar<TProps>(props: PropsWithChildren<TProps>): React.JSX.Element;
declare namespace GraphiQLToolbar {
    var displayName: string;
}
declare function GraphiQLFooter<TProps>(props: PropsWithChildren<TProps>): React.JSX.Element;
declare namespace GraphiQLFooter {
    var displayName: string;
}
export {};
//# sourceMappingURL=GraphiQL.d.ts.map